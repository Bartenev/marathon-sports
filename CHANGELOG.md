# Change Log
All notable changes to this project will be documented in this file.

## 05/22/2018
* ACF: create new field group Linked Button [CLONE]. This group will replace existing button fields used across content rows
* ACF: add linked button field group to flexible content > flexible content > featured product field group
* ACF: remove flexible content > flexible content > featured product fields: button type, button label, page link, URL, and link behavior. Use clone field instead
* ACF: add linked button field to flexible content > flexible content > featured link bar
* ACF: remove flexible content > flexible content > featured link bar > link fields: link type, page link, and URL. Use clone field instead
* ACF: add linked button field to flexible content > flexible content > full width slider
* ACF: remove flexible content > flexible content > full width slider fields: button type, button label, page link, and URL. Use clone field instead
* ACF: add linked button field to flexible content > flexible content > full width image
* ACF: remove flexible content > flexible content > full width image fields: show / hide button, button type, button label, page link, and URL. Use clone field instead
* ACF: remove flexible content > flexible content > multi-column fields: empty messages
* ACF: add linked button field to flexible content > flexible content > multi-column
* ACF: remove flexible content > flexible content > multi-column fields: button type, button label, page link, and URL. Use clone field instead

## 05/21/2018
* ACF: add link behavior field to flexible content > flexible content > featured product field group

## 05/08/2018
* Update references to "runner_alley" with "runners_alley"
* ACF theme settings: change logo value from "runner_alley" to "runners_alley"

## 05/02/2018
* Add new option to ACF Flexible Content
        * Eventbrite Events > (slider) Include Events By > all

## 04/24/2018
* Update Flexible Content : Eventbrite Events
    * Add require for Event Category field

## 03/05/2018
* Create run_clubs post type in pods (client requested this post type be restored)
* Add 'Run Clubs' option to ACF felxible content > Interactive Map field group
        * Add toolbar title fields
        * Update conditional logic
        * Update location settings (where fields are displayed)

## 02/16/2018
* Remove Notifications custom post.


## 02/14/2018
* Remove Flexible Content upcoming events content row - this row replaces by Eventbrite Events content row

## 02/14/2018
* Update Flexible Content :
        Full With Image - add Image Overlay
        Multi Column - add Image Overlay
        Page Header - add Image Overlay
        Eventbrite Events - remove Events tab

## 02/13/2018
* Create Eventbrite Events flexible content row
* Adjust theme settings page settings (icon, menu position, etc.)
* Delete run_clubs and race_teams custom post types from PODs admin - this content is managed by eventbrite

## 01/18/2018
* Add new field "section heading" to field group Flexible Content > Simple Text Content with Heading


## 01/09/2018
* Create new custom filed group Breadcrumb

## 01/04/2018
* ACF modification: flexible content > page header
 * Create "settings" tab
 * Create "background" tab
 * Organize fields into relevant tabs
 * Change image position field option "center -center" to "center - center"
 * Add in-line documentation for multiple fields (instructions field)
 * Remove section type choices field option from "Standart" to "Standard"
* ACF modification: flexible content > simple text content with heading
 * Add "content" tab
 *
