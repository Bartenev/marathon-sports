<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while (have_posts() ) : the_post(); ?>

		<?php
		$hero_image = get_field('image');
		if ( !empty($hero_image) ) {
			$hero_image = wp_get_attachment_image_src(get_field('image'), 'full_hd')[0];
		}else {
			$hero_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' )[0];
		}
		$map =  get_field('map');

		$address = explode( ',', $map['address']);
		$address_state = explode( ' ', $address[2]);
		$location_address = $address[0] . '<span>' . $address[1] . ', ' . $address_state[1] . ' ' . $address_state[2] . '</span>';
		$club_title = get_the_title();
		?>
        <!--  Club Header  -->
        <div class="c-full-width bg-cover" style="background-image:url(<?php echo $hero_image; ?>);">
            <div class="content content_full-width">
                <div class="c-full-width__title" data-clubtitle="<?php echo $club_title; ?>">
                    <?php echo $club_title; ?>
                    <span><?php echo $address[1] . ', ' . $address[2]; ?></span>
                </div>
            </div>
        </div>

		<?php
		$text_headline = get_field('text_headline');
		$description = get_field('description');

		?>
		<?php if ( !empty($text_headline) && !empty($description) ) : ?>
            <!--  Club details  -->
            <div class="content">
                <div class="heading-width-text">
                    <h2 class="heading-width-text__title">
						<?php echo $text_headline; ?>
                        <i class="title-bar brand"></i>
                    </h2>
                    <div class="heading-width-text__article">
						<?php echo $description; ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>

		<?php
		$phone = get_field('phone');
		$facebook_link = get_field('facebook_link');
		if ( empty($facebook_link) ) {
			$facebook_link = get_field('facebook', 'option');
		}
		$parking = get_field('parking');
		$public_transit = get_field('public_transit');
		?>
        <!--  Address and Contact Info  -->
        <!--  Store Hours  -->
        <!--  Additional location and direction information  -->
        <div class="content">
            <div class="location-single">
                <div class="location-grid__body">
                    <h2 class="location-grid__title"><?php echo $location_address; ?></h2>
					<?php if ( !empty($phone) ) : ?>
                        <p><a href="tel:<?php echo $phone; ?>" class="location-grid__phone"><?php echo $phone; ?></a></p>
					<?php endif; ?>
                    <p><a href="http://maps.google.com/maps?q=loc:<?php echo $map['lat'] . ',' . $map['lng']; ?>" target="_blank" data-map class="location-grid__direction"><span>get directions</span></a></p>
                    <a href="<?php echo $facebook_link; ?>" target="_blank" data-map class="location-grid__link location-grid__link_fb" >find us on facebook</a>
                </div>
                <div class="location-grid__body location-grid__body_list-style">
					<?php if ( have_rows('store_hour') ) : ?>
                        <h2 class="location-grid__title">store hours</h2>
                        <ul class="c-timetable">
							<?php while(have_rows('store_hour')) : the_row(); ?>
                                <li class="c-timetable__item">
									<?php echo get_sub_field('date'); ?> &ndash; <span><?php echo get_sub_field('hours'); ?></span>
                                </li>
							<?php endwhile; ?>
                        </ul>
					<?php endif; ?>
                </div>
				<?php if ( !empty($parking) || !empty($public_transit) ) : ?>
                    <div class="location-grid__body location-grid__body_list-style">
						<?php if ( !empty($parking) ) : ?>
                            <h2 class="location-grid__title">parking</h2>
                            <p class="location-grid__title location-grid__title_sub"><?php echo $parking; ?></p>
						<?php endif; ?>
						<?php if ( !empty($public_transit) ) : ?>
                            <h2 class="location-grid__title">public transit</h2>
                            <p class="location-grid__title location-grid__title_sub"><?php echo $public_transit; ?></p>
						<?php endif; ?>
                    </div>
				<?php endif; ?>
            </div>
        </div>

		<?php
		$management_image = wp_get_attachment_image_src(get_field('management_image'), 'full_hd')[0];
		$management_name = get_field('management_name');
		$management_job = get_field('management_job');
		$filter =  get_field('brand_color');
		?>
        <!--  Map -->
        <div class="c-half-width c-half-width_full">
            <div class="c-half-width__map">
                <div id="single-map"
                     data-marker-lat="<?php echo $map['lat']; ?>"
                     data-marker-lng="<?php echo $map['lng']; ?>"
                     data-image-url="<?php echo get_template_directory_uri() ?>/media/pin-<?php echo $filter; ?>.png"
                     data-href="http://maps.google.com/maps?q=loc:<?php echo $map['lat'] . ',' . $map['lng']; ?>"
                     data-address="<?php echo $location_address; ?>"
                ></div>
            </div>
        </div>
	<?php endwhile; ?>
<?php endif; ?>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>