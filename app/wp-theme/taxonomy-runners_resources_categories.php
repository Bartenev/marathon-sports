<?php
/**
 * The template for displaying Archive pages.
 */

get_header(); ?>

<?php
	$image = wp_get_attachment_image_src(get_field('background_search', 'option'), 'full_hd')[0];
	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'runners_resources',
		'tax_query' => array(
			array(
				'taxonomy' => 'runners_resources_categories',
				'field' => 'term_id',
				'terms' => get_queried_object()->term_id,
			)
		),
		'paged'=> 1
	);
	$query = new WP_Query( $args );
	$p_count = $query->max_num_pages;
?>
	<!--  Page Header  -->
	<div class="page-header page-header_small bg-cover" style="background-image: url(<?php echo $image; ?>);">
		<div class="content">
			<h1><?php echo single_cat_title('Archive results for: ',false); ?></h1>
		</div>
	</div>
	<div class="list-with-sidebar" data-type-post="runners_resources" data-category="<?php echo get_queried_object()->term_id; ?>">
		<aside class="sidebar">
            <ul class="sidebar__item">
                <form role="search" method="get" id="searchform-sidebar" class="searchform" action="<?php echo home_url( '/' ); ?>" data-name-post="RESOURCES">
                    <div>
                        <input type="text" value="" name="s" id="s" placeholder="Search Resources">
                        <button type="submit" id="searchsubmit">
                        <span class="trigger-search color-dark-to">
                             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                                <defs>
                                    <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                                </defs>
                                <g id="svg-1" data-name="svg-1">
                                    <g>
                                        <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"></path>
                                    </g>
                                </g>
                            </svg>
                        </span>
                        </button>
                    </div>
                </form>
            </ul>
			<?php if ( is_active_sidebar( 'sidebar-item-2' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar-item-2' ); ?>
			<?php endif; ?>
		</aside>
		<ul class="post-list" data-ajax-page="1">
			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full_hd')[0];
					?>
					<li class="post-list-item">
						<?php if ( !empty($image) ) : ?>
							<a href="<?php the_permalink(); ?>">
								<div class="post-list-item__img bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
							</a>
						<?php endif; ?>
						<a href="<?php the_permalink(); ?>">
							<h2><?php the_title(); ?></h2>
						</a>
						<div class="post-list-item__content">
							<?php the_excerpt(); ?>
						</div>
						<a href="<?php the_permalink(); ?>" class="btn brand brand--color-white"><span>READ MORE</span></a>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
			<li class="post-list-nav">
				<?php if ( $query->have_posts() && $p_count > 1 ) : ?>
					<a data-page="prev" class="post-list-nav_arrow">previous</a>
					<?php for ($x = 1; $x <= $p_count; $x++) : ?>
						<a class="post-list-nav__number <?php echo $x === 1 ? 'active' :'' ?>" data-page="<?php echo $x; ?>"><?php echo $x; ?></a>
					<?php endfor; ?>
					<a data-page="next" class="post-list-nav_arrow">next</a>
				<?php endif; ?>
			</li>
		</ul>
	</div>

<?php get_footer(); ?>