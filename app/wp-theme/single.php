<?php get_header(); ?>
    <?php get_template_part('breadcrumbs'); ?>
    <?php if ( have_posts() ) : ?>
        <?php while (have_posts() ) : the_post();
		    $id = get_the_ID();
		    $image = wp_get_attachment_image_src(get_post_thumbnail_id( $id ), 'full_hd')[0];
            $prev_post = get_previous_post(true);
            $next_post = get_next_post(true);
            $all_news = get_permalink(169);
		    $search_title = '';
		    if ( get_post_type($id) == 'post' ) $search_title = 'News';
		    if ( get_post_type($id) == 'runners_resources' ) $search_title = 'Resources';
        ?>

            <div class="list-with-sidebar" data-type-post="<?php echo get_post_type($id); ?>">
                <aside class="sidebar">
                    <ul class="sidebar__item">
                        <form role="search" method="get" id="searchform-sidebar" class="searchform"
                              action="<?php echo home_url( '/' ); ?>"
                              data-name-post="<?php echo $search_title; ?>"
                        >
                            <div>
                                <input type="text" value="" name="s" id="s" placeholder="Search <?php echo $search_title; ?>">
                                <button type="submit" id="searchsubmit">
                                <span class="trigger-search color-dark-to">
                                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                                        <defs>
                                            <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                                        </defs>
                                        <g id="svg-1" data-name="svg-1">
                                            <g>
                                                <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </span>
                                </button>
                            </div>
                        </form>
                    </ul>
                    <?php if ( is_active_sidebar( 'sidebar-item-1' ) ) : ?>
                        <?php dynamic_sidebar( 'sidebar-item-1' ); ?>
                    <?php endif; ?>
                </aside>
                <ul class="post-list" data-ajax-page="1">
                    <li class="post-list-item">
		                <?php if ( !empty($image) ) : ?>
                            <div class="post-list-item__img bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
		                <?php endif; ?>
                        <h2><?php the_title(); ?></h2>
                        <div class="post-list-item__content">
	                        <?php the_content(); ?>
                        </div>
                    </li>
                    <li class="post-list-nav">
                        <a href="<?php echo get_the_permalink($prev_post->ID); ?>" class="post-list-nav_arrow <?php echo $prev_post ? '' : 'empty'; ?>">previous</a>
                        <a class="post-list-nav_all" href="<?php  echo $all_news?>">all news</a>
                        <a href="<?php echo get_the_permalink($next_post->ID); ?>" class="post-list-nav_arrow <?php echo $next_post ? '' :  'empty'; ?>">next</a>
                    </li>
                </ul>
            </div>

            <?php get_template_part( 'flexible-content-rows'); ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php wp_reset_query(); ?>
<?php get_footer(); ?>