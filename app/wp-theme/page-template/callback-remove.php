<?php
/*
* Template name: Callback Remove
*/

	// Webhooks Shopify
	define('SHOPIFY_APP_SECRET', '8901130f9ef918598c7d43675d5c3390');

	function verify_webhook($data, $hmac_header) {
		$calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
		return hash_equals($hmac_header, $calculated_hmac);
	}

	$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
	$data = file_get_contents('php://input');
	$data_json_decoder = json_decode($data, true);

	// $static_file = 'product.json';
	// file_put_contents($static_file, $data);

	$args = array(
		'post_type' => 'product',
		'posts_per_page' => -1
	);
	$query = new WP_Query($args);

	// remove post
	while ( $query->have_posts() ) {
		$query->the_post();

		if ( $data_json_decoder['id'] == get_post_meta(get_the_id(), 'product_id')[0] ) {
			wp_delete_post(get_the_id());
		}
	}

	$verified = verify_webhook($data, $hmac_header);
	error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result
?>