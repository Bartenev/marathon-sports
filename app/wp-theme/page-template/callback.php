<?php
/*
* Template name: Callback
*/
	$api_url = get_field('shopify_app_api_url', 'option');


	// Webhooks Shopify
	define('SHOPIFY_APP_SECRET', '8901130f9ef918598c7d43675d5c3390');

	function verify_webhook($data, $hmac_header) {
		$calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
		return hash_equals($hmac_header, $calculated_hmac);
	}

	$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
	$data = file_get_contents('php://input');
	$data_json_decoder = json_decode($data, true);

	$image = '';

	if (!empty($data_json_decoder['images'][0]['src'])) {
		$image = $data_json_decoder['images'][0]['src'];
	}

	$post_data = array(
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		'post_content'   => $data_json_decoder['body_html'],
		'post_name'      => $data_json_decoder['title'],
		'post_status'    => 'publish',
		'post_title'     => $data_json_decoder['title'],
		'post_type'      => 'product',
		'meta_input'     => array(
			'product_id' => $data_json_decoder['id'],
			'product_price' => $data_json_decoder['variants'][0]['price'],
			'product_vendor' => $data_json_decoder['vendor'],
			'product_handle' => $data_json_decoder['handle'],
			'product_thumbnail' => $image,
			'product_variant' => array(),
			'product_tags' => $data_json_decoder['tags'],
			'product_category' => ''
		)
	);

	// product variant images
	$option_index = 0;
	$values = array();
	foreach ($data_json_decoder['options'] as $key => $option) {
		if ($option['name'] == 'Color' || $option == 'color') {
			$option_index = 'option' . ($key + 1);
		}
	}
	foreach ($data_json_decoder['variants'] as $key => $product_variant) {
		$variant_color = $product_variant[$option_index];
		$variant_image_scr = '//cdn.shopify.com/s/assets/no-image-2048-5e88c1b20e087fb7bbe9a3771824e743c244f437e4f8ba93bbf7b11b53f7824c_x200.gif';
		$variant_image_scr_small = '//cdn.shopify.com/s/assets/no-image-100-c91dd4bdb56513f2cbf4fc15436ca35e9d4ecd014546c8d421b1aece861dfecf_65x.gif';

		foreach ($data_json_decoder['images'] as $images) {
			$shop_url = get_field('shopify_site_url', 'option');
			$href = $shop_url . '/products/' . $data_json_decoder['handle'] . '?variant=' . $product_variant['id'];

			if ( !empty($images['variant_ids'][0]) && $product_variant['id'] == $images['variant_ids'][0] ) {
				$variant_image_parts = pathinfo($data_json_decoder['images'][$key]['src']);
				$variant_image_scr = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_x200.' . $variant_image_parts['extension'];
				$variant_image_scr_small = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_65x.' . $variant_image_parts['extension'];
			}

			if ( !in_array($variant_color, $values) ) {
				array_push($values, $variant_color);

				$post_data['meta_input']['product_variant'][$key]['small'] = $variant_image_scr_small;
				$post_data['meta_input']['product_variant'][$key]['full'] = $variant_image_scr;
			}
		}
	}

	// product reviews rating
	$product_reviews_url = $api_url . '/admin/products/' . $data_json_decoder['id'] . '/metafields.json?namespace=stamped&key=badge&fields=value';
	$product_reviews_url_json = file_get_contents($product_reviews_url);
	$product_reviews = json_decode($product_reviews_url_json, true);
	if (!empty($product_reviews["metafields"][0]['value'])) {
		$post_data['meta_input']['product_reviews'] = $product_reviews["metafields"][0]['value'];
	}

	// collection product
	$custom_collections_url = $api_url . '/admin/custom_collections.json';
	$custom_collections_json = file_get_contents($custom_collections_url);
	$custom_collections = json_decode($custom_collections_json);

	$product_categories = $api_url . '/admin/collects.json?product_id=' . $data_json_decoder['id'] . '&limit=250';
	$product_categories_json = file_get_contents($product_categories);
	$categories = json_decode($product_categories_json, true);
	$product_category = '';

	foreach ($custom_collections->custom_collections as $key => $custom_cat) {
		foreach ($categories['collects'] as $key => $cat) {
			if ( $custom_cat->id == $cat["collection_id"] ) {
				$product_category .= !empty($product_category) ? ', ' . $custom_cat->title : $custom_cat->title;
				// array_push($product_category, $custom_cat->title);
			}
		}
	}

	$post_data['meta_input']['product_category'] = $product_category;

	$args = array(
		'post_type' => 'product',
		'posts_per_page' => -1
	);
	$query = new WP_Query($args);
	$is_post = false;

	// update post
	while ( $query->have_posts() ) {
		$query->the_post();

		if ( $data_json_decoder['id'] == get_post_meta(get_the_id(), 'product_id')[0] ) {
			$is_post = true;
			$post_data['ID'] = get_the_id();
			wp_update_post($post_data);
		}
	}

	// add new post
	if (!$is_post) {
		wp_insert_post( $post_data );
	}

	$verified = verify_webhook($data, $hmac_header);
	error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result
?>