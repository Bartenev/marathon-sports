<?php
/*
* Template Name: Add Product Matathon
*/
	$api_url = 'https://f0673c7df4e03cfddc091447829f9929:b0ded4729394d69f935ab426b244ba86@marathon-sports-dev.myshopify.com';

	$product_count = $api_url . '/admin/products/count.json';
	$product_count_json = file_get_contents($product_count);
	$count = json_decode( $product_count_json, true );
	$results = [];
	$product_result = [];
	$count_post = 0;

	//  shopify all product page with limit count 250
	if ($count['count'] > 0) {
		$pages = ceil($count['count'] / 50);
		var_dump($pages);
		// for ($i=0; $i < 3; $i++) {
		// 	array_push($results, $api_url . "/admin/products.json?limit=250&page=".($i+1));
		// }
		array_push($results, $api_url . "/admin/products.json?limit=50&page=110");
	}
	var_dump($results);

	foreach ($results as $key => $result_url) {
		// var_dump($result_url );
		$result_json = file_get_contents($result_url);
		$result = json_decode($result_json, true);

		foreach( $result['products'] as $key=>$data_json_decoder ) {
			$image = '';

			if (!empty($data_json_decoder['images'][0]['src'])) {
				$image = $data_json_decoder['images'][0]['src'];
			}

			$post_data = array(
				'comment_status' => 'closed',
				'ping_status'    => 'closed',
				'post_content'   => $data_json_decoder['body_html'],
				'post_name'      => $data_json_decoder['title'],
				'post_status'    => 'publish',
				'post_title'     => $data_json_decoder['title'],
				'post_type'      => 'product',
				'meta_input'     => array(
					'product_id' => $data_json_decoder['id'],
					'product_price' => $data_json_decoder['variants'][0]['price'],
					'product_vendor' => $data_json_decoder['vendor'],
					'product_handle' => $data_json_decoder['handle'],
					'product_thumbnail' => $image,
					'product_variant' => array(),
					'product_tags' => $data_json_decoder['tags'],
					'product_category' => array()
				)
			);

			// product variant images
			$option_index;
			$values = array();
			foreach ($data_json_decoder['options'] as $key => $option) {
				if ($option['name'] == 'Color' || $option == 'color') {
					$option_index = 'option' . ($key + 1);
				}
			}
			if (!empty($option_index)) {
				foreach ($data_json_decoder['variants'] as $key => $product_variant) {
					$variant_color = $product_variant[$option_index];
					$variant_image_scr = '//cdn.shopify.com/s/assets/no-image-2048-5e88c1b20e087fb7bbe9a3771824e743c244f437e4f8ba93bbf7b11b53f7824c_x200.gif';
					$variant_image_scr_small = '//cdn.shopify.com/s/assets/no-image-100-c91dd4bdb56513f2cbf4fc15436ca35e9d4ecd014546c8d421b1aece861dfecf_65x.gif';

					foreach ($data_json_decoder['images'] as $images) {
						$shop_url = get_field('shopify_site_url', 'option');
						$href = $shop_url . '/products/' . $data_json_decoder['handle'] . '?variant=' . $product_variant['id'];

						if ( !empty($images['variant_ids']) && $product_variant['id'] == $images['variant_ids'][0] ) {
							$variant_image_parts = pathinfo($data_json_decoder['images'][$key]['src']);
							$variant_image_scr = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_x200.' . $variant_image_parts['extension'];
							$variant_image_scr_small = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_65x.' . $variant_image_parts['extension'];
						}

						if ( !in_array($variant_color, $values) ) {
							array_push($values, $variant_color);

							$post_data['meta_input']['product_variant'][$key]['small'] = $variant_image_scr_small;
							$post_data['meta_input']['product_variant'][$key]['full'] = $variant_image_scr;
						}
					}
				}
			}

			// product reviews rating
			$product_reviews_url = $api_url . '/admin/products/' . $data_json_decoder['id'] . '/metafields.json?namespace=stamped&key=badge&fields=value';
			$product_reviews_url_json = file_get_contents($product_reviews_url);
			$product_reviews = json_decode($product_reviews_url_json, true);
			if (!empty($product_reviews["metafields"][0]['value'])) {
				$post_data['meta_input']['product_reviews'] = $product_reviews["metafields"][0]['value'];
			}

			// collection product
			$custom_collections_url = $api_url . '/admin/custom_collections.json';
			$custom_collections_json = file_get_contents($custom_collections_url);
			$custom_collections = json_decode($custom_collections_json);

			$product_categories = $api_url . '/admin/collects.json?product_id=' . $data_json_decoder['id'];
			$product_categories_json = file_get_contents($product_categories);
			$categories = json_decode($product_categories_json, true);
			$product_category = array();

			foreach ($custom_collections->custom_collections as $key => $custom_cat) {
				foreach ($categories['collects'] as $key => $cat) {
					if ( $custom_cat->id == $cat["collection_id"] ) {
						array_push($product_category, $custom_cat->title);
					}
				}
			}

			array_push($post_data['meta_input']['product_category'], $product_category);

			// array_push($product_result, $post_data);
			var_dump($post_data);
//			wp_insert_post( $post_data );
		}
	}
?>