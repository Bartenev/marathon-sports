<div class="breadcrumb-link">
    <div class="content">
        <div class="breadcrumb-link__inner">
	        <?php if (!is_404()): ?>
    	        <?php bcn_display(); ?>
            <?php endif; ?>
        </div>
        <div class="breadcrumb-link__note">
            <div class="swiper-container" data-breadcrumb>
                <ul class="swiper-wrapper">
                    <?php if ( have_rows('notifications', 'option') ) : ?>
                        <?php while(have_rows('notifications', 'option')) : the_row(); ?>
                            <?php if ( get_sub_field('show__hide', 'option') == 'show' ) : ?>
                                <?php
                                    $content_full = get_sub_field('content', 'option');
                                    $content = strlen($content_full) > 31 ? substr($content_full, 0, 30) . '...' : $content_full;
                                    $button_type = get_sub_field('link_type','option' );
                                    $link = ( $button_type == '_self' ) ? get_sub_field('page_link', 'option') : get_sub_field('url', 'option');
                                ?>
                                <li class="swiper-slide">
                                    <p><?php echo $content; ?> <a href="<?php echo $link; ?>" target="<?php echo $button_type; ?>"> Learn More</a></p>
                                </li>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="swiper-arrow swiper-arrow_prev color-dark-to">
                <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
            </div>
            <div class="swiper-arrow swiper-arrow_next color-dark-to">
                <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
            </div>
        </div>
    </div>
</div>