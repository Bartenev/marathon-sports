<?php get_header(); ?>
    <?php if ( get_field('show_breadcrumb') || is_search() ) : ?>
       <?php get_template_part('breadcrumbs'); ?>
    <?php endif; ?>

    <?php get_template_part( 'flexible-content-rows'); ?>
<?php get_footer(); ?>