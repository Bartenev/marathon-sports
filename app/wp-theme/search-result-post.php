<?php
    $id = get_the_id();
    $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'full_hd')[0];
 ?>
<li class="post-list-item" data-post>
    <div class="post-item">
        <?php if ( !empty($image) ) : ?>
            <a href="<?php the_permalink(); ?>" class="post-item__image">
                <div class="post-list-item__img bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
            </a>
        <?php endif; ?>
        <div class="post-item__content">
            <a href="<?php the_permalink(); ?>">
                <h2><?php the_title(); ?></h2>
            </a>
            <div class="post-list-item__content">
                <?php the_excerpt(); ?>
            </div>
            <a href="<?php the_permalink(); ?>" class="btn brand brand--color-white"><span>READ MORE</span></a>
        </div>
    </div>
</li>
