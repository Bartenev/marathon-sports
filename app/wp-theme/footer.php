<?php
/**
 * The template for displaying the footer.
 */

// $copy = get_field('copyright', 'option');

?>
<!--  Footer  -->
<footer class="footer">
    <div class="footer-global content">
        <div class="footer-top">
            <div>
                <div class="footer-list">
                    <h3 class="footer-list__title">company links</h3>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'company_links',
                            'container' => false,
                            'menu_class' => 'footer-nav menu_footer'
                        ) );
                    ?>
                </div>
                <div class="footer-list">
                    <h3 class="footer-list__title">helpful links</h3>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'helpful_links',
                            'container' => false,
                            'menu_class' => 'footer-nav menu_footer'
                        ) );
                    ?>
                </div>
                <?php if ( !empty(get_field('instagram', 'option')) || !empty(get_field('facebook', 'option')) ||!empty(get_field('youtube', 'option')) || !empty(get_field('twitter', 'option'))  ) : ?>
                    <div class="footer-list footer-list_social">
                        <h3 class="footer-list__title">stay connected</h3>
                        <ul class="menu_footer">
                            <?php if ( !empty(get_field('instagram', 'option')) ) : ?>
                                <li class="footer-list__item_social color-to">
                                    <a href="<?php echo get_field('instagram', 'option'); ?>" target="_blank">
                                    <span class="social social_instagram s0_soc">
                                        <svg width="144" height="144.6" viewBox="0 0 144 144.6">

                                            <defs>
                                                <clipPath clipPathUnits="userSpaceOnUse">
                                                    <path d="M0 108.5H108V0H0Z"/>
                                                </clipPath>
                                            </defs>
                                            <g transform="matrix(1.3333333,0,0,-1.3333333,0,144.63867)">
                                                <g clip-path="url(#clipPath18)">
                                                    <g transform="translate(65.2188,53.0605)">
                                                        <path d="m0 0c0 5.9-4.8 10.6-10.6 10.6-5.9 0-10.6-4.8-10.6-10.6 0-5.9 4.8-10.6 10.6-10.6C-4.8-10.6 0-5.9 0 0" fill="#fff"/>
                                                    </g>
                                                    <g transform="translate(76.7021,92.0703)">
                                                        <path d="m0 0c0 1.7 1.4 3 3 3h9.3c1.7 0 3-1.3 3-3v-9.3c0-1.6-1.3-3-3-3H3c-1.6 0-3 1.4-3 3zm-22.1-13.4c5.5 0 10.7-1.8 14.8-4.8h20.1 18.5v7.5c0 14.9-12.1 27-27 27h-54c-14.9 0-27-12.1-27-27v-7.5h39.7c4.2 3 9.3 4.8 14.8 4.8" fill="#fff"/>
                                                    </g>
                                                    <g transform="translate(80.2188,53.0605)">
                                                        <path d="m0 0c0-14.1-11.5-25.6-25.6-25.6-14.1 0-25.6 11.5-25.6 25.6 0 3.9 0.9 7.6 2.4 10.9h-31.4v-37c0-14.9 12.1-27 27-27H0.8c14.9 0 27 12.1 27 27V10.9H-2.4C-0.9 7.6 0 3.9 0 0" fill="#fff"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </span>
                                        <span>instagram</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ( !empty(get_field('facebook', 'option')) ) : ?>
                                <li class="footer-list__item_social color-to">
                                    <a href="<?php echo get_field('facebook', 'option'); ?>" target="_blank">
                                    <span class="social social_facebook s0_soc">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.97 66.36">
                                            <defs>
                                                <style>.cls-face{fill:#fff;}</style></defs>
                                             <g>
                                                <path id="_Контур_" data-name="&lt;Контур&gt;" class="cls-face" d="M20.42,19.91V14.6c0-2.59,1.72-3.19,2.93-3.19H30.8V0L20.55,0c-11.38,0-14,8.48-14,13.91v6H0V33.18H6.64V66.36H19.91V33.18h9.85L30.23,28,31,19.91Z"/>
                                             </g>
                                        </svg>
                                    </span>
                                        <span>facebook</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ( !empty(get_field('youtube', 'option')) ) : ?>
                                <li class="footer-list__item_social color-to">
                                    <a href="<?php echo get_field('youtube', 'option'); ?>" target="_blank">
                                    <span class="social social_youtube s0_soc">
                                        <svg width="88.5" height="64.9" viewBox="0 0 88.5 64.9"><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="M0 48.7H66.4V0H0Z"/></clipPath></defs><g transform="matrix(1.3333333,0,0,-1.3333333,0,64.8808)"><g clip-path="url(#clipPath18)"><g transform="translate(44.251,25.0381)"><path d="m0 0-17.9-9.7 0 14.9v4.5l8.1-4.4zm21.4 13.1c0 0-0.6 4.8-2.6 6.9-2.5 2.8-5.4 2.8-6.6 2.9-9.3 0.7-23.2 0.7-23.2 0.7h0c0 0-13.9 0-23.2-0.7-1.3-0.2-4.1-0.2-6.6-2.9-2-2.1-2.6-6.9-2.6-6.9 0 0-0.7-5.6-0.7-11.2V1.3-3.3c0-5.6 0.7-11.2 0.7-11.2 0 0 0.6-4.8 2.6-6.9 2.5-2.8 5.8-2.7 7.3-3 5.3-0.5 22.6-0.7 22.6-0.7 0 0 13.9 0 23.2 0.7 1.3 0.2 4.1 0.2 6.6 2.9 2 2.1 2.6 6.9 2.6 6.9 0 0 0.7 5.6 0.7 11.2v4.3 1c0 5.6-0.7 11.2-0.7 11.2" class="s0_soc"/></g></g></g></svg>
                                    </span>
                                        <span>youtube</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ( !empty(get_field('twitter', 'option')) ) : ?>
                                <li class="footer-list__item_social color-to">
                                    <a href="<?php echo get_field('twitter', 'option'); ?>" target="_blank">
                                    <span class="social social_twitter s0_soc">
                                        <svg viewBox="0 0 88.5 70.8"><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="M0 53.1H66.4V0H0Z"/></clipPath></defs><g transform="matrix(1.3333333,0,0,-1.3333333,0,70.780667)"><g clip-path="url(#clipPath18)"><g transform="translate(66.3604,46.8003)"><path d="m0 0c-2.4-1.1-5.1-1.8-7.8-2.1 2.8 1.7 5 4.3 6 7.4-2.6-1.5-5.5-2.7-8.6-3.3-2.5 2.6-6 4.2-9.9 4.2-7.5 0-13.6-6-13.6-13.4 0-1.1 0.1-2.1 0.3-3.1-11.3 0.6-21.3 5.9-28.1 14-1.2-2-1.8-4.3-1.8-6.7 0-4.6 2.4-8.7 6.1-11.2-2.2 0.1-4.3 0.7-6.2 1.7v-0.2c0-1.7 0.3-3.3 0.9-4.9 1.7-4.2 5.4-7.4 10-8.3-1.1-0.3-2.3-0.5-3.6-0.5-0.9 0-1.7 0.1-2.6 0.2 1.7-5.3 6.8-9.2 12.7-9.3-4.7-3.6-10.5-5.7-16.9-5.7-1.1 0-2.2 0.1-3.3 0.2 6-3.8 13.2-6 20.9-6 21.3 0 34.4 14.7 37.8 30 0.6 2.7 0.9 5.4 0.9 8.1 0 0.6 0 1.2 0 1.7C-4.1-5-1.8-2.7 0 0" class="s0_soc"/></g></g></g></svg>
                                    </span>
                                        <span>twitter</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <div class="footer-list footer-list_newsletter">
	            <?php
                    $newsletter_title = get_field('newsletter_title', 'option');
                    $newsletter_content = get_field('newsletter_content', 'option');
	            ?>
                <h3 class="footer-list__title "><?php echo $newsletter_title; ?></h3>
                <ul class="menu_footer">
                    <li>
			            <?php echo $newsletter_content; ?>
                    </li>
                    <li>
                        <?php
                             gravity_form( 1, false, false, false, '', true );
                        ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="footer-bottom">
            <ul class="footer-bottom__list">
                <?php if ( have_rows('payment_type', 'option') ) : ?>
                    <?php while (have_rows('payment_type', 'option')) : the_row();
                        $image_src = get_sub_field('payment', 'option');
                        $image_alt = get_sub_field('payment_type', 'option'); ?>
                        <li style="background-image: url(<?php echo $image_src; ?>);" title="<?php echo $image_alt; ?>" aria-label="<?php echo $image_alt; ?>"></li>
                    <?php endwhile; ?>
                <?php endif; ?>
            </ul>
            <span class="footer-bottom__copy">copyright &copy; <?php echo date("Y") . ' ' . get_field('copyright', 'option'); ?></span>
        </div>
    </div>
</footer>

<!--[if IE 8]>
<script>window.isMsIe = 8;</script><![endif]-->
<!--[if lte IE 7]>
<script>window.isMsIe = 7;</script><![endif]-->
<?php wp_footer();
$bottom_scripts = get_field('bottom_scripts', 'option');
if (strlen($bottom_scripts) && $_ENV['PANTHEON_ENVIRONMENT'] === 'live') {
  echo $bottom_scripts;
} ?>

</body>
</html>
