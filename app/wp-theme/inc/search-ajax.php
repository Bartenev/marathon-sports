<?php
// pagination post list
add_action( 'wp_ajax_post_list_pagination', 'post_list_pagination_callback' );
add_action( 'wp_ajax_nopriv_post_list_pagination', 'post_list_pagination_callback' );

function post_list_pagination_callback() {
	$page = $_POST['page'];
	$type = $_POST['type'];
	$search = $_POST['search'];
	$category = $_POST['cat'];
	$html = '';

	$args = array (
		'post_type' => $type,
		'orderby' => 'date',
		'order'   => 'DESC',
		'post_status' => 'publish',
		'posts_per_page' => 10,
		'paged' => $page = '' ? 1 : $page
	);
	if (!empty($search)) {
		$args['s'] = $search;
	}
	if ($type == 'eventbrite_events') {
		$args['tax_query'][] = array(
			'taxonomy' => 'eventbrite_category',
			'field'    => 'slug',
			'terms'    => array( 'run-group', 'race-team', 'run-club' ),
			'operator' => 'NOT IN',
		);

		$args['meta_key'] = 'event_start_date';		
		$args['orderby'] = 'meta_value';		
		$args['order'] = 'ASC';

		// Omit past events from query
        $args['meta_value'] = date('Y-m-d');            
        $args['meta_compare'] = '>='; 
	}
	if ( !empty($category) ) {
		if ( $type == 'post' ) {
			$args['category_name'] = $category;
		}
		if ( $type == 'runners_resources' ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'runners_resources_categories',
				'field' => 'term_id',
				'terms' => $category,
			);
		}
		if ( $type == 'eventbrite_events' ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'eventbrite_category',
				'field' => 'term_id',
				'terms' => $category,
			);
		}
	}

	$query = new WP_Query( $args );
	$p_count = $query->max_num_pages;

	if ( $query->have_posts() ) {

		while( $query->have_posts()) {
			$query->the_post();
			if ( $type !== 'eventbrite_events' ) {
				$html .= '<li class="post-list-item">';
				$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full_hd')[0];
				if ( !empty($image) ) {
					$html .= '<div class="post-list-item__img bg-cover" style="background-image: url(' . $image . ');"></div>';
				}
				$html .= '<h2>' . get_the_title() . '</h2>
                      <p>'. get_the_excerpt() . '</p>
                      <a href="' . get_the_permalink() . '" class="btn brand brand--color-white"><span>READ MORE</span></a>
					</li>';
			}else {
				$venue_location = get_post_meta( get_the_ID(), 'venue_address' )[0];

				$event_date           = get_post_meta( get_the_ID(), 'event_start_date' )[0];
				$event_date_formatted = date( "m/d/Y", strtotime( $event_date ) );

				$events_start_hour    = get_post_meta( get_the_ID(), 'event_start_hour' )[0];
				$events_start_minute  = get_post_meta( get_the_ID(), 'event_start_minute' )[0];
				$event_start_meridian = get_post_meta( get_the_ID(), 'event_start_meridian' )[0];
				$event_start_time     = $events_start_hour . ":" . $events_start_minute . $event_start_meridian;

				$events_end_hour    = get_post_meta( get_the_ID(), 'event_end_hour' )[0];
				$events_end_minute  = get_post_meta( get_the_ID(), 'event_end_minute' )[0];
				$event_end_meridian = get_post_meta( get_the_ID(), 'event_end_meridian' )[0];
				$event_end_time     = $events_end_hour . ":" . $events_end_minute . $event_end_meridian;

				$event_url = get_post_meta( get_the_ID(), 'iee_event_link' )[0];

				$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full_hd' )[0];

				$html .= '<li class="post-list-item">
                            <a href="' . $event_url .'">
                                 <div class="post-list-item__img bg-cover"
                                 style="background-image: url(' .  $image . ');"></div>
                            </a>
                           <span class="post-list-item__date">' . $event_date_formatted . '</span>
                           <a href="' . $event_url . '" target="_blank">
                                <h2>' . get_the_title() . '</h2>
                           </a>
                           <div class="post-list-item__content">' . get_the_excerpt() . '</div>
                           <div class="post-list-item__info">';

				if ( !empty( $event_start_time ) || !empty( $event_end_time ) ) {
					$html .= '<span><strong>Time: </strong>' . $event_start_time . ' - ' . $event_end_time . '</span>';
				}
				if ( !empty( $venue_location ) ) {
					$html .= ' <span><strong>Location: ' . $venue_location . '</strong></span>';
				}

				$html .= '</div>
                          <a href="' . $event_url . '" class="btn brand brand--color-white" target="_blank"><span>REGISTER</span></a>
                          </li>';
			}

		}

		if ($p_count > 1) {
			$html .= '<li class="post-list-nav">';
			if ( $page != 1 ) {
				$html .= '<a data-page="prev" class="post-list-nav_arrow">previous</a>';
			}

			for ($x = 1; $x <= $p_count; $x++) {
				$html .= '<a class="post-list-nav__number" data-page="'. $x .'">'. $x .'</a>';
			}

			if ( $page != $p_count ) {
				$html .=  '<a data-page="next" class="post-list-nav_arrow">next</a>';
			}
			$html .= '</li>';
		}
	}

	if ( $html != '' ) {
		echo  $html;
		die();
	}

	echo '<p class="not-result">We couldn\'t find any result for "' . $search . '"</p>';
	die();
}
?>