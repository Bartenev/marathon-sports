<?php
	function custom_post_types_product() {
		$labels = array(
			'name'                  => _x( 'Product', 'Post Type General Name', 'thirtysix' ),
			'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'thirtysix' ),
			'menu_name'             => __( 'Product', 'thirtysix' ),
			'name_admin_bar'        => __( 'Product', 'thirtysix' ),
			'archives'              => __( 'Archives', 'thirtysix' ),
			'attributes'            => __( 'Attributes', 'thirtysix' ),
			'parent_item_colon'     => __( 'Parent Item:', 'thirtysix' ),
			'all_items'             => __( 'All Product', 'thirtysix' ),
			'add_new_item'          => __( 'Add New Product', 'thirtysix' ),
			'add_new'               => __( 'Add New', 'thirtysix' ),
			'new_item'              => __( 'New Person', 'thirtysix' ),
			'edit_item'             => __( 'Edit Person', 'thirtysix' ),
			'update_item'           => __( 'Update Person', 'thirtysix' ),
			'view_item'             => __( 'View Person', 'thirtysix' ),
			'view_items'            => __( 'View Person', 'thirtysix' ),
			'search_items'          => __( 'Search Event', 'thirtysix' ),
			'not_found'             => __( 'Not found', 'thirtysix' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'thirtysix' ),
			'featured_image'        => __( 'Featured Image', 'thirtysix' ),
			'set_featured_image'    => __( 'Set featured image', 'thirtysix' ),
			'remove_featured_image' => __( 'Remove featured image', 'thirtysix' ),
			'use_featured_image'    => __( 'Use as featured image', 'thirtysix' ),
			'insert_into_item'      => __( 'Insert into item', 'thirtysix' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'thirtysix' ),
			'items_list'            => __( 'Items list', 'thirtysix' ),
			'items_list_navigation' => __( 'Items list navigation', 'thirtysix' ),
			'filter_items_list'     => __( 'Filter items list', 'thirtysix' ),
		);
	
		$args = array(
			'label'                 => __( 'Product', 'thirtysix' ),
			'description'           => __( 'Product for our work', 'thirtysix' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies'            => array( 'awards_category' ),
			'hierarchical'          => true,
			'public'                => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_icon'             => 'dashicons-screenoptions',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'capability_type'       => 'post',
			'publicly_queryable'    => true,
			'rewrite'               => false,
		);
		register_post_type( 'product', $args );
	}
	add_action( 'init', 'custom_post_types_product' );
?>