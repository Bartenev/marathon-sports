<?php
	function more_post_ajax(){

	    header("Content-Type: text/html");

	    $page = $_POST['pageNumber'];
	    $s = $_POST['searchQuery'];
		$types = !empty($_POST['postType']) ? $_POST['postType'] : array('post', 'locations');

	    $args = array(
			'post_type' => $types,
			'posts_per_page' => 6,
	        's' => $s,
			'paged' => $page,
			'meta_query' => array('relation' => 'or')
	    );
	    if ( !empty($_POST['metaQuery']) ) {
	    	foreach ( $_POST['metaQuery'] as $meta ) {
			    $meta_query = array(
				    'key' => 'product_vendor',
				    'value' => $meta,
				    'compare' => '='
			    );

			    $args['meta_query'][] = $meta_query;
		    }
	    }

	    $query = new WP_Query($args);
	    $out = '';

	    if ($query -> have_posts()) :  while ($query -> have_posts()) : $query -> the_post();

			$next = get_next_post(true, '');
			$end = (($query->current_post +1) == ($query->post_count)) ? 'true' : 'false';

//	        if( get_post_type() == 'product' ) {
//	            get_template_part('search-result', 'product');
//	        }
//	        else {
//	          get_template_part('search-result', 'post');
//	        }
		    get_template_part('search-result', 'post');

	    endwhile;
	    endif;
	    wp_reset_query();
	    wp_reset_postdata();
	    exit();
	}

	add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
	add_action('wp_ajax_more_post_ajax', 'more_post_ajax');
 ?>
