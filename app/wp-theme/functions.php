<?php
function setup() {
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary' => 'Header',
        'company_links'  => 'Company Links',
        'helpful_links'  => 'Helpful Links'
    ) );
    // See: https://codex.wordpress.org/Function_Reference/add_theme_support
    add_theme_support('post-thumbnails');
    add_image_size( 'full_hd', 1920, 0, false );
    add_image_size( 'location', 350, 0, false );

    if (function_exists('acf_add_options_page')) {

      $args = array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Theme Settings',
        'position' => 3,
        'icon_url' => 'dashicons-admin-site',
      );

      acf_add_options_page( $args );
    }

//    include('inc/shopify-product.php');
}
include('inc/search-ajax.php');
include('inc/infinite-search.php');

add_action( 'after_setup_theme', 'setup' );
// custom excerpt
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
function _excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', '_excerpt_length', 999 );


add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
    return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
    $content = ' }, false );';
    return $content;
}

add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );
/**
 * Changes the default Gravity Forms AJAX spinner.
 */
function tgm_io_custom_gforms_spinner( $src ) {
    return get_stylesheet_directory_uri() . '/images/ajax-loader.gif';

}


// Filter out the `medium_large` image size
add_filter( 'intermediate_image_sizes', function( $sizes ) {
    return array_filter( $sizes, function( $val ) {
        return 'medium_large' !== $val;
    });
});


/**
 * Enqueues scripts and styles for front end.
 */
function add_async_attribute($tag, $handle) {
    if ( 'minified-js' !== $handle )
        return $tag;
    return str_replace( ' src', ' async="async" src', $tag );
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function scripts_styles() {
    wp_enqueue_script(
        'manifest-js',
        get_template_directory_uri() . '/js/manifest.js',
        array(),
        filemtime(get_template_directory() . '/js/manifest.js'),
        true
    );
    wp_enqueue_script(
        'vendor-js',
        get_template_directory_uri() . '/js/vendor.js',
        array(),
        filemtime(get_template_directory() . '/js/vendor.js'),
        true
    );
    wp_enqueue_script(
        'app-js',
        get_template_directory_uri() . '/js/app.js',
        array(),
        filemtime(get_template_directory() . '/js/app.js'),
        true
    );
    wp_enqueue_style(
        'styles',
        get_template_directory_uri() . '/css/style.css',
        array(),
        filemtime(get_template_directory() . '/css/style.css'),
        false
    );
}
add_action( 'wp_enqueue_scripts', 'scripts_styles' );

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

 add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
 function form_submit_button( $button, $form ) {
     if ( $form['id'] == 1 ) {
	     return "<button class='footer-list__submit color-dark-to' id='gform_submit_button_{$form['id']}'>
                    <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 44.12 32.67\" height=\"20\">
                        <defs>
                            <style>
                                .form-arrow,.form-arrow-2{fill:none;stroke:#000;stroke-linecap:round;stroke-width:4px;}
                                .form-arrow{stroke-linejoin:round;}
                                .form-arrow-2{stroke-miterlimit:10;}
                            </style>
                        </defs>
                        <g id=\"svg-7\" data-name=\"svg-7\">
                            <g>
                                <path class=\"form-arrow\" d=\"M2,15.9H24.11\"/>
                                <path class=\"form-arrow\" d=\"M41.78,15.9h.33L24.45,30.67\"/>
                                <line class=\"form-arrow-2\" x1=\"42.12\" y1=\"15.9\" x2=\"25.45\" y2=\"2\"/>
                            </g>
                        </g>
                    </svg>
                 </button>";
     }else if ( $form['id'] == 2) {
         return "<button class='btn brand brand--color-white' id='gform_submit_button_{$form['id']}'>
                     <span>Submit</span>
                </button>";
     }
 }
add_filter('gform_init_scripts_footer', '__return_true');

add_filter( 'gform_tabindex_10', 'change_tabindex' , 10, 2 );
function change_tabindex( $tabindex, $form ) {
    if ( $form['id'] == 1 ) {
	    return 2;
    }else if ( $form['id'] == 2 ) {
	    return 1;
    }
}
add_filter( 'gform_tabindex', '__return_false' );

/**
 * Menu Walker
 */
class _walker_nav_menu extends Walker_Nav_Menu {

    var $number = 1;

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = $class_names ? ' class="link ' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names .'>';

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;
        $item_output .= '<div><a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= '<i class="link__hover"></i></div>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' );
        $display_depth = ( $depth + 1);
        $classes = array(
            'sub-menu'
            );
        $class_names = implode( ' ', $classes );

        $output .= "\n" . $indent . '<div class="menu__dropdown"><ul class="' . $class_names . '">' . "\n";
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "</ul></div>\n";
    }
}

// custom excerpt
function new_excerpt_length($length) {
    return substr($length, 0, 85) . '...';
}
add_filter('excerpt_length', 'new_excerpt_length');



/**
 * Add REST API support to an already registered post type.
 */
add_action( 'init', 'my_custom_post_type_rest_support', 25 );
function my_custom_post_type_rest_support() {
    global $wp_post_types;

    //be sure to set this to the name of your post type!
    $post_type_name = 'events';
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }
}

add_action('init', 'remove_editor_init');
function remove_editor_init() {
    remove_post_type_support('page', 'editor');
}

add_filter('acf/settings/google_api_key', function () {
	return 'AIzaSyDg8dpleH1YYqlRzebf2CgRaBRzirmN0gs';
});

// resources ajax

add_action('wp_head','add_ajaxurl');
function add_ajaxurl() {
	?>
	<script type="text/javascript">
        var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
	<?php
}

// widget post list
function childtheme_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar News Page', 'twentyeleven' ),
		'id' => 'sidebar-item-1',
		'description' => __( 'Widget For News List Sidebar', 'twentyeleven' ),
		'before_widget' => '<ul class="sidebar__item">',
		'after_widget' => '</ul>',
		'before_title' => '<h3 class="sidebar__title">',
		'after_title' => '</h3>'
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar Upcoming Events Page', 'twentyeleven' ),
		'id' => 'sidebar-item-2',
		'description' => __( 'Widget For Events List Sidebar', 'twentyeleven' ),
		'before_widget' => '<ul class="sidebar__item">',
		'after_widget' => '</ul>',
		'before_title' => '<h3 class="sidebar__title">',
		'after_title' => '</h3>'
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar Runner’s Resource Page', 'twentyeleven' ),
		'id' => 'sidebar-item-3',
		'description' => __( 'Widget For Runner’s Resource List Sidebar', 'twentyeleven' ),
		'before_widget' => '<ul class="sidebar__item">',
		'after_widget' => '</ul>',
		'before_title' => '<h3 class="sidebar__title">',
		'after_title' => '</h3>'
	) );
}
add_action('init', 'childtheme_widgets_init');

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'share_widget' );
	register_widget( 'categories_widget' );
	register_widget( 'past_events_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget Share Post
class share_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

            // Base ID of your widget
			'share_widget',

            // Widget name will appear in UI
			__('Share Widget', 'wpb_widget_domain'),

            // Widget description
			array( 'description' => __( 'Custom Share Widget', 'wpb_widget_domain' ), )
		);
	}

    // Creating widget front-end
	public function widget( $args, $instance ) {
		$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$title = apply_filters( 'widget_title', $instance['title'] );
		$social_list = '<ul class="share-widget">
                            <li class="share-widget__facebook">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=' . $url . '" target="_blank" data-map="facebook">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.97 66.36">
                                            <defs>
                                                <style>.cls-face{fill:#fff;}</style></defs>
                                             <g>
                                                <path id="_Контур_" data-name="&lt;Контур&gt;" class="cls-face" d="M20.42,19.91V14.6c0-2.59,1.72-3.19,2.93-3.19H30.8V0L20.55,0c-11.38,0-14,8.48-14,13.91v6H0V33.18H6.64V66.36H19.91V33.18h9.85L30.23,28,31,19.91Z"/>
                                             </g>
                                        </svg>
                                    </span>
                                    facebook
                                </a>
                            </li>
                            <li class="share-widget__twitter">
                                <a href="https://twitter.com/home?status=' . $url . '" target="_blank">
                                    <span>
                                        <svg viewBox="0 0 88.5 70.8"><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="M0 53.1H66.4V0H0Z"/></clipPath></defs><g transform="matrix(1.3333333,0,0,-1.3333333,0,70.780667)"><g clip-path="url(#clipPath18)"><g transform="translate(66.3604,46.8003)"><path d="m0 0c-2.4-1.1-5.1-1.8-7.8-2.1 2.8 1.7 5 4.3 6 7.4-2.6-1.5-5.5-2.7-8.6-3.3-2.5 2.6-6 4.2-9.9 4.2-7.5 0-13.6-6-13.6-13.4 0-1.1 0.1-2.1 0.3-3.1-11.3 0.6-21.3 5.9-28.1 14-1.2-2-1.8-4.3-1.8-6.7 0-4.6 2.4-8.7 6.1-11.2-2.2 0.1-4.3 0.7-6.2 1.7v-0.2c0-1.7 0.3-3.3 0.9-4.9 1.7-4.2 5.4-7.4 10-8.3-1.1-0.3-2.3-0.5-3.6-0.5-0.9 0-1.7 0.1-2.6 0.2 1.7-5.3 6.8-9.2 12.7-9.3-4.7-3.6-10.5-5.7-16.9-5.7-1.1 0-2.2 0.1-3.3 0.2 6-3.8 13.2-6 20.9-6 21.3 0 34.4 14.7 37.8 30 0.6 2.7 0.9 5.4 0.9 8.1 0 0.6 0 1.2 0 1.7C-4.1-5-1.8-2.7 0 0" class="s0_soc"/></g></g></g></svg>
                                    </span>
                                    twitter
                                </a data-map="twitter">
                            </li>
                        </ul>';

    // before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

    // This is where you run the code and display the output
		echo __( $social_list, 'wpb_widget_domain' );
		echo $args['after_widget'];
	}

    // Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
    // Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
		<?php
	}

    // Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here


// Creating the widget Category Post
class categories_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

		// Base ID of your widget
			'categories_widget',

			// Widget name will appear in UI
			__('Categories Widget', 'wpb_widget_domain'),

			// Widget description
			array( 'description' => __( 'Custom Categories Widget', 'wpb_widget_domain' ), )
		);
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$category = $instance['category'];
		$cat_name = '';

		if (!empty( get_the_category(get_the_ID()) )) {
			$cat_name = get_the_category(get_the_ID())[0]->taxonomy;
		}else if ( !is_search() ) {
			$cat_name = get_queried_object()->taxonomy;
        }

		if (get_sub_field('choose_post_type')) {
		    $cat_name = $category;
			get_terms('eventbrite_category');
        }
        if (is_single()) {
	        $cat_name = get_post_taxonomies(get_the_ID())[0];
	        if  (get_post_taxonomies(get_the_ID())[1] == 'events_categories') {
		        $cat_name = 'events_categories';
            }
        }
        if (is_search()) {
	        $cat_name = array(
                'category',
		        'runners_resources_categories',
                'eventbrite_category'
            );
        }
		$cat_list = get_terms($cat_name);
		$html = '';

		foreach ($cat_list as $cat) {
			$html .= '<li class="cat-item"><a href="' . get_term_link( $cat ) . '">'. $cat->name .'</a></li>';
        }

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output
		echo __( '<ul data-cat="' . $cat_name . '">' . $html . '</ul>', 'wpb_widget_domain' );
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		if  ( isset( $instance[ 'category' ] ) ) {
			$category = $instance[ 'category' ];
        }
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
			$category =  __( 'category', 'wpb_widget_cat' );
		}
		// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:' ); ?></label>
            <select class="widefat" id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>">
                <option <?php if ( $category == 'category' ) : ?>selected="selected" <?php endif; ?>value="category">News Category</option>
                <option <?php if ( $category == 'runners_resources_categories' ) : ?>selected="selected" <?php endif; ?> value="runners_resources_categories">Runners Resources Categories</option>
                <option <?php if ( $category == 'eventbrite_category' ) : ?>selected="selected" <?php endif; ?> value="eventbrite_category">Event Categories</option>
            </select>
        </p>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here


// Creating the widget Past Events
class past_events_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

		// Base ID of your widget
			'past_events',

			// Widget name will appear in UI
			__('Past Events', 'wpb_widget_domain'),

			// Widget description
			array( 'description' => __( 'Custom Past Events', 'wpb_widget_domain' ), )
		);
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$past_post = '';

		$args_post = array(
			'posts_per_page' => 3,
			'post_type' => 'eventbrite_events',
			'meta_key' => 'event_end_date',
			'orderby' => 'meta_value',
			'order' => 'ASC',
			'meta_query' =>  array(
				array(
					'key' => 'event_end_date',
					'value' =>  date("Y-m-d H:i:s"),
					'compare' => '<',
					'type'=> 'DATETIME'
				)
			)
		);

		$query = new WP_Query( $args_post );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$past_post .= '<li>
                                <a href="' . get_the_permalink() . '" class="wpp-post-title">' . get_the_title() . '</a>
                                <span class="wpp-excerpt">'. get_the_excerpt() . '</span>
                                <a href="' . get_the_permalink() . '" class="post-link">READ MORE</a>
                             </li>';
            }
        }
		wp_reset_query();

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output
		echo __( '<ul class="wpp-list">' . $past_post . '</ul>', 'wpb_widget_domain' );
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form
		?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            </p>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here


add_filter( 'get_search_form', 'my_search_form' );
function my_search_form( $form ) {

	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . esc_url( home_url( '/' ) ) . '">
                <div>
                    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search" />
                    <button type="submit" id="searchsubmit">
                        <span class="trigger-search color-dark-to">
                             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                                <defs>
                                    <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                                </defs>
                                <g id="svg-1" data-name="svg-1">
                                    <g>
                                        <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"/>
                                    </g>
                                </g>
                            </svg>
                        </span>
                    </button>
                </div>
            </form>';

	return $form;
}

add_action( 'rest_api_init', function() {
	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
	add_filter( 'rest_pre_serve_request', function( $value ) {
		$origin = get_http_origin();
		if ($origin && in_array($origin, [
				// acceptable origins
				'https://dev-marathon-sports.pantheonsite.io','https://test-marathon-sports.pantheonsite.io','https://live-marathon-sports.pantheonsite.io',
				'https://www.marathonsports.com','https://shop.marathonsports.com','https://marathon-sports-dev.myshopify.com','https://marathonsports.myshopify.com',
				'https://dev-runners-alley.pantheonsite.io','https://test-runners-alley.pantheonsite.io','https://live-runners-alley.pantheonsite.io',
				'https://dev-sound-runner.pantheonsite.io','https://test-sound-runner.pantheonsite.io','https://live-sound-runner.pantheonsite.io',
				'https://runnersalley.com','https://soundrunner.com'
			])) {
			header( 'Access-Control-Allow-Origin: ' . esc_url_raw( $origin ) );
			header( 'Access-Control-Allow-Methods: GET' );
			header( 'Access-Control-Allow-Credentials: true' );
		}
		return $value;
	});
}, 15 );


// Set custom Google Maps API key for ACF admin
function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyDVCq7mKYJBT40JZeT2cX9kqx9ia-mn-V4';
	return $api;	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');