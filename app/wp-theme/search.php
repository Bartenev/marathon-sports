<?php
	get_header();

    $image = wp_get_attachment_image_src(get_field('background_search', 'option'), 'full')[0];

	$s = get_search_query();
	$types = array('post', 'locations', 'run_clubs', 'eventbrite_events');

	$args = array(
		'post_type' => $types,
		'posts_per_page' => 6,
        's' => $s,
		'paged' => 1,
    );
   	$query = new WP_Query($args);
	$p_count = $query->found_posts;
	$post_count = $query->found_posts;
    $theme_logo = get_field('logo', 'option');

	$shop_url_search = get_field('shopify_site_url', 'option') . '/search?q=' . $s . '&type=product' . '&from=' . $theme_logo;
?>

<!--  Page Header  -->
<div class="search-page-header">
    <div class="content">
        <h1>Search results for
			<span class="page-header__highlight"><?php echo $s; ?></span> (<?php echo $post_count; ?> items)
            <p class="page-header__search">Looking for a product? <a href="<?php echo $shop_url_search; ?>" data-search="shopify">Search our Shop</a></p>
		</h1>
    </div>
</div>
<div class="search-wrap content" id="content">
	<?php
	if ( $query->have_posts() ) {

		echo '<ul class="post-list post-list_search" data-ajax-container data-ajax-page="1" data-ajax-search="'.$s.'" data-max-page="' . $query->max_num_pages . '">';

		while ( $query->have_posts() ) {
			$query->the_post();
			if ( get_post_type() == "eventbrite_events" ) {
				get_template_part('search-result', 'event');
			} else {
				get_template_part('search-result', 'post');
			}						
		}
		echo '</ul>';
		wp_reset_postdata();
	}else {
		echo '<div class="not-result"><p>no posts found</p></div>';
	}
	?>
    <div class="page-load-status">
        <div class="loader-ellips infinite-scroll-request">
            <span class="loader-ellips__dot"></span>
            <span class="loader-ellips__dot"></span>
            <span class="loader-ellips__dot"></span>
            <span class="loader-ellips__dot"></span>
        </div>
    </div>
</div>

 <?php get_footer(); ?>
