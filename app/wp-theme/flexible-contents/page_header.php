<?php
	$image = wp_get_attachment_image_src(get_sub_field('background_image'), 'full_hd')[0];
    $image_overlay = get_sub_field('background_overlay');
	$section_type = get_sub_field('section_type');
	$title_type = get_sub_field('title_type');
	$title = get_sub_field('title');
	$image_position = get_sub_field('image_position');
?>
<!--  Page Header  -->
<div class="page-header <?php echo ( $section_type == 'small' ) ? 'page-header_small' : ''; ?> bg-cover <?php echo $image_position != 'center' ? 'bg-cover' . $image_position : ''; ?>" style="background-image: url(<?php echo $image; ?>);">
	<?php if ( $image_overlay ) : ?>
        <div class="page-header__overlay"></div>
	<?php endif; ?>
	<?php if( $section_type == 'small' ) : $bg2 =  wp_get_attachment_image_src(get_sub_field('mobile_background'), 'large')[0];?>
		<div class="page-header__add-bg bg-cover" style="background-image: url(<?php echo $bg2; ?>);" ></div>
	<?php endif; ?>
	<div class="content">
		<h1 style="text-transform: <?php echo $title_type; ?>;"><?php echo $title; ?></h1>
	</div>
</div>