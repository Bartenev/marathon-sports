<!-- Full Width Image -->
<?php
    $image = wp_get_attachment_image_src(get_sub_field('image')['ID'], 'full_hd')[0];
    $image_overlay = get_sub_field('image_overlay');
    $image_position = get_sub_field('image_position');
    $content = get_sub_field('content');
    $align_content = get_sub_field('align_content');
    $content_position = get_sub_field('content_position');    
    
    // Get button clone field options    
    $button_text = get_sub_field( 'linked_button_button_text' );

    $link_type = get_sub_field( 'linked_button_link_type' );
    if ( $link_type == "internal" ) {
        $button_link = get_sub_field( 'linked_button_page_link' );
    } else {
        $button_link = get_sub_field( 'linked_button_link_url' );
    }

    $link_behavior = get_sub_field( 'linked_button_link_behavior' );
    if ( $link_behavior[0] == "blank" ) { 
        $open_in_new_tab = true; 
    } else {
        $open_in_new_tab = false;
    }  
?>
<div class="full-width-image bg-cover <?php echo $image_position != 'center' ? 'bg-cover' . $image_position : ''; ?>" style="background-image: url('<?php echo $image; ?>');">
    <?php if ( $image_overlay ) : ?>
        <div class="full-width-image__overlay"></div>
    <?php endif; ?>
    <div class="content <?php echo 'content_' . $align_content; ?>">
        <div class="full-width-image__inner <?php echo $content_position; ?> <?php echo get_the_id() === 7191 ? 'full-width-image__inner_full': ''; ?>">
            <div><?php echo $content; ?></div>
            <?php if ( $button_text ) { ?>
                <a href="<?php echo $button_link; ?>" <?php if ( $open_in_new_tab ) { echo "target='_blank'"; } ?> class="btn brand brand--color-white">
                    <span><?php echo $button_text; ?></span>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
