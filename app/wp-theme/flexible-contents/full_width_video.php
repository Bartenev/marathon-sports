<?php
    $video_type = get_sub_field('video_type');
    $video_id = get_sub_field('video_id');
    $poster = wp_get_attachment_image_src(get_sub_field('video_poster'), 'full_hd')[0];
    $bind_id = rand();
?>
<!--  Full Width Video  -->
<div class="full-width-video">
	<div class="full-width-video__wrap">
		<div class="full-width-video__poster" style="background-image:url(<?php  echo $poster;  ?>);"></div>
		<span class="full-width-video__control" data-type="<?php echo $video_type; ?>" data-video="<?php echo $video_id; ?>" data-lightbox-bind="lightbox-<?php echo $bind_id; ?>">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 93.57 93.61">
                <defs>
                    <style>
                        .play-1,.play-2{fill:none;stroke:#fff;stroke-linecap:round;stroke-width:4px;}
                        .play-1{stroke-linejoin:round;}.play-2{stroke-miterlimit:10;}
                    </style>
                </defs>
                <g id="play" data-name="play">
                    <g id="Layer_1" data-name="Layer 1">
                        <polygon class="play-1" points="36.69 30.01 36.69 63.61 62.93 46.81 36.69 30.01"/>
                        <path class="play-2" d="M46,91.61A44.81,44.81,0,1,1,91.57,44.66"/>
                    </g>
                </g>
            </svg>
        </span>
	</div>
</div>
<div class="lightbox lightbox-product-video" id="lightbox-<?php echo $bind_id; ?>">
	<div class="lightbox__content">
		<div class="lightbox__poster">
			<span class="lightbox__closer"></span>
			<div class="lightbox__poster" style="background-image:url(<?php  echo get_template_directory_uri(); ?>/media/video-preview.jpg);"></div>
		</div>
	</div>
</div>