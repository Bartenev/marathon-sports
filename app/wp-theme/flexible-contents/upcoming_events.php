<!-- Upcoming events -->
<?php
    $title = get_sub_field('title');
    $sub_title = get_sub_field('sub_title');
    $button_type = get_sub_field('button_type');
    $link = ( $button_type == '_self' ) ? get_sub_field('page_link') : get_sub_field('url');
    $button_label = get_sub_field('button_label');
    $events = get_sub_field('events');
    $custom = get_sub_field('custom', false, false);
    $header_type = get_sub_field('header_type') == 'true' ? true : false;
?>
<div class="title-row <?php echo !$header_type ? 'title-row_heading' : 'title-row_light'?>">
    <div class="content">
        <h2>
            <?php echo $title; ?>
            <span><?php echo $sub_title; ?></span>
        </h2>
        <?php if($header_type): ?>
            <a href="<?php echo $link; ?>" target="<?php echo $button_type; ?>" class="brand-dark-to"><?php echo $button_label ?></a>
        <?php endif; ?>
    </div>
</div>
<div class="upcoming-events <?php echo !$header_type ? 'upcoming-events_no-top-padding' : ''?>">
    <div class="upcoming-events__inner">
        <div class="content swiper-container" data-event data-brand-color="<?php echo $GLOBALS['theme_color']; ?>">
            <div class="swiper-wrapper">
                <?php
                    $args = [
                        'post_type' => 'events',
                        'orderby' => 'event_data',
                        'order' => 'ASC'
                    ];

                    if ($events == 'most_recent') {
                        $args['posts_per_page'] = 12;
                        $args['meta_query'] = array(
                            array(
	                            'key' => 'event_data',
	                            'value' =>  date("Y-m-d H:i:s"),
	                            'compare' => '>=',
	                            'type'=> 'DATETIME'
                            )
                        );
                    }else {
                        $args['posts_per_page'] = count($custom);
                        $args['post__in'] = $custom;
                    }

                    $query = new WP_Query( $args );
                ?>
                <?php if ( $query->have_posts() ) : ?>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <?php
                            $data_event = get_post_meta($post->ID, 'event_data')[0];
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id( $post_id ), 'full_hd')[0];
                            $categories = get_the_terms($post->ID, 'event_location');
                            $content = get_the_content();
                        ?>
                        <a href="<?php the_permalink(); ?>" class="upcoming-events__item swiper-slide">
                            <div class="upcoming-events__date brand brand--color-white">
                                <p><?php echo date('M', strtotime($data_event)); ?></p>
                                <span><?php echo date('d', strtotime($data_event)); ?></span>
                            </div>
                            <div class="upcoming-events__image">
                                <div class="bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
                            </div>
                            <div class="upcoming-events__body brand-to brand--color-white">
                                <h2 class="upcoming-events__title">
                                    <?php the_title(); ?>
                                    <?php if ( !empty($categories) ) : ?>
                                        <span>
                                            <?php foreach ($categories as $key=>$category) : ?>
                                                <?php echo ($key != 0) ? ', ' . $category->name : $category->name; ?>
                                            <?php endforeach; ?>
                                        </span>
                                    <?php endif; ?>
                                </h2>
                                <p class="upcoming-events__description"><?php echo new_excerpt_length($content); ?></p>
                                <span class="upcoming-events__link">learn more</span>
                            </div>
                        </a>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
        <?php if ( ($query->post_count) > 4 ) : ?>
            <div class="swiper-arrow swiper-arrow_prev color-dark-to">
                <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
            </div>
            <div class="swiper-arrow swiper-arrow_next color-dark-to">
                <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
            </div>
        <?php endif; ?>
    </div>
</div>