<!--  Simple Text Content With Heading  -->

<?php get_sub_field( 'accordion_footer_text' ) ? $has_footer = true : $has_footer = false; ?>
<div class="content">
  <div class="heading-width-text">
    <h2 class="heading-width-text__title">
      <?php echo get_sub_field( 'text_headline' ); ?>
      <i class="title-bar brand"></i>
    </h2>
    <div class="heading-width-text__article">
      <?php echo get_sub_field( 'description' ); ?>
      <?php if ( get_sub_field( 'add_accordion' ) ): ?>
        <?php $type = get_sub_field( 'accordion_type' ); ?>
        <?php if ( have_rows( 'accordion' ) ): ?>
          <ul class="c-accordion c-accordion_<?php echo $type; if ( $has_footer ) { echo ' has-footer'; } ?>">
            <?php while ( have_rows( 'accordion' ) ) :
              the_row(); ?>
              <?php if ( get_sub_field('section_heading') ) { ?>
                <li class="c-accordion__item c-accordion__section-heading">
                  <h3><?php the_sub_field('section_heading'); ?></h3>
                </li>
              <?php } ?>
              <li class="c-accordion__item">
                <div class="c-accordion__title brand brand--color-white">
                  <?php echo get_sub_field( 'title' ) ?>
                  <span></span>
                </div>
                <div class="c-accordion__inner">
                  <div class="c-accordion__article">
                    <div class="c-accordion__article-inner">
                      <?php echo get_sub_field( 'content' ); ?>
                    </div>
                  </div>
                  <?php $button = get_sub_field( 'button' );
                  if ( $button ): ?>
                  <a href="<?php echo $button['url'] ?>" <?php echo $button['target'] ?>
                    class="btn">
                    <span><?php echo $button['text'] ?></span>
                  </a>
                <?php endif; ?>

              </div>
            </li>
          <?php endwhile; ?>
        </ul>
        <?php if ( $has_footer ) { ?>
          <div class="c-accordion__footer-text">
            <?php the_sub_field( 'accordion_footer_text' ); ?>
          </div>
        <?php } ?>
      <?php endif; ?>
    <?php endif; ?>
    <?php if ( get_sub_field( 'add_two_column_content' ) ): ?>
      <?php if ( have_rows( 'column_content' ) ): ?>
        <div class="b-two-column">
          <?php while ( have_rows( 'column_content' ) ): ?>
            <div class="b-two-column__item">
              <?php the_row(); ?>
              <div class="column-editor"><?php echo get_sub_field( 'column_editor' ) ?></div>
              <?php $button = get_sub_field( 'button' );
              if ( $button ): ?>
              <a href="<?php echo $button['url'] ?>" <?php echo $button['target'] ?>
                class="btn brand brand--color-white">
                <span><?php echo $button['text'] ?></span>
              </a>
            <?php endif; ?>
          </div>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  <?php endif; ?>
</div>
</div>
</div>
