<!-- Interactive map -->
<?php
    $title = get_sub_field( 'title' );
    $sub_title = get_sub_field( 'sub_title' );
    $button_label = get_sub_field( 'button_label' );
    $button_type = get_sub_field( 'button_type' );
    $link = ( $button_type == '_self' ) ? get_sub_field( 'page_link' ) : get_sub_field( 'url' );
    $show_title_section = get_sub_field( 'show_title_section' );
    $show_listing_grid = get_sub_field( 'show_listing_grid' );
    $post_type = get_sub_field('post_type');
    $show_brands = get_sub_field('show_brands');
    $toolbar_title = get_sub_field('toolbar_title');
?>
<?php if ( $show_title_section ) : ?>
    <div class="title-row title-row_light">
        <div class="content">
            <h2><?php echo $title; ?> <span><?php echo $sub_title; ?></span></h2>
            <?php if ( !empty($link) && !empty($button_type) && !empty($button_label) ) : ?>
                <a href="<?php echo $link; ?>" target="<?php echo $button_type; ?>"
                   class="brand-dark-to"><?php echo $button_label; ?></a>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="interactive-map">
    <div class="interactive-map__header">
        <div class="content">
			<?php if ( $post_type != 'locations' && $toolbar_title ): ?>
                <h2 class="interactive-map__title">
                    <?php echo $toolbar_title; ?>
                </h2>
			<?php else: ?>
                <ul class="interactive-map__brand">
                    <?php foreach ( $show_brands as $show_brand ) : ?>
                        <?php if ( $show_brand == 'yellow' ) : ?>
                            <li class="link" data-brand="yellow">
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269.01 99.51" style="width: 125px">
                                        <defs>
                                            <style>.cls-1 {
                                                    fill: #221f1f;
                                                }</style>
                                        </defs>
                                        <g id="svg-8" data-name="svg-8">
                                            <g>
                                                <path class="cls-1"
                                                      d="M0,34.79H4.78L7.16,5.63H2.27V0H15.41l9,22.8a31.09,31.09,0,0,1,1,3.35h.11a25.86,25.86,0,0,1,1-3.35L35.42,0H48.55V5.63H43.72L46,34.79h4.78v5.57H34.28V34.79h4.49L37.29,14.44a24.4,24.4,0,0,1,.17-3.75h-.17a30.7,30.7,0,0,1-1,3.58L28.59,32.86H22.23L14.5,14.27a26,26,0,0,1-.91-3.58h-.23a24.35,24.35,0,0,1,.17,3.75L12.05,34.79H16.6v5.57H0Z"/>
                                                <path class="cls-1"
                                                      d="M69.76,22.46h1.82v-1c0-3.7-2.33-5-5.34-5a16,16,0,0,0-8.19,2.79l-2.67-5a19.71,19.71,0,0,1,11.48-3.58c7.79,0,11.94,4,11.94,11.42v11.6A1.11,1.11,0,0,0,80.05,35h2.62v5.4H75.85c-2.67,0-3.69-1.42-3.69-3.07V37a5.7,5.7,0,0,1,.17-1.48h-.11A10.09,10.09,0,0,1,63.06,41c-4.95,0-9.61-2.9-9.61-8.81C53.45,23.36,65.1,22.46,69.76,22.46ZM64.93,35.64c3.92,0,6.71-4.21,6.71-7.84v-1H70.45c-2.84,0-9.72.45-9.72,4.83A3.91,3.91,0,0,0,64.93,35.64Z"/>
                                                <path class="cls-1"
                                                      d="M85,35h4.21V18A1.14,1.14,0,0,0,88,16.77H84.76v-5.4h7.56c2.56,0,3.87,1.08,3.87,3.52V16.6a13.24,13.24,0,0,1-.11,1.82h.11C97.49,14.27,101,11,105.39,11a8.28,8.28,0,0,1,1.31.11v7.11a12.86,12.86,0,0,0-1.76-.11c-5.85,0-8.47,5.12-8.47,10.57V35h4.15v5.4H85Z"/>
                                                <path class="cls-1"
                                                      d="M123.36,22.46h1.82v-1c0-3.7-2.33-5-5.34-5a16,16,0,0,0-8.19,2.79l-2.67-5a19.71,19.71,0,0,1,11.48-3.58c7.79,0,11.94,4,11.94,11.42v11.6A1.11,1.11,0,0,0,133.65,35h2.61v5.4h-6.82c-2.67,0-3.69-1.42-3.69-3.07V37a5.7,5.7,0,0,1,.17-1.48h-.11A10.09,10.09,0,0,1,116.65,41c-4.95,0-9.61-2.9-9.61-8.81C107,23.36,118.7,22.46,123.36,22.46Zm-4.83,13.19c3.92,0,6.71-4.21,6.71-7.84v-1H124c-2.84,0-9.72.45-9.72,4.83A3.91,3.91,0,0,0,118.53,35.64Z"/>
                                                <path class="cls-1"
                                                      d="M138.56,16.77h-4.43v-5.4h4.66V3.47h7v7.9h6.59v5.4h-6.59V29c0,4.95,3.75,5.63,5.8,5.63.79,0,1.31-.06,1.31-.06v5.91a13.24,13.24,0,0,1-2.22.17c-4.09,0-12.11-1.2-12.11-10.91Z"/>
                                                <path class="cls-1"
                                                      d="M155.52,35h4.21V5.4h-4.49V0h11.71V14.1a16.54,16.54,0,0,1-.17,2.44h.11a11.31,11.31,0,0,1,10-5.85c6.42,0,10.12,3.35,10.12,11V35h4.21v5.4H179.8V23.19c0-3.52-.91-5.91-4.55-5.91-5.12,0-8.3,4.44-8.3,9.78V35h4.15v5.4H155.52Z"/>
                                                <path class="cls-1"
                                                      d="M208.06,10.69c8.87,0,16,6.31,16,15.18S216.93,41,208.11,41s-16-6.25-16-15.18S199.24,10.69,208.06,10.69Zm.06,24.22c4.66,0,8.59-3.64,8.59-9a8.62,8.62,0,1,0-17.23,0C199.47,31.27,203.4,34.9,208.11,34.9Z"/>
                                                <path class="cls-1"
                                                      d="M225.53,35h4.21V18a1.14,1.14,0,0,0-1.25-1.25h-3.24v-5.4h7.62c2.56,0,3.75,1.19,3.75,3.24v1.31a8,8,0,0,1-.11,1.25h.11a11.63,11.63,0,0,1,10.69-6.48c6.2,0,9.72,3.35,9.72,11V35h4.21v5.4H249.86V23.19c0-3.52-1-5.91-4.6-5.91-5.23,0-8.3,5-8.3,10.06V35h4.15v5.4H225.53Z"/>
                                                <path class="cls-1"
                                                      d="M100,75.12v3c0,2.62,3.3,4.15,7.11,4.15,4.09,0,7-1.76,7-5,0-4.09-4.6-5.4-9.26-7.22-5.4-2-10.69-4.49-10.69-11.71,0-8.13,6.6-11.26,13.53-11.26,6.37,0,12.79,2.44,12.79,7.11v5.29h-6.76V56.76c0-2-3-3.13-6-3.13-3.3,0-6,1.36-6,4.43,0,3.7,3.75,5,8,6.6,6,2.16,12.05,4.55,12.05,12.11,0,8.24-6.94,12.05-14.55,12.05-6.71,0-13.87-3-13.87-9.1v-4.6Z"/>
                                                <path class="cls-1"
                                                      d="M127.87,94.11V65.8a1.14,1.14,0,0,0-1.25-1.25h-3.24v-5.4h7.45c2.67,0,3.58,1.25,3.58,2.73V62c0,.68-.06,1.31-.06,1.31h.11s2.5-4.89,9.32-4.89c7.84,0,12.85,6.2,12.85,15.18,0,9.21-5.63,15.18-13.25,15.18A10.12,10.12,0,0,1,135,84.73h-.11a16.75,16.75,0,0,1,.17,2.5v6.88h4.21v5.4H123.66v-5.4Zm14.21-11.37c4,0,7.28-3.24,7.28-9,0-5.52-3-9.1-7.22-9.1-3.75,0-7.28,2.73-7.28,9.15C134.86,78.3,137.36,82.74,142.08,82.74Z"/>
                                                <path class="cls-1"
                                                      d="M175,58.46c8.87,0,16,6.31,16,15.18S183.85,88.82,175,88.82s-16-6.25-16-15.18S166.17,58.46,175,58.46ZM175,82.68c4.66,0,8.59-3.64,8.59-9a8.62,8.62,0,1,0-17.23,0C166.4,79,170.32,82.68,175,82.68Z"/>
                                                <path class="cls-1"
                                                      d="M192.2,82.74h4.21V65.8a1.14,1.14,0,0,0-1.25-1.25h-3.24v-5.4h7.56c2.56,0,3.87,1.08,3.87,3.52v1.71a13,13,0,0,1-.12,1.82h.12c1.31-4.15,4.77-7.45,9.21-7.45a8.28,8.28,0,0,1,1.31.11V66a12.86,12.86,0,0,0-1.76-.11c-5.85,0-8.47,5.12-8.47,10.57v6.31h4.15v5.4H192.2Z"/>
                                                <path class="cls-1"
                                                      d="M220.45,64.55H216v-5.4h4.66v-7.9h7v7.9h6.59v5.4h-6.59V76.77c0,4.95,3.75,5.63,5.8,5.63.79,0,1.31-.06,1.31-.06v5.91a13.24,13.24,0,0,1-2.22.17c-4.09,0-12.11-1.19-12.11-10.91Z"/>
                                                <path class="cls-1"
                                                      d="M237,78.47h6.14v1.71c0,2,2.67,3,5.68,3s4.95-1,4.95-3c0-2.44-2.73-3.07-6.59-4.49-4.26-1.48-9.15-3.47-9.15-9,0-6.14,6.14-8.3,11.43-8.3,4.21,0,10.46,1.65,10.46,6v3.7h-6.2V66.54c0-1.48-1.71-2.39-4.26-2.39-2.27,0-4.21.79-4.21,2.61,0,2.39,2.84,3.36,6.08,4.32C255.74,72.51,261,74.32,261,80c0,5.86-5.57,8.81-12.22,8.81-5.29,0-11.77-2.27-11.77-7Z"/>
                                                <path class="cls-1"
                                                      d="M265,10.44a3.9,3.9,0,0,1,4,3.83,4,4,0,1,1-4-3.83Zm0,6.82a3,3,0,1,0-3-3A3,3,0,0,0,265,17.25Zm-1.39-4.93h1.69a1.2,1.2,0,0,1,1.33,1.21,1,1,0,0,1-.72,1v0a1.09,1.09,0,0,1,.15.22l.77,1.38h-1l-.69-1.41h-.53v1.41h-.95Zm1.47,1.79a.54.54,0,0,0,.59-.58.52.52,0,0,0-.58-.56h-.53v1.14Z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <i class="link__hover brand"></i>
                            </li>
                        <?php endif; ?>
                        <?php if ( $show_brand == 'blue' ) : ?>
                            <li class="link" data-brand="blue">
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 317.72 32.23" style="width: 160px">
                                        <defs>
                                            <style>.cls-1 {
                                                    fill: #9c9ea0;
                                                }

                                                .cls-2 {
                                                    fill: #252324;
                                                }</style>
                                        </defs>
                                        <g id="svg-9" data-name="svg-9">
                                            <g>
                                                <path class="cls-1"
                                                      d="M3.77,24.09a9.41,9.41,0,0,0,5.7,2c1.18,0,2.45-.35,2.45-1.14,0-2-8.93-2.36-8.93-9,0-4.38,4.64-7.31,9.46-7.31a13.87,13.87,0,0,1,8.5,2.58L17.7,16.38a8.46,8.46,0,0,0-5.25-1.62c-1.1,0-2.45.35-2.45,1.18,0,1.75,9,2.23,9,9,0,4.38-3.85,7.27-9.5,7.27A15.8,15.8,0,0,1,0,28.95Z"/>
                                                <path class="cls-1"
                                                      d="M36.63,8.63c6.79,0,11.69,4,11.69,10.12,0,8-7.05,13.49-14.06,13.49-6.79,0-11.69-3.94-11.69-10.12C22.57,14.19,29.58,8.63,36.63,8.63Zm-2.23,17.3a6.5,6.5,0,0,0,6.26-6.7,4,4,0,0,0-4.12-4.29,6.69,6.69,0,0,0-6.31,6.83A3.91,3.91,0,0,0,34.39,25.93Z"/>
                                                <path class="cls-1"
                                                      d="M51.66,23.34,54.42,9.15H62l-2.54,13a8.9,8.9,0,0,0-.13,1.31c0,1.36.66,1.93,2,1.93,2.89,0,5.26-2.94,5.87-6.22l1.93-10h7.58L72.33,31.71H65l.22-1.14a22.2,22.2,0,0,1,.57-2.23h-.09a9,9,0,0,1-7.27,3.9c-3.9,0-7.1-1.71-7.1-6.39A13.77,13.77,0,0,1,51.66,23.34Z"/>
                                                <path class="cls-1"
                                                      d="M81.45,9.15h7.31l-.22,1.14A22.31,22.31,0,0,1,88,12.52h.09a9.56,9.56,0,0,1,7.53-3.9c3.9,0,7.1,1.71,7.1,6.39a14,14,0,0,1-.31,2.5L99.63,31.71H92l2.54-13a8.93,8.93,0,0,0,.13-1.31c0-1.36-.66-1.93-2-1.93-3,0-5.52,2.93-6.13,6.22l-1.93,10H77.07Z"/>
                                                <path class="cls-1"
                                                      d="M118.56,8.63c1.88,0,4.42.48,5.69,2h.09s0-.57.13-1l1.8-9.24h7.58l-6.09,31.32h-7.09l.31-1.49c.13-.7.26-1.27.26-1.27h-.09a8.11,8.11,0,0,1-6.48,3.28c-5.52,0-8.76-3.81-8.76-9.68C105.9,14.19,111.86,8.63,118.56,8.63ZM117.11,26c2.41,0,5.61-2.72,5.61-7,0-2.15-1-4-3.5-4s-5.65,2.54-5.65,7C113.56,24.44,114.92,26,117.11,26Z"/>
                                                <path class="cls-2"
                                                      d="M139,.39h11.65a12.51,12.51,0,0,1,4.6.74A7.28,7.28,0,0,1,160,8.32c0,3.72-1.75,8.72-6.53,10.6V19a12.68,12.68,0,0,1,.79,1.88l4,10.82H150l-3.5-10.55h-3.86L140.6,31.71h-7.67Zm8.19,14.19a4.56,4.56,0,0,0,4.86-4.82c0-1.84-1.23-2.8-3.2-2.8h-3.46l-1.49,7.62Z"/>
                                                <path class="cls-2"
                                                      d="M162.95,20,166.76.39h7.67l-3.85,19.8a6.05,6.05,0,0,0-.13,1.23c0,2.5,1.75,3.81,4.6,3.81,3.37,0,5.74-1.75,6.39-5L185.28.39h7.66l-3.9,20c-1.4,7.05-6.75,11.83-14.28,11.83-6.53,0-12.09-3.37-12.09-9.72A12.77,12.77,0,0,1,162.95,20Z"/>
                                                <path class="cls-2"
                                                      d="M199.22.39h7.45L212.49,15a44,44,0,0,1,1.57,5.34h.09s.31-3.37.7-5.34L217.7.39h7.67l-6.09,31.32h-7.4L206,17.12a47.39,47.39,0,0,1-1.62-5.34h-.09s-.26,3.37-.66,5.34L200.8,31.71h-7.67Z"/>
                                                <path class="cls-2"
                                                      d="M231.21.39h7.45L244.48,15a44,44,0,0,1,1.57,5.34h.09s.31-3.37.7-5.34L249.69.39h7.67l-6.09,31.32h-7.4L238,17.12a47.39,47.39,0,0,1-1.62-5.34h-.09s-.26,3.37-.66,5.34l-2.85,14.58h-7.67Z"/>
                                                <path class="cls-2"
                                                      d="M263.68.39h19.58L282,7H270l-1.09,5.69h9.5l-1.31,6.57h-9.5l-1.14,5.91h12.57l-1.27,6.57H257.55Z"/>
                                                <path class="cls-2"
                                                      d="M289,.39h11.65a12.54,12.54,0,0,1,4.6.74,7.28,7.28,0,0,1,4.69,7.18c0,3.72-1.75,8.72-6.53,10.6V19a12.68,12.68,0,0,1,.79,1.88l4,10.82H300l-3.5-10.55h-3.85l-2.06,10.55h-7.67Zm8.19,14.19A4.56,4.56,0,0,0,302,9.76c0-1.84-1.23-2.8-3.2-2.8h-3.46l-1.49,7.62Z"/>
                                                <path class="cls-2"
                                                      d="M315,0a2.72,2.72,0,0,1,2.72,2.75,2.7,2.7,0,1,1-5.4,0A2.72,2.72,0,0,1,315,0Zm0,4.75a1.92,1.92,0,0,0,1.88-2,1.86,1.86,0,1,0-3.72,0A1.9,1.9,0,0,0,315,4.75Zm-1-3.32h1.22a.83.83,0,0,1,.91.86.7.7,0,0,1-.43.67V3a.52.52,0,0,1,.1.15l.47.91h-.88l-.37-.88h-.21V4H314Zm1,1.17a.28.28,0,0,0,.3-.31c0-.21-.1-.31-.28-.31h-.22v.62Z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <i class="link__hover brand"></i>
                            </li>
                        <?php endif; ?>
                        <?php if ( $show_brand == 'red' ) : ?>
                            <li class="link" data-brand="red">
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3643.02 421.97" style="width: 175px">
                                        <defs>
                                            <style>.cls-1 {
                                                    fill: #0e0809;
                                                }</style>
                                        </defs>
                                        <g id="svg-10" data-name="svg-10">
                                            <g>
                                                <path class="cls-1"
                                                      d="M126.82,317.38H97.38v97.37H0V6H120.82C210.38,6,268.67,59.51,268.67,158.1v10.81c0,55.3-21,95-54.69,119.62l58.3,126.22h-101ZM97.38,236.83h19.23c34.26,0,55.3-25.85,55.3-68.51v-9c0-45.07-21-68.51-55.3-68.51H97.38Z"/>
                                                <path class="cls-1"
                                                      d="M301.59,281.31V6H399V281.31c0,33.06,10.82,52.9,42.08,52.9s42.08-19.84,42.08-52.9V6h97.37V281.31c0,93.17-56.49,139.46-139.45,139.46C357.49,420.76,301.59,374.48,301.59,281.31Z"/>
                                                <path class="cls-1"
                                                      d="M715,290.33V414.75H627.23V1.21h14.43l161.68,199s-1.8-37.86-1.8-63.12V6H889.3V419.56H874.88L713.18,227.22S715,263.89,715,290.33Z"/>
                                                <path class="cls-1"
                                                      d="M1026.19,290.33V414.75H938.43V1.21h14.43l161.69,199s-1.8-37.86-1.8-63.12V6h87.74V419.56h-14.41L1024.39,227.22S1026.19,263.89,1026.19,290.33Z"/>
                                                <path class="cls-1"
                                                      d="M1249.64,6h217.59V87.76H1345.82v113h97.37v78.75h-97.37V333h125.62v81.74h-221.8Z"/>
                                                <path class="cls-1"
                                                      d="M1636.59,317.38h-29.46v97.37h-97.37V6h120.81c89.56,0,147.87,53.49,147.87,152.08v10.81c0,55.3-21,95-54.7,119.62L1782,414.75h-101Zm-29.46-80.55h19.24c34.25,0,55.3-25.85,55.3-68.51v-9c0-45.07-21.05-68.51-55.3-68.51h-19.24Z"/>
                                                <path class="cls-1"
                                                      d="M1829.39,94.37c-19.23-6.61-30-24-30-44.47,0-28.85,23.44-48.69,49.88-48.69,27.05,0,51.69,20.43,51.69,48.69,0,18.64-7.2,34.25-19.23,54.69l-28.86,48.08h-39.67Z"/>
                                                <path class="cls-1"
                                                      d="M1908.56,383.49,1956,311.36c20.44,14.43,39.67,26.46,63.72,26.46,27.64,0,40.86-13.83,40.86-28.25v-2.41c0-19.24-10.81-28.25-32.46-46.29L1979.5,220c-36.07-30.65-55.3-63.11-55.3-107v-3c0-66.71,42.69-110,120.81-110,45.08,0,79.94,13.83,101,29.46L2103.92,104c-16.24-11.41-33.06-21-54.1-21C2026.38,83,2018,96.78,2018,107.6v1.2c0,13.22,6.62,21.64,27.05,38.46l51.69,43.29c37.87,31.85,60.1,66.71,60.1,111.19v4.81c0,58.91-37.85,115.41-129.23,115.41C1965.06,422,1930.8,402.13,1908.56,383.49Z"/>
                                                <path class="cls-1"
                                                      d="M2428.51,1.21h13.22L2592.6,414.75h-96.77L2485,380.49H2378.61l-10.8,34.25h-91.36Zm-27.05,307.15h60.11l-18.64-58.91c-6-18-10.81-38.47-11.41-40.27-.61,1.2-5.42,21.63-10.81,40.27Z"/>
                                                <path class="cls-1" d="M2618.91,6h97.37V329.39H2841.9v85.35h-223Z"/>
                                                <path class="cls-1" d="M2878.41,6h97.38V329.39h125.62v85.35h-223Z"/>
                                                <path class="cls-1"
                                                      d="M3137.92,6h217.59V87.76H3234.09v113h97.38v78.75h-97.38V333h125.63v81.74h-221.8Z"/>
                                                <path class="cls-1"
                                                      d="M3434.44,284.92,3322.65,6h101.58l46.28,129.24c7.81,22.24,14.42,53.49,14.42,53.49s7.21-31.25,15-53.49L3546.85,6H3643L3531.83,282.51V414.75h-97.39Z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <i class="link__hover brand"></i>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>

                <?php if ( $show_brands ) { ?>
                    <ul class="interactive-map__brand interactive-map__mobile">
                        <li>
                            Filter by store                            
                            <ul>           
                                <?php foreach( $show_brands as $show_brand ) { ?>   
                                    <li class="link" data-brand="<?php echo $show_brand; ?>">
                                        <?php switch ( $show_brand ) {
                                            case "yellow":
                                                echo '<a href="#">Marathon Sports</a>';
                                                break;
                                            case "blue":
                                                echo '<a href="#">Sound Runner</a>';
                                                break;
                                            case "red":
                                                echo '<a href="#">Runner\'s Alley</a>';
                                                break;
                                        } ?>                                        
                                    </li>
                                <?php } ?>                                            
                            </ul>
                        </li>
                    </ul>
                <?php } ?>

			<?php endif; ?>
            <div class="interactive-map__nav">
                <div class="interactive-map__search">
                    <span class="trigger color-dark-to">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                            <defs>
                                <style>.search-icon {
                                        fill: none;
                                        stroke: #000;
                                        stroke-linecap: round;
                                        stroke-miterlimit: 10;
                                        stroke-width: 9px;
                                    }</style>
                            </defs>
                            <g id="svg-11" data-name="svg-11">
                                <g>
                                    <path class="search-icon"
                                          d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"/>
                                </g>
                            </g>
                        </svg>
                    </span>
                    <input type="text" placeholder="Search by City, State, or Zip Code" id="search-map">
                </div>
                <a class="btn trigger__link">
                    <span>Near Me</span>
                </a>
            </div>
        </div>
    </div>
    <div class="interactive-map__body">
        <div id="map"></div>
    </div>
    <div class="content">
        <ul class="location-grid set-markers"
            data-post="<?php echo $post_type; ?>" <?php echo !$show_listing_grid ? 'style="display:none;"' : ''; ?>>
			<?php
                $args = array(
                    'post_type'      => $post_type,
                    'posts_per_page' => -1,
                );

                $query = new WP_Query( $args );
            ?>
			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php
                        // Selected brand color
                        $filter = get_post_meta( $post->ID, 'brand_color' )[0];
                        if ( $post_type == "locations" ) {   
                            if ( in_array( $filter, $show_brands ) ) { ?>                                
                                <?php    
                                    $map = get_post_meta( $post->ID, 'map' )[0];
                                    $address = explode( ',', $map['address'] );
                                    $address_state = explode( ' ', $address[2] );
                                    $work_hours = [];

                                    $phone = get_post_meta($post->ID, 'phone')[0];
                                    $phone ? $phone_link = preg_replace('/[^A-Za-z0-9]/', '', $phone) : null;
                                ?>
                                <li data-marker-lat='<?php echo $map['lat']; ?>'
                                    data-marker-lng='<?php echo $map['lng']; ?>'
                                    data-marker-filter="<?php if ( $post_type == 'run_clubs' || $post_type == 'race_teams' ) { echo 'all'; } else { echo $filter; } ?>"
                                    data-color="<?php echo $filter; ?>"
                                    data-image-url="<?php echo get_template_directory_uri() ?>/media/pin-<?php echo $filter; ?>.png"
                                    data-href="http://maps.google.com/maps?q=loc:<?php echo $map['lat'] . ',' . $map['lng']; ?>"
                                    data-sity='<?php echo str_replace(' ', '', strtolower( $address[1])); ?>'
                                    data-state='<?php echo strtolower( $address_state[1] ); ?>'
                                    data-zip='<?php echo $address_state[2]; ?>'
                                    data-clubtitle='<?php echo get_the_title(); ?>'
                                    data-posturl='<?php echo get_permalink(); ?>'
                                    data-workhours='
                                    <?php
                                        while (have_rows('store_hour')) {
                                            the_row();
                                            $day = get_sub_field('date');
                                            $hours = get_sub_field('hours');
                                            $str = $day . ' - '. $hours;
                                            array_push($work_hours, $str);
                                        }
                                        echo json_encode($work_hours);
                                    ?>'
                                    class="location-grid__item" data-waypoint>
                                    <?php if ($show_listing_grid) { ?>
                                    <div class="location-grid__wrapper">
                                        <a href="<?php the_permalink(); ?>" class="location-grid__image">
                                            <div class="bg-cover"
                                                style="background-image:url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'location' ); ?>);"></div>
                                        </a>
                                        <div class="location-grid__body">
                                            <a href="<?php the_permalink(); ?>">
                                                <h2 class="location-grid__title">
                                                    <?php if ($post_type == 'run_clubs' || $post_type == 'race_teams'): ?>
                                                        <?php echo get_the_title();
                                                        foreach ($work_hours as $val) {
                                                            echo '<span>' . $val .  '</span>';
                                                        };
                                                        ?>
                                                    <?php else: ?>
                                                        <?php if ( !empty($address_state[2]) ) : ?>
                                                            <?php echo $address[0] . '<span>' . $address[1] . ', ' . $address_state[1] . ' ' . $address_state[2] . '</span>'; ?>
                                                        <?php else : ?>
                                                            <?php echo $address[0] . '<span>' . $address[1] . ', ' . $address_state[1] . '</span>'; ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </h2>
                                            </a>
                                            <?php if ( $phone && $phone_link ) { ?>
                                                <p>
                                                    <a href="tel:<?php echo $phone_link; ?>" class="location-grid__phone"><?php echo $phone; ?></a>
                                                </p>
                                            <?php } ?>
                                            <p>
                                                <a href="http://maps.google.com/maps?q=loc:<?php echo $map['lat'] . ',' . $map['lng']; ?>"
                                                    target="_blank" data-map
                                                    class="location-grid__direction"><span>get directions</span></a></p>
                                            <a href="<?php the_permalink(); ?>" class="location-grid__link">learn more</a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </li>
                            <?php }
                        } else { ?>
                            <?php    
                                $map = get_post_meta( $post->ID, 'map' )[0];
                                $address = explode( ',', $map['address'] );
                                $address_state = explode( ' ', $address[2] );
                                $work_hours = [];

                                $phone = get_post_meta($post->ID, 'phone')[0];
                                $phone ? $phone_link = preg_replace('/[^A-Za-z0-9]/', '', $phone) : null;
                            ?>
                            <li data-marker-lat='<?php echo $map['lat']; ?>'
                                data-marker-lng='<?php echo $map['lng']; ?>'
                                data-marker-filter="<?php if ( $post_type == 'run_clubs' || $post_type == 'race_teams' ) { echo 'all'; } else { echo $filter; } ?>"
                                data-color="<?php echo $filter; ?>"
                                data-image-url="<?php echo get_template_directory_uri() ?>/media/pin-<?php echo $filter; ?>.png"
                                data-href="http://maps.google.com/maps?q=loc:<?php echo $map['lat'] . ',' . $map['lng']; ?>"
                                data-sity='<?php echo str_replace(' ', '', strtolower( $address[1])); ?>'
                                data-state='<?php echo strtolower( $address_state[1] ); ?>'
                                data-zip='<?php echo $address_state[2]; ?>'
                                data-clubtitle='<?php echo get_the_title(); ?>'
                                data-posturl='<?php echo get_permalink(); ?>'
                                data-workhours='
                                <?php
                                    while (have_rows('store_hour')) {
                                        the_row();
                                        $day = get_sub_field('date');
                                        $hours = get_sub_field('hours');
                                        $str = $day . ' - '. $hours;
                                        array_push($work_hours, $str);
                                    }
                                    echo json_encode($work_hours);
                                ?>'
                                class="location-grid__item" data-waypoint>
                                <?php if ($show_listing_grid) { ?>
                                <div class="location-grid__wrapper">
                                    <a href="<?php the_permalink(); ?>" class="location-grid__image">
                                        <div class="bg-cover"
                                            style="background-image:url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'location' ); ?>);"></div>
                                    </a>
                                    <div class="location-grid__body">
                                        <a href="<?php the_permalink(); ?>">
                                            <h2 class="location-grid__title">
                                                <?php if ($post_type == 'run_clubs' || $post_type == 'race_teams'): ?>
                                                    <?php echo get_the_title();
                                                    foreach ($work_hours as $val) {
                                                        echo '<span>' . $val .  '</span>';
                                                    };
                                                    ?>
                                                <?php else: ?>
                                                    <?php if ( !empty($address_state[2]) ) : ?>
                                                        <?php echo $address[0] . '<span>' . $address[1] . ', ' . $address_state[1] . ' ' . $address_state[2] . '</span>'; ?>
                                                    <?php else : ?>
                                                        <?php echo $address[0] . '<span>' . $address[1] . ', ' . $address_state[1] . '</span>'; ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </h2>
                                        </a>
                                        <?php if ( $phone && $phone_link ) { ?>
                                            <p>
                                                <a href="tel:<?php echo $phone_link; ?>" class="location-grid__phone"><?php echo $phone; ?></a>
                                            </p>
                                        <?php } ?>
                                        <p>
                                            <a href="http://maps.google.com/maps?q=loc:<?php echo $map['lat'] . ',' . $map['lng']; ?>"
                                                target="_blank" data-map
                                                class="location-grid__direction"><span>get directions</span></a></p>
                                        <a href="<?php the_permalink(); ?>" class="location-grid__link">learn more</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </li>
                        <?php } ?>
                                                                
				<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
        </ul>
    </div>
</div>
