<!--  News List  -->
<?php
    $post_type = get_sub_field( 'choose_post_type' );
    $args      = array(
        'post_type'     => $post_type,
        'orderby'       => 'date',
        'order'         => 'DESC',
        'post_status' => 'publish',
        'post_pre_page' => 10,
        'paged'         => 1,
    );

    if ( $post_type == 'eventbrite_events' ) {
        $args['tax_query'][] = array(
            'taxonomy' => 'eventbrite_category',
            'field'    => 'slug',
            'terms'    => array( 'run-group', 'race-team', 'run-club' ),
            'operator' => 'NOT IN',
        );
                                 
        $args['meta_key'] = 'event_start_date';
        $args['orderby']  = 'meta_value';
        $args['order'] = 'ASC';

        // Omit past events from query
        $args['meta_value'] = date('Y-m-d');            
        $args['meta_compare'] = '>='; 
    }

    $query   = new WP_Query( $args );
    $p_count = $query->max_num_pages;
    $search_title = '';

    if ( $post_type == 'post' ) $search_title = 'News';
    if ( $post_type == 'runners_resources' ) $search_title = 'Resources';
    if ( $post_type == 'eventbrite_events' ) $search_title = 'Events';
?>

<div class="list-with-sidebar" data-type-post="<?php echo $post_type; ?>">
    <aside class="sidebar">
        <ul class="sidebar__item">
            <form role="search" method="get" id="searchform-sidebar" class="searchform" action="<?php echo home_url( '/' ); ?>" data-name-post="<?php echo $search_title; ?>">
                <div>
                    <input type="text" value="" name="s" id="s" placeholder="Search <?php echo $search_title; ?>">
                    <button type="submit" id="searchsubmit">
                        <span class="trigger-search color-dark-to">
                             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                                <defs>
                                    <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                                </defs>
                                <g id="svg-1" data-name="svg-1">
                                    <g>
                                        <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"></path>
                                    </g>
                                </g>
                            </svg>
                        </span>
                    </button>
                </div>
            </form>
        </ul>
		<?php if ( $post_type == 'post' ) : ?>
			<?php if ( is_active_sidebar( 'sidebar-item-1' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar-item-1' ); ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php if ( $post_type == 'runners_resources' ) : ?>
			<?php if ( is_active_sidebar( 'sidebar-item-3' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar-item-3' ); ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php if ( $post_type == 'eventbrite_events' ) : ?>
			<?php if ( is_active_sidebar( 'sidebar-item-2' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar-item-2' ); ?>
			<?php endif; ?>
		<?php endif; ?>
    </aside>
    <ul class="post-list" data-ajax-page="1">
		<?php if ( $query->have_posts() ) : ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<?php if ( $post_type !== 'eventbrite_events' ) : ?>
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full_hd' )[0]; ?>
                    <li class="post-list-item">
						<?php if ( ! empty( $image ) ) : ?>
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-list-item__img bg-cover"
                                     style="background-image: url(<?php echo $image; ?>);"></div>
                            </a>
						<?php endif; ?>
                        <a href="<?php the_permalink(); ?>">
                            <h2><?php the_title(); ?></h2>
                        </a>
                        <div class="post-list-item__content">
							<?php the_excerpt(); ?>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="btn brand brand--color-white"><span>READ MORE</span></a>
                    </li>
				<?php else : ?>
					<?php
                        $venue_location = get_post_meta( $post->ID, 'venue_address' )[0];
                        $event_date           = get_post_meta( $post->ID, 'event_start_date' )[0];
                        $event_date_formatted = date( "m/d/Y", strtotime( $event_date ) );

                        $events_start_hour    = get_post_meta( $post->ID, 'event_start_hour' )[0];
                        $events_start_minute  = get_post_meta( $post->ID, 'event_start_minute' )[0];
                        $event_start_meridian = get_post_meta( $post->ID, 'event_start_meridian' )[0];
                        $event_start_time     = $events_start_hour . ":" . $events_start_minute . $event_start_meridian;

                        $events_end_hour    = get_post_meta( $post->ID, 'event_end_hour' )[0];
                        $events_end_minute  = get_post_meta( $post->ID, 'event_end_minute' )[0];
                        $event_end_meridian = get_post_meta( $post->ID, 'event_end_meridian' )[0];
                        $event_end_time     = $events_end_hour . ":" . $events_end_minute . $event_end_meridian;

                        $event_url = get_post_meta( $post->ID, 'iee_event_link' )[0];

                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full_hd' )[0];
					?>
                    <li class="post-list-item">
						<?php if ( $image ) : ?>
                            <a href="<?php echo $event_url; ?>">
                                <div class="post-list-item__img bg-cover"
                                     style="background-image: url(<?php echo $image; ?>);"></div>
                            </a>
						<?php endif; ?>

                        <span class="post-list-item__date"><?php echo $event_date_formatted; ?></span>
                        <a href="<?php echo $event_url; ?>" target="_blank">
                            <h2><?php the_title(); ?></h2>
                        </a>
                        <div class="post-list-item__content">
							<?php the_excerpt(); ?>
                        </div>
                        <div class="post-list-item__info">
							<?php if ( !empty( $event_start_time ) || !empty( $event_end_time ) ) : ?>
                                <span>
                                     <strong>Time: </strong>
									<?php echo $event_start_time . ' - ' . $event_end_time; ?>
                                </span>
							<?php endif; ?>
							<?php if ( !empty( $venue_location ) ) : ?>
                                <span>
                                    <strong>Location: <?php echo $venue_location; ?></strong>
                                </span>
							<?php endif; ?>
                        </div>
                        <a href="<?php echo $event_url; ?>" class="btn brand brand--color-white" target="_blank"><span>REGISTER</span></a>
                    </li>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
        <li class="post-list-nav">
			<?php if ( $query->have_posts() && $p_count > 1 ) : ?>
<!--                <a data-page="prev" class="post-list-nav_arrow">previous</a>-->
				<?php for ( $x = 1; $x <= $p_count; $x ++ ) : ?>
                    <a class="post-list-nav__number <?php echo $x === 1 ? 'active' : '' ?>"
                       data-page="<?php echo $x; ?>"><?php echo $x; ?></a>
				<?php endfor; ?>
                <a data-page="next" class="post-list-nav_arrow">next</a>
			<?php endif; ?>
        </li>
    </ul>
</div>
