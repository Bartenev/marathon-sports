<?php
	$title = get_sub_field('title');
	$items = get_sub_field('items');
	$bind_id = rand();
 ?>
<!--  Image Gallery  -->
<?php if(have_rows('items')) : ?>
<div class="image-gallery brand">
	<h2 class="image-gallery__title"><?php echo $title; ?></h2>
	<div class="image-gallery__content">
		<div class="swiper-arrow swiper-arrow_prev color-dark-to">
			<svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
		</div>
		<div class="content swiper-container" data-gallery data-slider-bind="slider-<?php echo $bind_id ?>" data-lightbox-bind="lightbox-<?php echo $bind_id; ?>">
			<div class="swiper-wrapper">
				<?php while(have_rows('items')) : the_row();
					$image = wp_get_attachment_image_src(get_sub_field('image'), 'full_hd')[0];
					$caption = get_sub_field('caption');
				?>
					<div class="swiper-slide" >
						<div class="card">
							<div class="card__image bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
							<?php if ( $caption ) { ?>
								<div class="card__caption"><?php echo $caption; ?></div>
							<?php } ?>
						</div>
					</div>
				<?php endwhile; ?>
				<div class="swiper-slide">
					<div class="card">
						<div class="card__image bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
						<div class="card__caption"><?php echo $caption; ?><span class="card__subcaption"><?php echo $sub_caption; ?></span></div>
					</div>
				</div>
			</div>
		</div>
		<div class="swiper-arrow swiper-arrow_next color-dark-to">
			<svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
		</div>
	</div>
</div>


<div class="lightbox lightbox_slider" id="lightbox-<?php echo $bind_id; ?>">

	<div class="lightbox__content">
		<div class="lightbox__closer"></div>
		<div class="content swiper-container" data-lightbox-gallery data-slider-bind="slider-<?php echo $bind_id ?>">
			<div class="swiper-wrapper">
				<?php while(have_rows('items')) : the_row();
					$image = wp_get_attachment_image_src(get_sub_field('image'), 'full_hd')[0];
					$caption = get_sub_field('caption');
					$sub_caption = get_sub_field('sub_caption');
				?>
					<?php if(get_sub_field('panorama')) : ?>
						<div class="swiper-slide swiper-no-swiping">
							<div class="card">
								<div class="card__vr">
									<div class="vr-pano" style="" data-pano="<?php echo get_sub_field('panorama') ?>"></div>
								</div>
								<div class="card__caption"><?php echo $caption; ?><span class="card__subcaption"><?php echo $sub_caption; ?></span></div>
							</div>
						</div>
					<?php else: ?>
						<div class="swiper-slide swiper-no-swiping">
							<div class="card">
								<div class="card__image bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
								<div class="card__caption"><?php echo $caption; ?><span class="card__subcaption"><?php echo $sub_caption; ?></span></div>
							</div>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>

			</div>
		</div>
		<div class="swiper-arrow swiper-arrow_prev color-to">
			<svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
		</div>
		<div class="swiper-arrow swiper-arrow_next color-to">
			<svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
		</div>
	</div>
</div>
 <?php endif;?>
