<?php
	$first_element_alignment = get_sub_field('first_element_alignment');
	$count = 0;
?>
<!--  Two Column Numbered Sections  -->
<div class="content c-column-wrapper two-column-numbered-section">
	<?php if ( have_rows('elements') ) : ?>
		<?php while(have_rows('elements')) : the_row(); ?>
			<?php
				$count++;
				$align = '';
				$delay = 400;
				$delayText = 250;

				$featured_image_id = get_sub_field('image');
				$featured_image_array = wp_get_attachment_image_src($featured_image_id, 'large');

				$row_count = (strlen($count) == 1) ? '0' . $count : $count;
				if ( $first_element_alignment == 'left' ) {
					$align = ($count % 2 == 0) ? 'c-two-column_text-left' : '';
				}else {
					$align = ($count % 2 == 0) ? '' : 'c-two-column_text-left';
				}
			?>
			<div class="c-two-column <?php echo $align; ?> c-numbered" data-waypoint-fade>
				<div class="c-two-column__item c-numbered__image" data-delay="<?php echo $delay ?>" style="background-image:url(<?php echo $featured_image_array[0]; ?>);"></div>
				<div class="c-two-column__item">
					<div class="c-numbered-text">
						<div class="c-numbered-text__count" data-delay="0"><span><?php echo $row_count; ?></span></div>
						<div class="c-numbered-text__title" data-delay="<?php echo $delayText ?>"><?php echo get_sub_field('text_heading'); ?></div>
						<div class="c-numbered-text__sub brand" data-delay="<?php echo $delayText ?>"><?php echo get_sub_field('text_description'); ?></div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
