<!-- Slider main container -->
<?php
    $dot_navigation = get_sub_field('dot_navigation');
    $arrows_navigation = get_sub_field('arrows_navigation');
    $slider_auto_play = get_sub_field('slider_auto_play');
    $time_delay = '';
    if ( $slider_auto_play == 'true' ) {
        $time_delay = get_sub_field('time_delay');
    }
    $count = 0;
?>
<div class="swiper-container"
     data-autoplay="<?php echo $time_delay; ?>"
     data-dot-nav="<?php echo $dot_navigation; ?>"
     data-full-width
     data-brand-color="<?php echo $GLOBALS['theme_color']; ?>">
	<!-- Additional required wrapper -->
	<div class="swiper-wrapper">
		<!-- Slides -->
        <?php if( have_rows('slide') ) : ?>
            <?php while (have_rows('slide')) : the_row(); 
                // Get button clone field options
                $button_text = get_sub_field( 'linked_button_button_text' );
                $link_type = get_sub_field( 'linked_button_link_type' );   
                if ( $link_type == "internal" ) {
                    $button_link = get_sub_field( 'linked_button_page_link' );
                } else {
                    $button_link = get_sub_field( 'linked_button_link_url' );
                }

                $link_behavior = get_sub_field( 'linked_button_link_behavior' );                    
                if ( $link_behavior[0] == "blank" ) { 
                    $open_in_new_tab = true; 
                }  else {
                    $open_in_new_tab = false;
                }                   
            ?>

                <?php if ( get_sub_field('slide_type') == 'image' ) : ?>
                    <?php                        
                        $image = wp_get_attachment_image_src(get_sub_field('image')['ID'], 'full_hd')[0];
                        $image_position = get_sub_field('image_position');
                        $heading = get_sub_field('heading');
                        $content = get_sub_field('content');                        
                    ?>
                    <!-- TYPE: IMAGE -->
                    <div class="swiper-slide hero-slide">
                        <div class="hero-slide__media bg-cover <?php echo $image_position != 'center' ? 'bg-cover' . $image_position : ''; ?>" style="background-image:url('<?php echo $image; ?>')"></div>
                        <div class="hero-slide__caption">
                            <div class="hero-slide__title">
                                <h2><?php echo $heading; ?></h2>
                                <?php echo $content; ?>
                                <?php if ( $button_text ) { ?>
                                    <a href="<?php echo $button_link; ?>" <?php if ( $open_in_new_tab ) { echo "target='_blank'"; } ?> class="btn brand brand--color-white">
                                        <span><?php echo $button_text; ?></span>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php else :  ?>
                    <?php
                        $video_type = get_sub_field('video_type');
                        $video_id = get_sub_field('video_id');
                        $background_image = wp_get_attachment_image_src(get_sub_field('mobile_background_image')['ID'], 'full_hd')[0];
                    ?>
                    <!-- TYPE: VIDEO VIMEO-->
                    <div class="swiper-slide hero-slide" data-player="<?php echo $video_type; ?>" data-video="<?php echo $video_id; ?>">
                        <div class="hero-slide__media hero-slide__media_video bg-cover"  style="background-image:url('<?php echo $background_image; ?>')">
                            <div class="video-aligner">
                                <div class="video-player"></div>
                            </div>
                        </div>
                        <div class="hero-slide__video-caption">
                            <?php if ( $button_text ) { ?>
                                <button class="btn brand brand--color-white"><span><?php echo $button_text; ?></span></button>
                            <?php } ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php $count++; ?>
            <?php endwhile; ?>
        <?php endif;  ?>
        <?php wp_reset_postdata(); ?>

	</div>
	<!-- If we need pagination -->
	<div class="swiper-controls <?php echo $dot_navigation; ?>"></div>
	<!-- If we need navigation buttons -->
    <?php if ( $arrows_navigation == 'true' && $count > 1 ) : ?>
        <div class="swiper-arrow swiper-arrow_prev color-to">
            <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
        </div>
        <div class="swiper-arrow swiper-arrow_next color-to">
            <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
        </div>
    <?php endif; ?>
</div>

<div class="lightbox">
	<div class="lightbox__content"></div>
</div>