<!-- Call Out -->
<?php
    $heading = get_sub_field('heading');
    $subheading = get_sub_field('subheading');
    $button_label = get_sub_field('button_label');
    $button_type = get_sub_field('button_type');
    $link = ( $button_type == '_self' ) ? get_sub_field('page_link') : get_sub_field('url');
?>
<div class="title-row title-row_dark">
    <div class="content">
        <h2><?php echo $heading; ?> <span><?php echo $subheading; ?></span></h2>
        <a href="<?php echo $link; ?>" target="<?php echo $button_type; ?>" class="brand"><?php echo $button_label; ?></a>
    </div>
</div>