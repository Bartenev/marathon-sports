<?php
	$content = get_sub_field('content');
	$show_button = get_sub_field('show_button');
	$button_type = get_sub_field('button_type');
	$link = ( $button_type == '_self' ) ? get_sub_field('page_link') : get_sub_field('url');
	$button_label = get_sub_field('button_label');
	$additional_title = get_sub_field('content_link');
?>
<!--  Text Content  -->
<div class="text-content">
	<div class="text-content__inner">
        <div class="text-content__editor">
            <?php echo $content; ?>
        </div>
        <div class="text-content__addings">
            <h3><?php echo $additional_title; ?></h3>
            <?php if ( have_rows('link') ) : ?>
                <?php while ( have_rows('link') ) : the_row(); ?>
		            <?php
                        $icon = wp_get_attachment_image_src(get_sub_field('icon'), 'full_hd')[0];
                        $type = get_sub_field('type');
                        $label = get_sub_field('link_label');
		            ?>
                    <?php if ( get_sub_field('type') != 'another' ) : ?>
                        <a href="<?php echo $type . ':' . $label; ?>" <?php echo  $type == 'mailto' ? 'style="color: #0099ff;"' : ''; ?>>
                            <span style="background-image: url(<?php echo $icon; ?>);"></span>
                            <?php echo $label; ?>
                        </a>
                    <?php else : ?>
                        <?php
                            $link_type = get_sub_field('link_type');
                            $link = ( $link_type == '_self' ) ? get_sub_field('page_link') : get_sub_field('url');
                        ?>
                        <a target="<?php echo $link_type; ?>" href="<?php echo $link; ?>">
                            <span style="background-image: url(<?php echo $icon; ?>);"></span>
				            <?php echo $label; ?>
                        </a>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php if ( $show_button == 'show' ) : ?>
                <a href="<?php echo $link; ?>" target="<?php echo $button_type; ?>" class="btn brand brand--color-white"><span><?php echo $button_label; ?></span></a>
            <?php endif; ?>
        </div>
		
	</div>
</div>