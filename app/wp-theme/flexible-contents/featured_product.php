<!-- Featured products -->
<?php
$title = get_sub_field('title');
$sub_title = get_sub_field('sub_title');
$api_url = get_field('shopify_app_api_url', 'option');
$shop_url = get_field('shopify_site_url', 'option');
$collection_ID = get_sub_field('shopify_colleciton_id') == '' ? get_field('collection_id', 'option') : get_sub_field('shopify_colleciton_id');
$header_type = get_sub_field('header_type') == 'true' ? true : false;

// Get button clone field options
$button_text = get_sub_field( 'linked_button_button_text' );

$link_type = get_sub_field( 'linked_button_link_type' );
if ( $link_type == "internal" ) {
	$button_link = get_sub_field( 'linked_button_page_link' );
} else {
	$button_link = get_sub_field( 'linked_button_link_url' );
}

$link_behavior = get_sub_field( 'linked_button_link_behavior' );
if ( $link_behavior[0] == "blank" ) { 
	$open_in_new_tab = true; 
} else {
	$open_in_new_tab = false;
}    

?>
<?php if (!$header_type): ?>
    <h2 class="product-title"><?php echo $title?></h2>
<?php else: ?>
    <div class="title-row">
        <div class="content">
            <h2>
				<?php echo $title; ?>
                <span><?php echo $sub_title; ?></span>
            </h2>		
			<?php if ( $button_text ) { ?>	
            	<a href="<?php echo $button_link; ?>" <?php if ( $open_in_new_tab ) { echo "target='_blank'"; } ?> class="brand-dark-to"><?php echo $button_text; ?></a>
			<?php } ?>
        </div>
    </div>
<?php endif; ?>
<div class="content">
	<?php if ( get_sub_field('product_type') == 'product' && have_rows('shopify_products') ) : ?>
        <ul class="featured-products <?php echo get_the_ID() == 57 ? 'featured-products_simple' : ''; ?>">
			<?php if ( have_rows('shopify_products') ) : ?>
				<?php while ( have_rows('shopify_products') ): the_row(); ?>
					<?php
					$product_ID = get_sub_field('product_id');
					$product_url = $api_url . '/admin/products.json?ids=' . $product_ID;
					$product_reviews_url = $api_url . '/admin/products/' . $product_ID . '/metafields.json?namespace=stamped&key=badge&fields=value';
					$product_reviews_url_json;
					$product_content_json;
					$product;
                    $errText = NULL;
					if ( false === ( $product_content_json = get_transient( 'shopify_product_feed' . $product_ID ) ) ) {
						$product_content_json = @file_get_contents( $product_url );

                        if(!$product_content_json) {
                            $errText = "Product ID: ".$product_ID. " not found, please check the ID and try again";
                        }
                        else {
                            set_transient( 'shopify_product_feed' . $product_ID, $product_content_json, 4 * HOUR_IN_SECONDS );
                        }

					}

					if ( false === ( $product_reviews_url_json = get_transient( 'shopify_product_reviews' . $product_ID ) ) ) {
						$product_reviews_url_json = @file_get_contents($product_reviews_url);

                        if(!$product_reviews_url_json) {
                            $errText = "Product ID: ".$product_ID. " not found, please check the ID and try again";
                        }
                        else {
                            set_transient( 'shopify_product_reviews' . $product_ID, $product_reviews_url_json, 4 * HOUR_IN_SECONDS );
                        }
					}
                    if(empty($errText)) {
                        $product = json_decode( $product_content_json, true );
    					$product_content = $product['products'][0];
    					$product_handle = $product_content['handle'];
    					$product_title = $product_content['title'];
    					$product_type = $product_content['product_type'];
    					$product_price = $product_content['variants'][0]['price'];
    					$product_image_parts = pathinfo($product_content['images'][0]['src']);
    					$product_image_src = '';
    					if ( !empty($product_image_parts['dirname']) && !empty($product_image_parts['extension']) ) {
    						$product_image_src = $product_image_parts['dirname'] . '/' . $product_image_parts['filename'] . '_x200.' . $product_image_parts['extension'];
                        }
    					$count_variant = 0;

    					$product_reviews = json_decode($product_reviews_url_json, true);
    					if ( count($product_reviews["metafields"]) !== 0 ) {
    						$product_reviews_data =  strstr($product_reviews["metafields"][0]['value'], '</style>');
    						$product_rating = 0;
    						if ($product_reviews_data) {
    							libxml_use_internal_errors(true);
    							$doc = new DOMDocument();
    							$doc->loadHTML($product_reviews_data);
    							$sxml = simplexml_import_dom($doc);
    							$product_rating = $sxml->body->div->div->div->span->meta[3]['content'];
    						}
                        }
                    }

					?>
                    <?php if( !empty($errText) ) : ?>
                        <li class="featured-products__item">
                            <span class="note-err"><?php echo $errText; ?></span>
                        </li>
                    <?php else: ?>
                        <li class="featured-products__item" data-rating="<?php echo $product_rating; ?>">
                            <div class="featured-product__inner">
                                <a href="<?php echo $shop_url; ?>/products/<?php echo $product_handle; ?>" target="_blank" class="featured-product__link-img">
                                    <img class="featured-product__img" data-origin-img="<?php echo $product_image_src; ?>" src="<?php echo $product_image_src; ?>" alt="<?php echo $product_title; ?>">
                                </a>
                                <!-- Variant Slider -->
    							<?php if ( count($product_content['variants']) > 1 ) : ?>
    								<?php $option_index = 0; ?>
    								<?php foreach ( $product_content['options'] as $key => $option ) : ?>
    									<?php if ( $option['name'] == 'Color' || $option == 'color' ) : ?>
    										<?php $option_index = 'option' . ($key + 1); ?>
    									<?php endif; ?>
    								<?php endforeach; ?>
                                    <div class="featured-products__body">
                                        <div class="featured-products__variant">
                                            <div class="swiper-container" data-product data-brand-color="<?php echo $GLOBALS['theme_color']; ?>">
                                                <div class="swiper-wrapper">
    												<?php $values = array(); ?>
    												<?php foreach ( $product_content['variants'] as $key => $product_variant ) : ?>
    													<?php
                                                            $variant_color = $product_variant[$option_index];
                                                            $variant_image_scr = '//cdn.shopify.com/s/assets/no-image-2048-5e88c1b20e087fb7bbe9a3771824e743c244f437e4f8ba93bbf7b11b53f7824c_x200.gif';
                                                            $variant_image_scr_small = '//cdn.shopify.com/s/assets/no-image-100-c91dd4bdb56513f2cbf4fc15436ca35e9d4ecd014546c8d421b1aece861dfecf_65x.gif';
    													?>
    													<?php foreach ( $product_content['images'] as $images ) : ?>
    														<?php
                                                                $href = $shop_url . '/products/' . $product_handle . '?variant=' . $product_variant['id'];
                                                                if ( $product_variant['id'] == $images['variant_ids'][0] ) {
                                                                    $variant_image_parts = pathinfo($product_content['images'][$key]['src']);
                                                                    $variant_image_scr = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_x200.' . $variant_image_parts['extension'];
                                                                    $variant_image_scr_small = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_65x.' . $variant_image_parts['extension'];
                                                                }
    														?>
    													<?php endforeach; ?>
    													<?php if ( !in_array($variant_color, $values) ) : ?>
    														<?php
                                                                $count_variant++;
                                                                array_push($values, $variant_color);
    														?>
                                                            <div class="swiper-slide <?php echo $key == 0 ? 'active' : ''; ?>" data-hover-img="<?php echo $variant_image_scr; ?>">
                                                                <a href="<?php echo $href; ?>" target="_blank"><img src="<?php echo $variant_image_scr_small; ?>" alt="<?php echo $variant_color; ?>"></a>
                                                            </div>
    													<?php endif; ?>
    												<?php endforeach; ?>
                                                </div>
                                            </div>
    										<?php if ( $count_variant > 3 ) : ?>
                                                <div class="swiper-arrow swiper-arrow_prev color-dark-to">
                                                    <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
                                                </div>
                                                <div class="swiper-arrow swiper-arrow_next color-dark-to">
                                                    <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
                                                </div>
    										<?php endif; ?>
                                        </div>
                                        <span class="color"><?php echo $count_variant; ?> colors</span>
                                        <!--                            <span class="width">Additional Widths</span>-->
                                    </div>
    							<?php endif; ?>
                                <!-- End Variant Slider -->
                                <a href="<?php echo $shop_url; ?>/products/<?php echo $product_handle; ?>" target="_blank"><h2 class="featured-products__title"><?php echo $product_title; ?></h2></a>
                                <p class="featured-products__cat"><?php echo $product_type; ?></p>
                                <div class="featured-products__footer">
                                    <span>$<?php echo $product_price; ?></span>
    								<?php if ( count($product_reviews["metafields"]) !== 0 ) : ?>
    									<?php echo $product_reviews["metafields"][0]['value']; ?>
    								<?php else : ?>
                                        <ul class="star-list">
                                            <li class="spr-icon-star-empty"></li>
                                            <li class="spr-icon-star-empty"></li>
                                            <li class="spr-icon-star-empty"></li>
                                            <li class="spr-icon-star-empty"></li>
                                            <li class="spr-icon-star-empty"></li>
                                        </ul>
    								<?php endif; ?>
                                </div>
                            </div>
                        </li>
                    <?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
        </ul>
	<?php else : ?>
        <ul class="featured-products <?php echo get_the_ID() == 57 ? 'featured-products_simple' : ''; ?>">
			<?php
			$new = $api_url . '/admin/collects.json?collection_id=' . $collection_ID . '&limit=250' ;
			// if ( false === ( $new_json = get_transient( 'shopify_collection_feed' . $collection_ID ) ) ) {
			//     $new_json = file_get_contents( $new );

			//     set_transient( 'shopify_product_feed' . $collection_ID, $new_json, 4 * HOUR_IN_SECONDS );
			// }						

			$new_json = file_get_contents( $new );			
			if ( $new_json !== FALSE ) { 
				$new_collection = json_decode( $new_json, true );																									
				$product_count = 0; 

				shuffle($new_collection['collects']);
				foreach ($new_collection['collects'] as $key => $product) : 																																
				if ($product_count <= 3): 
					$product_ID = $product['product_id'];					
					$product_url = $api_url . '/admin/products.json?ids=' . $product_ID;
					$product_reviews_url = $api_url . '/admin/products/' . $product_ID . '/metafields.json?namespace=stamped&key=badge&fields=value';
					$product_reviews_url_json;
					$product_content_json;
					$product;

					$product_content_json = file_get_contents( $product_url );																											
					if ( false === ( $product_reviews_url_json = get_transient( 'shopify_product_reviews' . $product_ID ) ) ) {
						$product_reviews_url_json = file_get_contents($product_reviews_url);
						set_transient( 'shopify_product_reviews' . $product_ID, $product_reviews_url_json, 4 * HOUR_IN_SECONDS );
					}

					$product = json_decode( $product_content_json, true );					
																												
					$product_content = $product['products'][0];							
					$product_published_at = $product_content['published_at'];
					$product_type = $product_content['product_type'];

					if ( $product_type != "Staging" && $product_published_at != NULL ) :
						$product_count++;	
						$product_handle = $product_content['handle'];
						$product_title = $product_content['title'];
						$product_type = $product_content['product_type'];
						$product_price = $product_content['variants'][0]['price'];
						$product_image_parts = pathinfo($product_content['images'][0]['src']);
						$product_image_src = $product_image_parts['dirname'] . '/' . $product_image_parts['filename'] . '_x200.' . $product_image_parts['extension'];
						$count_variant = 0;
						$count_variant_width = 0;
	
						$product_reviews = json_decode($product_reviews_url_json, true);
						
						if ( count($product_reviews["metafields"]) !== 0 ) {
							$product_reviews_data =  strstr($product_reviews["metafields"][0]['value'], '</style>');
							$product_rating = 0;
							if ($product_reviews_data) {
								libxml_use_internal_errors(true);
								$doc = new DOMDocument();
								$doc->loadHTML($product_reviews_data);
								$sxml = simplexml_import_dom($doc);
								$product_rating = $sxml->body->div->div->div->span->meta[3]['content'];
							}
						}
						?>
						<li class="featured-products__item" data-rating="<?php echo $product_rating; ?>">
							<div class="featured-product__inner">
								<a href="<?php echo $shop_url; ?>/products/<?php echo $product_handle; ?>" target="_blank" class="featured-product__link-img">
									<img class="featured-product__img" data-origin-img="<?php echo $product_image_src; ?>" src="<?php echo $product_image_src; ?>" alt="<?php echo $product_title; ?>">
								</a>
								<!-- Variant Slider -->
								<?php if ( count($product_content['variants']) > 1 ) : ?>
									<?php $option_index = 0; ?>
									<?php foreach ( $product_content['options'] as $key => $option ) : ?>
										<?php if ( $option['name'] == 'Color' || $option == 'color' ) : ?>
											<?php $option_index = 'option' . ($key + 1); ?>
										<?php endif; ?>
										<?php if ( $option['name'] == 'width' || $option == 'Width' ) : ?>
											<?php $option_index_width = 'option' . ($key + 1); ?>
										<?php endif; ?>
									<?php endforeach; ?>
									<div class="featured-products__body">
										<div class="featured-products__variant">
											<div class="swiper-container" data-product data-brand-color="<?php echo $GLOBALS['theme_color']; ?>">
												<div class="swiper-wrapper">
													<?php $values = array(); ?>
													<?php $values_width = array(); ?>
													<?php foreach ( $product_content['variants'] as $key => $product_variant ) : ?>
														<?php
															$variant_color = $product_variant[$option_index];
															$variant_width = $product_variant[$option_index_width];
															$variant_image_scr = '//cdn.shopify.com/s/assets/no-image-2048-5e88c1b20e087fb7bbe9a3771824e743c244f437e4f8ba93bbf7b11b53f7824c_x200.gif';
															$variant_image_scr_small = '//cdn.shopify.com/s/assets/no-image-100-c91dd4bdb56513f2cbf4fc15436ca35e9d4ecd014546c8d421b1aece861dfecf_65x.gif';
														?>
														<?php foreach ( $product_content['images'] as $images ) : ?>
															<?php
																$href = $shop_url . '/products/' . $product_handle . '?variant=' . $product_variant['id'];
																if ( $product_variant['id'] == $images['variant_ids'][0] ) {
																	$variant_image_parts = pathinfo($product_content['images'][$key]['src']);
																	$variant_image_scr = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_x200.' . $variant_image_parts['extension'];
																	$variant_image_scr_small = $variant_image_parts['dirname'] . '/' . $variant_image_parts['filename'] . '_65x.' . $variant_image_parts['extension'];
																}
															?>
														<?php endforeach; ?>
														<?php if ( !in_array($variant_color, $values) ) : ?>
															<?php
																$count_variant++;
																array_push($values, $variant_color);
															?>
															<div class="swiper-slide <?php echo $key == 0 ? 'active' : ''; ?>" data-hover-img="<?php echo $variant_image_scr; ?>">
																<a href="<?php echo $href; ?>" target="_blank"><img src="<?php echo $variant_image_scr_small; ?>" alt="<?php echo $variant_color; ?>"></a>
															</div>
														<?php endif; ?>
														<?php if ( !in_array($variant_width, $values_width) ) : ?>
															<?php
																$count_variant_width++;
																array_push($values_width, $variant_width);
															?>
														<?php endif; ?>
													<?php endforeach; ?>
												</div>
											</div>
											<?php if ( $count_variant > 3 ) : ?>
												<div class="swiper-arrow swiper-arrow_prev color-dark-to">
													<svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
												</div>
												<div class="swiper-arrow swiper-arrow_next color-dark-to">
													<svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
												</div>
											<?php endif; ?>
										</div>
										<span class="color"><?php echo $count_variant; ?> colors</span>
										<?php if ( $count_variant_width > 1 ) : ?>
											<span class="width">Additional Widths</span>
										<?php endif; ?>
									</div>
								<?php endif; ?>
								<!-- End Variant Slider -->
								<a href="<?php echo $shop_url; ?>/products/<?php echo $product_handle; ?>" target="_blank"><h2 class="featured-products__title"><?php echo $product_title; ?></h2></a>
								<p class="featured-products__cat"><?php echo $product_type; ?></p>
								<div class="featured-products__footer">
									<span>$<?php echo $product_price; ?></span>
									<?php if ( count($product_reviews["metafields"]) !== 0 ) : ?>
										<?php echo $product_reviews["metafields"][0]['value']; ?>
									<?php else : ?>
										<ul class="star-list">
											<li class="spr-icon-star-empty"></li>
											<li class="spr-icon-star-empty"></li>
											<li class="spr-icon-star-empty"></li>
											<li class="spr-icon-star-empty"></li>
											<li class="spr-icon-star-empty"></li>
										</ul>
									<?php endif; ?>
								</div>
							</div>
						</li>
					<?php endif; // if staging ?>
				<?php endif; // if key less than 4 ?>																							
			<?php endforeach; ?>
			<?php } // if new_json ?>
        </ul>
	<?php endif; ?>

</div>
