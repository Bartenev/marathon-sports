<?php
    // Get ACF row layout style
    $layout_style = get_sub_field( 'layout_style' );
    $post_type = 'eventbrite_events';
?>

<?php if ( $layout_style == "grid" ) :
    $section_title = get_sub_field( 'section_title' );
    $grid_event_category = get_sub_field( 'grid_event_category' );
    $grid_include_events_by = get_sub_field( 'grid_include_events_by' );
	if ( $grid_include_events_by == "category" ) {
		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'eventbrite_category',
					'field' => 'slug',
					'terms' => $grid_event_category,
				),
            ),
            'meta_key' => 'event_start_date', 
            'meta_value' => date('Y-m-d'),
            'meta_compare' => '>=',                     
            'orderby' => 'meta_value',
            'order' => 'ASC'
		);
	} else {
		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => -1,
            'meta_key' => 'event_start_date',
            'meta_value' => date('Y-m-d'),
            'meta_compare' => '>=',       
			'orderby' => 'meta_value',
			'order' => 'ASC'
		);
	}
	$query = new WP_Query( $args );
?>

    <!-- Eventbrite events: grid -->
	<?php if ( $query->have_posts() ) : ?>
        <?php if ( $query->found_posts > 4 ) :  ?>
            <div class="interactive-map">
                <div class="interactive-map__header">
                    <!--  Section title  -->
                    <div class="content">
                        <?php if ( $section_title ) : ?>
                            <h2 class="interactive-map__title"><?php echo $section_title; ?></h2>
                        <?php endif; ?>

                        <!--  Map toolbar  -->
                        <div class="interactive-map__nav">
                            <div class="interactive-map__search">
                                <span class="trigger color-dark-to">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                                        <defs>
                                            <style>
                                                .search-icon {
                                                    fill: none;
                                                    stroke: #000;
                                                    stroke-linecap: round;
                                                    stroke-miterlimit: 10;
                                                    stroke-width: 9px;
                                                }
                                            </style>
                                        </defs>
                                        <g id="svg-11" data-name="svg-11">
                                            <g><path class="search-icon" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"/></g>
                                        </g>
                                    </svg>
                                </span>
                                <input type="text" placeholder="Search by City, State, or Zip Code" id="search-map">
                            </div>
                            <a class="btn trigger__link">
                                <span>Near Me</span>
                            </a>
                        </div>

                    </div>
                </div>
                <!--  Map container  -->
                <div class="interactive-map__body">
                    <div id="map"></div>
                </div>
                <!--  Elements used to generate map pins  -->
                <div class="content">
                    <ul class="location-grid set-markers" data-post="<?php echo $post_type; ?>">
                        <?php $post_titles = array(); ?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <?php
                                // Check globally defined color scheme based on site logo
                                // and assign filter options to support map functionality
                                switch ( get_field( 'logo', 'option' ) ) {
                                    case "marathon_sports":
                                        $filter = "yellow";
                                        break;
                                    case "runners_alley":
                                        $filter = "red";
                                        break;
                                    case "sound_runner":
                                        $filter = "blue";
                                        break;
                                }

                                // Prevent duplicate posts from appearing in the grid
                                $post_title = get_the_title( $post->ID );
                                if ( in_array( $post_title, $post_titles  ) ) {
                                    $skip_post = true;
                                } else {
                                    array_push( $post_titles, $post_title );
                                    $skip_post = false;
                                }

                                // Get data from Eventbrite post
                                $venue_lat = get_post_meta( $post->ID, 'venue_lat' )[0];
                                $venue_lon = get_post_meta( $post->ID, 'venue_lon' )[0];

                                $venue_address = get_post_meta( $post->ID, 'venue_address' )[0];
                                $venue_city = get_post_meta( $post->ID, 'venue_city' )[0];
                                $venue_state = get_post_meta( $post->ID, 'venue_state' )[0];
                                $venue_zipcode = get_post_meta( $post->ID, 'venue_zipcode' )[0];

                                $event_date = get_post_meta( $post->ID, 'event_start_date' )[0];
                                $event_date_formatted = date("m/d/Y", strtotime( $event_date ));

                                $events_start_hour = get_post_meta( $post->ID, 'event_start_hour' )[0];
                                $events_start_minute = get_post_meta( $post->ID, 'event_start_minute' )[0];
                                $event_start_meridian = get_post_meta( $post->ID, 'event_start_meridian' )[0];

                                $event_url = get_post_meta( $post->ID, 'iee_event_link' )[0];
                            ?>

                            <?php if ( !empty($venue_address) && !$skip_post ) : ?>
                                <li data-marker-lat='<?php echo $venue_lat; ?>'
                                    data-marker-lng='<?php echo $venue_lon; ?>'
                                    data-marker-filter="<?php echo $filter; ?>"
                                    data-color="<?php echo $filter; ?>"
                                    data-image-url="<?php echo get_template_directory_uri() ?>/media/pin-<?php echo $filter; ?>.png"
                                    data-href="http://maps.google.com/maps?q=loc:<?php echo $venue_lat . ',' . $venue_lon; ?>"
                                    data-sity='<?php echo $venue_city; ?>'
                                    data-state='<?php echo $venue_state; ?>'
                                    data-zip='<?php echo $venue_zipcode; ?>'
                                    data-clubtitle='<?php echo get_the_title( $post->ID ); ?>'
                                    data-posturl='<?php echo $event_url; ?>'
                                    data-workhours='
                                    <?php
                                        $work_hours = array();
                                        $str = $event_date_formatted . ' - '. $events_start_hour . ':' . $events_start_minute . $event_start_meridian;
                                        array_push($work_hours, $str);
                                        echo json_encode($work_hours);
                                    ?>'
                                    class="location-grid__item no-filter" data-waypoint>

                                    <div class="location-grid__wrapper">
                                        <a href="<?php echo $event_url; ?>" class="location-grid__image">
                                            <div class="bg-cover" style="background-image:url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'location' ); ?>);"></div>
                                        </a>
                                        <div class="location-grid__body">
                                            <a href="<?php echo $event_url; ?>" target="_blank">
                                                <h2 class="location-grid__title">
                                                    <?php echo get_the_title( $post->ID ) . '<span>' . $venue_address . '</span>'; ?>
                                                </h2>
                                            </a>
                                            <a href="<?php echo $event_url; ?>" class="location-grid__link">learn more</a>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
	<?php wp_reset_query(); ?>

<?php else : ?>
    <!-- Eventbrite events: slider -->
    <?php
        $section_title = get_sub_field( 'section_title' );
        $section_sub_title = get_sub_field( 'section_sub_title' );

        $linked_button = get_sub_field( 'linked_button' );
        $linked_button_text = $linked_button['text'];
        $linked_button_url = $linked_button['url'];

        $linked_button_target = $linked_button['target'];
        if ( isset( $linked_button ) && !empty( $linked_button ) ) {
          $linked_button_target = "_blank";
        } else {
          $linked_button_target = "_self";
        }

        $slider_include_events_by = get_sub_field( 'slider_include_events_by' );
        if ( $slider_include_events_by == "category" ) {
            $slider_event_category = get_sub_field( 'slider_event_category' );
        } else if ( $slider_include_events_by == "manual" ) {
            $selected_events = get_sub_field( 'selected_events', false, false );
        } else if ( $slider_include_events_by == "all" ) {
            $include_all_events = true;
        }
    ?>
    <?php if ( $slider_include_events_by == 'category' || $include_all_events ) : ?>
        <?php if ( $slider_include_events_by == 'category' && !empty($slider_event_category) ) : ?>
            <?php // Build query based on selected category
                $args = [
                    'post_type' => $post_type,
                    'posts_per_page' => 24,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'eventbrite_category',
                            'field' => 'slug',
                            'terms' => $slider_event_category,
                        ),
                    ),
                    'meta_key' => 'event_start_date', 
                    'meta_value' => date('Y-m-d'),
                    'meta_compare' => '>=',                     
                    'orderby' => 'meta_value',
                    'order' => 'ASC'
                ];
                // Execute query using $args array
                $query = new WP_Query( $args );            
            ?>
        <?php endif; ?>

        <?php if  ($include_all_events ) : ?>
            <?php // Build query based on selected category
                $args = [
                    'post_type' => $post_type,
                    'posts_per_page' => -1,
                    'meta_key' => 'event_start_date',
                    'meta_value' => date('Y-m-d'),
                    'meta_compare' => '>=',                    
                    'orderby' => 'meta_value',
                    'order' => 'ASC'
                ];
                // Execute query using $args array
                $query = new WP_Query( $args );            
            ?>
        <?php endif; ?>

		<?php if ( $query->have_posts() ) : ?>
            <div class="title-row <?php echo !$linked_button_text ? 'title-row_heading' : 'title-row_light'?>">
                <div class="content">
                    <?php if ( $section_title ) : ?>
                        <h2><?php echo $section_title; ?>
                            <?php if ( $section_sub_title ) : ?>
                                <span><?php echo $section_sub_title; ?></span>
                            <?php endif; // section sub-title ?>
                        </h2>
                    <?php endif; // section title ?>

                    <?php if( $linked_button_text ) : ?>
                        <a href="<?php echo $linked_button_url; ?>" target="<?php echo $linked_button_target; ?>" class="brand-dark-to">
                            <?php echo $linked_button_text ?>
                        </a>
                    <?php endif; // header_type ?>
                </div>
            </div>

            <div class="upcoming-events <?php echo !$linked_button_text ? 'upcoming-events_no-top-padding' : ''?>">
                <div class="upcoming-events__inner">
                    <div class="content swiper-container" data-event data-brand-color="<?php echo $GLOBALS['theme_color']; ?>">
                        <div class="swiper-wrapper">
                            <?php while ( $query->have_posts() ) : $query->the_post();
                                $event_date = get_post_meta( $post->ID, 'event_start_date' )[0];                                
                                $event_url = get_post_meta( $post->ID, 'iee_event_link' )[0];
                                $categories = get_the_terms($post->ID, 'eventbrite_category');
                                $image = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'full_hd')[0];
                                $content = wp_strip_all_tags( get_the_content() ); ?>

                                    <a href="<?php echo $event_url; ?>" class="upcoming-events__item swiper-slide" target="_blank">
                                        <div class="upcoming-events__date brand brand--color-white">
                                            <p><?php echo date('M', strtotime($event_date)); ?></p>
                                            <span><?php echo date('d', strtotime($event_date)); ?></span>
                                        </div>
                                        <div class="upcoming-events__image">
                                            <div class="bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
                                        </div>
                                        <div class="upcoming-events__body brand-to brand--color-white">
                                            <h2 class="upcoming-events__title"><?php the_title(); ?>
                                                <?php if ( !empty($categories) ) : ?>
                                                    <?php $event_categories = array(); ?>
                                                    <span>
                                                        <?php 
                                                            foreach ($categories as $key=>$category) :                                                           
                                                                array_push($event_categories, $category->name);                                                            
                                                            endforeach; 
                                                            echo implode(", ", $event_categories);
                                                        ?>
                                                    </span>
                                                <?php endif; // !empty($categories) ?>
                                            </h2>
                                            <p class="upcoming-events__description">
                                                <?php echo new_excerpt_length($content); ?>
                                            </p>
                                            <span class="upcoming-events__link">learn more</span>
                                        </div>
                                    </a>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); wp_reset_query(); ?>
                        </div>
                    </div>

                    <!--  Slider pagination links / arrow icons  -->                    
                    <?php if ( $query->post_count > 4 ) : ?>
                        <div class="swiper-arrow swiper-arrow_prev color-dark-to">
                            <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
                        </div>
                        <div class="swiper-arrow swiper-arrow_next color-dark-to">
                            <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
                        </div>                        
                    <?php endif; ?>                    
                </div>
            </div>
		<?php endif; ?>
	<?php endif; ?>


	<?php if ( $slider_include_events_by == 'manual' && $selected_events ) : ?>
        <div class="title-row <?php echo !$linked_button_text ? 'title-row_heading' : 'title-row_light'?>">
            <div class="content">
				<?php if ( $section_title ) : ?>
                    <h2><?php echo $section_title; ?>
						<?php if ( $section_sub_title ) : ?>
                            <span><?php echo $section_sub_title; ?></span>
						<?php endif; // section sub-title ?>
                    </h2>
				<?php endif; // section title ?>

				<?php if( $linked_button_text ) : ?>
                    <a href="<?php echo $linked_button_url; ?>" target="<?php echo $linked_button_target; ?>" class="brand-dark-to">
						<?php echo $linked_button_text ?>
                    </a>
				<?php endif; // header_type ?>
            </div>
        </div>

        <div class="upcoming-events <?php echo !$linked_button_text ? 'upcoming-events_no-top-padding' : ''?>">
            <div class="upcoming-events__inner">
                <div class="content swiper-container" data-event data-brand-color="<?php echo $GLOBALS['theme_color']; ?>">
                    <div class="swiper-wrapper">
                        <?php if ( $selected_events ) : ?>
                            <?php foreach( $selected_events as $post ) : ?>
                                <?php
                                setup_postdata($post);
                                $event_date = get_post_meta( $post, 'event_start_date' )[0];
                                $event_url = get_post_meta( $post, 'iee_event_link' )[0];
                                $categories = get_the_terms($post, 'eventbrite_category');
                                $content = wp_strip_all_tags( get_the_content(), true );
                                $image = wp_get_attachment_image_src(get_post_thumbnail_id( $post ), 'full_hd')[0];
                                ?>
                                <a href="<?php echo $event_url; ?>" class="upcoming-events__item swiper-slide" target="_blank">
                                    <div class="upcoming-events__date brand brand--color-white">
                                        <p><?php echo date('M', strtotime($event_date)); ?></p>
                                        <span><?php echo date('d', strtotime($event_date)); ?></span>
                                    </div>
                                    <div class="upcoming-events__image">
                                        <div class="bg-cover" style="background-image: url(<?php echo $image; ?>);"></div>
                                    </div>
                                    <div class="upcoming-events__body brand-to brand--color-white">
                                        <h2 class="upcoming-events__title"><?php the_title(); ?>
                                            <?php if ( !empty($categories) ) : ?>
                                                 <?php $event_categories = array(); ?>
                                                <span>
                                                    <?php 
                                                        foreach ($categories as $key=>$category) :                                                           
                                                            array_push($event_categories, $category->name);                                                            
                                                        endforeach; 
                                                        echo implode(", ", $event_categories);
                                                    ?>
                                                </span>
                                            <?php endif; // !empty($categories) ?>
                                        </h2>
                                        <p class="upcoming-events__description"><?php echo new_excerpt_length( $content ); ?></p>
                                        <span class="upcoming-events__link">learn more</span>
                                    </div>
                                </a>
                            <?php endforeach; // foreach ?>
                            <?php wp_reset_postdata(); wp_reset_query(); ?>
                        <?php endif; // $selected_events ?>
                    </div>
                </div>

                 <!--  Slider pagination links / arrow icons  -->                                      
                <?php if ( count($selected_events) > 4 ) : ?>
                    <div class="swiper-arrow swiper-arrow_prev color-dark-to">
                        <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
                    </div>
                    <div class="swiper-arrow swiper-arrow_next color-dark-to">
                        <svg width="26.7" height="55.3" viewBox="0 0 26.7 55.3"><style>.s0{fill:none;stroke-linecap:round;stroke-width:4;stroke:#231f20;}</style><defs><clipPath clipPathUnits="userSpaceOnUse"><path d="m0 44.2 21.3 0L21.3 0 0 0 0 44.2Z"/></clipPath></defs><g transform="matrix(1.25,0,0,-1.25,0,55.285625)"><g clip-path="url(#clipPath16)"><g transform="translate(2.0005,2)"><path d="M0 0 17.3 20.1" class="s0"/></g><g transform="translate(12.5259,30.0157)"><path d="M0 0C-4.9 5.6-10.5 12.2-10.5 12.2" class="s0"/></g></g></g></svg>
                    </div>
                <?php endif; ?>                    
            </div>
        </div>
	<?php endif; ?>
<?php endif; ?>
