<!-- Multi Column -->
<div class="multi-column">
    <?php if( have_rows('column') ) : ?>
        <?php while (have_rows('column')) : the_row(); ?>
            <?php
                $background_type = get_sub_field('background_type');
                $image_position = get_sub_field('image_position');
		        $image_overlay = get_sub_field('image_overlay');
                $image = wp_get_attachment_image_src(get_sub_field('image')['ID'], 'full_hd')[0];
                $color = get_sub_field('color');
                $heading = get_sub_field('heading');

                // Get button clone field options    
                $button_text = get_sub_field( 'linked_button_button_text' );

                $link_type = get_sub_field( 'linked_button_link_type' );
                if ( $link_type == "internal" ) {
                    $button_link = get_sub_field( 'linked_button_page_link' );
                } else {
                    $button_link = get_sub_field( 'linked_button_link_url' );
                }

                $link_behavior = get_sub_field( 'linked_button_link_behavior' );
                if ( $link_behavior[0] == "blank" ) { 
                    $open_in_new_tab = true; 
                } else {
                    $open_in_new_tab = false;
                }  
            ?>
            <div class="multi-column__col">
                <div class="multi-column__bg bg-cover bg-cover<?php echo $image_position; ?>" style="<?php echo $background_type == 'image' ? 'background-image: url(' . $image .')' : 'background-color:' . $color . ';'; ?>">
                    <?php if ( $background_type == 'image' && $image_overlay ) : ?>
                        <div class="multi-column__bg-overlay"></div>
                    <?php endif; ?>
                </div>
                <div class="multi-column__title"><?php echo $heading; ?></div>
                <?php if ( $button_text ) { ?>
                    <a href="<?php echo $button_link; ?>" <?php if ( $open_in_new_tab ) { echo "target='_blank'"; } ?> class="btn brand brand--color-white">
                        <span><?php echo $button_text; ?></span>
                    </a>
                <?php } ?>
            </div>
        <?php endwhile; ?>
    <?php endif;  ?>
    <?php wp_reset_postdata(); ?>
</div>