<!-- Featured Link Bar -->
<div class="link-bar">
    <ul class="link-bar__inner">
        <?php if( have_rows('link') ) : ?>
            <?php while (have_rows('link')) : the_row(); ?>
                <?php
                    $subheading = get_sub_field('subheading');
                    $heading = get_sub_field('heading');
                    
                    // Get button clone field options
                    $link_type = get_sub_field( 'linked_button_link_type' );   
                    if ( $link_type == "internal" ) {
                        $button_link = get_sub_field( 'linked_button_page_link' );
                    } else {
                        $button_link = get_sub_field( 'linked_button_link_url' );
                    }

                    $link_behavior = get_sub_field( 'linked_button_link_behavior' );                    
                    if ( $link_behavior[0] == "blank" ) { 
                        $open_in_new_tab = true; 
                    }  else {
                        $open_in_new_tab = false;
                    }          
                                        
                ?>
                <li class="link">
                    <a href="<?php echo $button_link; ?>" <?php if ( $open_in_new_tab ) { echo "target='_blank'"; } ?>>
                        <span><?php echo $subheading; ?></span><?php echo $heading; ?>
                    </a>
                    <i class="link__hover brand"></i>
                </li>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </ul>
</div>