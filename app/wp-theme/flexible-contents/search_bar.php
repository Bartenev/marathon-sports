<?php
	$heading = get_sub_field('heading');
	$link = get_sub_field('link');
	$link_label = get_sub_field('link_label');
	$link_url = get_sub_field('link_url');
?>
<div class="search-bar">
	<h2>
		<?php echo $heading ?>
		<?php if ( $link == 'show' ) : ?>
			<a href="<?php echo $link_url; ?>"><?php echo $link_label; ?></a>
		<?php endif; ?>
	</h2>
    <form action="<?php echo home_url( '/' ); ?>">
        <input type="search" value="<?php echo get_search_query() ?>" placeholder="Search" name="s">
        <span class="trigger color-dark-to">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                    <defs>
                        <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                    </defs>
                    <g id="svg-1" data-name="svg-1">
                        <g>
                            <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"/>
                        </g>
                    </g>
                </svg>
            </span>
    </form>
</div>