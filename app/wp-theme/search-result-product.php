<?php
    $shop_url = get_field('shopify_site_url', 'option');
    $id = get_the_id();
    $meta_obj = get_post_meta($id);
    $product_id = $meta_obj['product_id'][0];
    $product_price = $meta_obj['product_price'][0];
    $product_link = $shop_url.'/products/'.$meta_obj['product_handle'][0];
    $image = $meta_obj['product_thumbnail'][0];
    $vendor = $meta_obj['product_vendor'][0];
?>

<li class="post-list-item" data-vendor="<?php echo $vendor; ?>" data-tag="">
    <div class="post-item ">
        <?php if ( !empty($image) ) : ?>
            <a href="<?php echo $product_link; ?>" class="post-item__image">
                <div class="post-list-item__img bg-contain" style="background-image: url(<?php echo $image; ?>);"></div>
            </a>
        <?php endif; ?>
        <div class="post-item__content">
            <a href="<?php echo $product_link; ?>">
                <h2><?php the_title(); ?></h2>
            </a>
            <div class="post-list-item__content">
                <?php the_excerpt(); ?>
            </div>
            <a href="<?php echo $product_link; ?>" class="btn brand brand--color-white"><span>READ MORE</span></a>
        </div>
    </div>
</li>
