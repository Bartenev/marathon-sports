<?php
/**
 * The Header for our theme.
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
	<meta name="format-detection" content="telephone=no">
	<title><?php wp_title("|", true); ?></title>
	<?php
	$theme_logo = get_field('logo', 'option');

	if ( $theme_logo == 'runners_alley' ) {
      $GLOBALS['theme_color'] = 'red';
      $GLOBALS['theme_hex'] = '#df002e';
      $GLOBALS['site_slug'] = $theme_logo;
	} else if ( $theme_logo == 'sound_runner' ) {
      $GLOBALS['theme_color'] = 'blue';
      $GLOBALS['theme_hex'] = '#10a6de';
      $GLOBALS['site_slug'] = $theme_logo;
	}else {
      $GLOBALS['theme_color'] = 'yellow';
      $GLOBALS['theme_hex'] = '#fcdd3e';
      $GLOBALS['site_slug'] = 'marathon_sports';
	}

	$body_class = 'color-theme-' . $GLOBALS['theme_color'];
	$shop_url = get_field('shopify_site_url', 'option'); ?>
	<meta name="theme-color" content="<?php echo $GLOBALS['theme_hex']; ?>">
	<link type="text/plain" rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt" />
	<?php /* <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" /> */ ?>

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

	<?php $top_scripts = get_field('top_scripts', 'option');
	if (strlen($top_scripts) && $_ENV['PANTHEON_ENVIRONMENT'] === 'live') {
	  echo $top_scripts;
	} ?>

	<script>
		(function(d) {
			var config = {
				kitId: 'esf0ghb',
				scriptTimeout: 3000,
				async: true
			},
			h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
		})(document);
	</script>

	<script>
		// Asynchronously load non-critical css
		function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }
		// load css file async
		loadCSS("//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css");
	</script>

	<?php
		wp_head();
	?>
</head>
<body <?php body_class($body_class); ?> data-color-brand="<?php echo $GLOBALS['theme_color']; ?>" data-site-slug="<?php echo $GLOBALS['site_slug']; ?>" data-shop-url="<?php echo $shop_url; ?>">
	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<?php
        $live_chat = get_field('live_chat_link', 'option');
	    $my_account = get_field('my_account_link', 'option');
	?>
	<header class="header">
		<div class="content">
			<a href="<?php echo get_option('home'); ?>" class="site-logo header__logo brand">
				<img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-<?php echo $theme_logo; ?>.svg" alt="<?php echo get_bloginfo('site_name'); ?>">
			</a>
			<div class="header__nav">
				<div class="secondory-nav">
					<div class="secondory-nav__brand">
            <?php
                $marathon_sports = get_field('marathon_sports', 'option');
                $runners_alley_url = get_field('runners_alley_url', 'option');
                $sound_runner_url = get_field('sound_runner_url', 'option');
            ?>
            <?php if ( $theme_logo == 'marathon_sports' ) : ?>
                <a href="<?php echo $sound_runner_url; ?>" class="secondory-nav__link"><img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-sound_runner_top.svg" alt="Sound Runner"></a>
                <a href="<?php echo $runners_alley_url; ?>" class="secondory-nav__link"><img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-runners_alley_top.svg" alt="Runner's Alley"></a>
            <?php elseif ( $theme_logo == 'runners_alley' ) : ?>
                <a href="<?php echo $marathon_sports; ?>" class="secondory-nav__link" style="position:relative;top: 3px;"><img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-marathon_sports_top.svg" style="width: 85px;" alt="Marathon Sports"></a>
                <a href="<?php echo $sound_runner_url; ?>" class="secondory-nav__link"><img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-sound_runner_top.svg" alt="Sound Runner"></a>
            <?php else : ?>
                <a href="<?php echo $marathon_sports; ?>" class="secondory-nav__link" style="position:relative;top: 3px;"><img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-marathon_sports_top.svg" style="width: 85px;" alt="Marathon Sports"></a>
                <a href="<?php echo $runners_alley_url; ?>" class="secondory-nav__link"><img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-runners_alley_top.svg" alt="Runner's Alley"></a>
            <?php endif; ?>
					</div>
					<div class="secondory-nav__link">
                        <?php if ( $live_chat ) { ?>
                            <a href="<?php echo $live_chat; ?>">Live Chat</a>
                        <?php } ?>
                        <?php if ( $my_account ) { ?>
                            <a href="<?php echo $my_account; ?>" target="_blank">My Account</a>
                        <?php } ?>
					</div>
				</div>
				<nav class="primary-nav">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'container' => false,
							"walker" => new _walker_nav_menu(),
						) );
					?>
                    <div class="unique-nav">
                        <form action="<?php echo $shop_url . '/search?q=' . get_search_query() ?>" class="form-search">
                            <input type="search" value="<?php echo get_search_query() ?>" placeholder="Search" name="q">
                            <input type="hidden" value="product" name="type">   
                            <input type="hidden" value="<?php echo $theme_logo ?>" name="from">                          
                        </form>
                        <span class="trigger-search color-dark-to">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                                <defs>
                                    <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                                </defs>
                                <g id="svg-1" data-name="svg-1">
                                    <g>
                                        <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"/>
                                    </g>
                                </g>
                            </svg>
                        </span>
                        <span class="slash">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40.38 41.59">
                                <defs>
                                    <style>.slash-1{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4px;}</style>
                                </defs>
                                <g id="svg-2" data-name="svg-2">
                                    <g>
                                        <line class="slash-1" x1="2" y1="2" x2="38.38" y2="39.59"/>
                                    </g>
                                </g>
                            </svg>
                        </span>
                        <?php $card_url = $shop_url . '/cart.json'; ?>
                        <a href="<?php echo $shop_url . '/cart'; ?>" target="_blank" class="btn-cart color-dark-to" data-shop-url="<?php echo $card_url; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 103.77 92.09">
                                <defs>
                                    <style>.cart-1,.cart-2{fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:7px;}.cls-2{stroke-linecap:round;}</style>
                                </defs>
                                <g id="svg-3" data-name="svg-3">
                                    <g>
                                        <path class="cart-1" d="M19.66,22.92l6.83,42.55H92.84l6.83-42.55H43.5"/>
                                        <path class="cart-1" d="M43.5,22.92h-4"/>
                                        <circle class="cart-1" cx="79.68" cy="80.27" r="8.32"/>
                                        <circle class="cart-1" cx="39.65" cy="80.27" r="8.32"/>
                                        <polyline class="cart-2" points="19.68 22.92 16.91 3.5 3.5 3.5"/>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </div>
				</nav>
			</div>
		</div>
	</header>
    <header class="header_mobile brand">
        <div class="content">
            <div class="mobile-trigger">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <a href="<?php echo get_settings('home'); ?>" class="site-logo header__logo">
                <img src="<?php echo get_template_directory_uri(); ?>/media/theme_logo/icon-<?php echo $theme_logo; ?>.svg" alt="<?php echo get_bloginfo('site_name'); ?>">
            </a>
            <div class="unique-nav">
                <form action="<?php echo $shop_url . '/search?q=' . get_search_query(); ?>" class="form-search">
                    <input type="search" value="<?php echo get_search_query() ?>" placeholder="Search" name="q">
                </form>
                <span class="trigger-search color-dark-to">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 101.38 98.35">
                        <defs>
                            <style>.search{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:9px;}</style>
                        </defs>
                        <g id="svg-4" data-name="svg-4">
                            <g>
                                <path class="search" d="M72.73,44a34.31,34.31,0,1,0-9.66,19.09l.18-.18,33.63,31"/>
                            </g>
                        </g>
                    </svg>
                </span>
                <span class="slash">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40.38 41.59">
                        <defs>
                            <style>.slash-1{fill:none;stroke:#000;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4px;}</style>
                        </defs>
                        <g id="svg-5" data-name="svg-5">
                            <g>
                                <line class="slash-1" x1="2" y1="2" x2="38.38" y2="39.59"/>
                            </g>
                        </g>
                    </svg>
                </span>
                <a href="<?php echo $shop_url . '/cart'; ?>" target="_blank" class="btn-cart" data-shop-url="<?php echo $card_url; ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 103.77 92.09">
                        <defs>
                            <style>.cart-1,.cart-2{fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:7px;}.cls-2{stroke-linecap:round;}</style>
                        </defs>
                        <g id="svg-6" data-name="svg-6">
                            <g>
                                <path class="cart-1" d="M19.66,22.92l6.83,42.55H92.84l6.83-42.55H43.5"/>
                                <path class="cart-1" d="M43.5,22.92h-4"/>
                                <circle class="cart-1" cx="79.68" cy="80.27" r="8.32"/>
                                <circle class="cart-1" cx="39.65" cy="80.27" r="8.32"/>
                                <polyline class="cart-2" points="19.68 22.92 16.91 3.5 3.5 3.5"/>
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
        <nav class="primary-nav_mobile">
            <div class="primary-nav_body">
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'container' => false,
                        'walker' => new _walker_nav_menu(),
                        'menu_class' => 'mobile_menu'
                    ) );
                ?>
            </div>
            <div class="primary-nav_footer">
                <?php if ( $live_chat ) { ?>
                    <a href="<?php echo $live_chat; ?>">Live Chat</a>
                <?php } ?>
                <?php if ( $my_account ) { ?>
                    <a href="<?php echo $my_account; ?>" target="_blank">My Account</a>
                <?php } ?>
            </div>
        </nav>
    </header>
