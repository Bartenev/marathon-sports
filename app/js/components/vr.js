import GoogleMapsLoader from 'google-maps';

var svService, container = $(".vr-pano");

export default function vr() {
	GoogleMapsLoader.load(function(google) {
		svService = new google.maps.StreetViewService();
		container.each(function(index, el) {
			initPano(el);
		});
	});
}

function initPano(holder) {
	var panorama = new google.maps.StreetViewPanorama(holder, {});

	svService.getPanoramaById( holder.dataset.pano, function (data, status) {
		if (status == google.maps.StreetViewStatus.OK) {
			panorama.setPano(data.location.pano);
			panorama.setVisible(true);
		}
	});
}