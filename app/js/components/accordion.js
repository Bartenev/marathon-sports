const $accordionControl = $('.c-accordion__item');

const accordion = {
    init: () => $accordionControl.length ? toggle() : this,
};

function toggle() {
    $accordionControl.on('click', function () {
        let $this = $(this);
        $this.toggleClass('active');
        let $panel = $this.find('.c-accordion__inner');
        let panel = $panel.get(0);
        if ($panel.attr('style')) {
            $panel.removeAttr('style');
        }
        else {
            panel.style.maxHeight = panel.scrollHeight + 'px';
        }
    });
}

export default accordion;