/**
* @typedef options
* @see https://developers.google.com/youtube/iframe_api_reference#Loading_a_Video_Player
* @param {Number} width 
* @param {Number} height 
* @param {String} videoId 
* @param {Object} playerVars 
* @param {Object} events 
*/

/**
* @typedef YT.Player
* @see https://developers.google.com/youtube/iframe_api_reference
* */

/**
* A factory function used to produce an instance of YT.Player and queue function calls and proxy events of the resulting object.
*
* @param {YT.Player|HTMLElement|String} elementId Either An existing YT.Player instance,
* the DOM element or the id of the HTML element where the API will insert an <iframe>.
* @param {YouTubePlayer~options} options See `options` (Ignored when using an existing YT.Player instance).
* @param {boolean} strictState A flag designating whether or not to wait for
* an acceptable state when calling supported functions. Default: `false`.
* See `FunctionStateMap.js` for supported functions and acceptable states.
* @returns {Object}
*/
import YouTubePlayer from 'youtube-player';
import Player from '@vimeo/player';

import Lightbox from './lightbox';

var VideoPlayer = {
	players: [],
	current: null,
	init: function () {
		var that = this;
		var container = $('[data-video]');
		var lbox = new Lightbox( $('#'+container.data('lightbox-bind')) );

		$('.hero-slide__video-caption').on('click', '.btn', function(e) {
			e.preventDefault();
			var playerContainer = $(this).closest(container).find('.video-player').clone();
			var type = $(this).closest(container).data('player');

			if( type === 'vimeo' ) {
				that.current = new Player(playerContainer.find('iframe')[0]);
				that.current.play();
				
			}
			// if( type === 'youtube' ) {
			// 	that.current = YouTubePlayer(playerContainer[0])
			// }

			lbox.append(playerContainer);
			lbox.open();
		});



	},
	load: function (container) {
		
		if( this.players[container.index()] === undefined ) {
			// NEW PLAYER 
			var player;
			var type = container.data('player');
			var id = container.data('video');
			var element = container.find('.video-player')[0];

			if( type === 'vimeo' ) {
				player = new Player(element, {
					id: id,
					loop: true
				});
				player.ready().then(function () {
					player.setVolume(0);
					player.play();
					player.on('progress', function(event) {
						
						container.addClass('play');
					});
					
					
				});
				
			}

			if( type === 'youtube' ) {
				player = YouTubePlayer(element, {
					videoId: id,
					playerVars: {
						enablejsapi: 1,
						autoplay: 1,
						loop: 1,
                        playlist: id
					}
				});
				
				player.on('ready', function(state) {
					player.mute();
					
					player.on('stateChange', function(state) {
						setTimeout(function () {
							container.addClass('play');
						}, 2000);
					});
				});
				
			}
			
			this.players.push(player);
		}
	}
};
export default VideoPlayer;