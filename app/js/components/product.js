var productWrap = $('.featured-products'),
    product = productWrap.find('.featured-products__item');

var productRow = {
    init: function () {
        if ( productWrap.length && !productWrap.is('.featured-products__collection') ) {
            productWrap.each(function () {
                var _this = $(this);
                $(this).find(product).each(function () {
                    if ( $(this).find('.swiper-slide').length > 1 && window.innerWidth > 1024 ) {
                    }else {
                        $(this).find('.featured-products__variant').hide();
                        $(this).find('.color').hide();
                    }
                });
            });
        }else {
            $('body .featured-products__item').each(function () {
                if ( $(this).find('.swiper-slide').length <= 1 ) {
                    $(this).find('.featured-products__variant').hide();
                    $(this).find('.color').hide();
                }
            });
        }

        this.event();
    },
    event: function () {
        product.find('.swiper-slide').on({
            'mouseenter' : function () {
                changeImage($(this), $(this).data('hover-img'));
            },
            'click' : function () {
                changeImage($(this), $(this).data('hover-img'));
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                $(this).parents('.featured-product__inner').find('.featured-product__img').data('origin-img', $(this).data('hover-img'));
            }
        });

        $('body .featured-products__item').on({
            'mouseenter': function () {
                if ( $(this).find('.swiper-slide').length > 1 ) {
                    $(this).height('initial');
                }
            },
            'mouseleave': function () {
                $(this).height(420);
            }
        });

        product.find('.swiper-container').on({
            'mouseleave' : function () {
                changeImage($(this));
            },
        });

        $(window).on('resize', function () {
            if ( window.innerWidth > 1024 && !productWrap.is('.featured-products__collection') ) {
                productWrap.removeAttr('style');
            }
        });
    }
};

function changeImage(_this, image) {
    var bigImage = _this.parents('.featured-product__inner').find('.featured-product__img'),
        originImage = bigImage.data('origin-img');

    function change(url) {
        bigImage.addClass('hide');
        setTimeout(function () {
            bigImage.attr('src', url);
        }, 250);
        setTimeout(function () {
            bigImage.removeClass('hide');
        }, 350);
    }

    if ( image !== undefined ) {
        change(image);
    }else {

        change(originImage);
    }

}

export default productRow;