var menu, menuItem, menuItemInner, menuItemChildren, searchWrap , searchOpen, mobileBtn, footerLInk, btnCart;

var navigation = {
	init: function () {
        menu = $("body .menu");
        menuItem = $("body .menu > li");
        menuItemInner = $("body .primary-nav .menu > li > .menu__dropdown > .sub-menu li");
        menuItemChildren = $("body .menu > .menu-item-has-children");
        searchWrap = $('.unique-nav');
        searchOpen = false;
        mobileBtn = $('.mobile-trigger');
        footerLInk = $('.footer-nav li');
        btnCart = $('.btn-cart');

	    getCountCart();
        setColorBrand();
        event();
    }
};

export default navigation;

function getCountCart() {
    if ( !btnCart.is('.btn-cart-shopify') ) {
        jQuery.ajax({
            url: btnCart.data('shop-url'),
            contentType: "application/json; charset=utf-8",
            dataType: "jsonp",
            crossDomain: true,
            success: function( data ) {
                var count = data['item_count'];
                if ( count !== 0 ) {
                    btnCart.prepend('<span class="count brand">' + count + '</span>');
                }
            },
            error: function (data) {
                console.log( data );
            }
        });
    }
}

function setColorBrand() {
	var color = $('body').data('color-brand');

    footerLInk.each(function () {
        $(this).addClass('color-to');
    });

    // Set Color Brand
	menuItem.each(function() {
		$(this).find('.link__hover').eq(0).addClass('brand');

		if ( $(this).find('.menu__dropdown').eq(0) ) {
			$(this).find('.menu__dropdown').eq(0).css({
				'left' : -$(this).offset().left
			});
		}
	});
}

function triggerSearch() {
    searchWrap.toggleClass('search-active');
    searchOpen = (searchOpen === true) ? false : true;
    if ( searchOpen ) {
        setTimeout(function () {
            searchWrap.find('input').focus();
        }, 400);
    }
}

function triggerHeight(elem, height) {
    elem.height(height);
}

function event() {
    $('body').on({
        'keyup' : function (e) {
            if (e.keyCode === 27) {
                triggerSearch();
            }
        },
        'click' : function(e) {
            if ( !searchWrap.is(e.target) && searchWrap.has(e.target).length === 0 && window.innerHeight > 1024 && searchOpen ) {
                triggerSearch();
            }
        }
    });

    $(document).on('click touchstart', function (e) {
        if ( !searchWrap.is(e.target) && searchWrap.has(e.target).length === 0 && window.innerHeight <= 1024 && searchOpen ) {
            triggerSearch();
        }
    });

    $(window).on('resize', function() {
        setColorBrand();
    });

    // Search Form
    $('.trigger-search').on('click', function() {
        if ( !searchOpen ) {
            triggerSearch();
        }else {
             $('.form-search').submit();
        }
    });

    menuItemChildren.on({
        'mouseenter' : function() {
            var height = $(this).find('.sub-menu').eq(0).innerHeight();
            var subMenuWrap = $(this).find('.menu__dropdown').eq(0);

            triggerHeight(subMenuWrap, height);
            setColorBrand();
        },
        'mouseleave' : function() {
            var subMenuWrap = $(this).find('.menu__dropdown').eq(0);

            triggerHeight(subMenuWrap, 0);
        }
    });

    [
        menuItem,
        menuItemInner,
        $('.sidebar__item .cat-item'),
        $('.share-widget li'),
        $('.sidebar .wpp-list li')].forEach(function (el) {
        el.on({
            'mouseenter': function () {
                $(this).siblings().addClass('not-hover');
            },
            'mouseleave' : function() {
                $(this).siblings().removeClass('not-hover');
            }
        });
    });

    // Mobile Menu
    mobileBtn.on('click', function() {
        $('.header_mobile').toggleClass('open');
    });

    $('.mobile_menu > .menu-item-has-children').on('click', function (e) {
        var height = $(this).find('.sub-menu').eq(0).height(),
            itemWrap = $(this).find('.menu__dropdown').eq(0);

        if ( !$(this).is('.active') ) {
            e.preventDefault();
            triggerHeight(itemWrap, height);
            $(this).addClass('active');

        }else if ( $(this).is('.active') && !$('.mobile_menu > .menu-item-has-children.active .sub-menu .menu-item-has-children a').is(e.target) ) {
            triggerHeight(itemWrap, 0);
            triggerHeight($(this).find('.menu__dropdown'), 0);
            $(this).add($(this).find('.menu-item-has-children')).removeClass('active');
        }
    });

    $('.mobile_menu .sub-menu .menu-item-has-children').on('click', function(e) {
        var height = $(this).find('.sub-menu').eq(0).height(),
            itemWrap = $(this).find('.menu__dropdown').eq(0),
            parentHeight = $(this).parents('.menu-item-has-children').find('.sub-menu').eq(0).height(),
            parentWrap = $(this).parents('.menu-item-has-children').find('.menu__dropdown').eq(0);

        if ( $(this).parents('.menu-item-has-children').is('.active') && !$(this).is('.active') ) {
            e.preventDefault();
            triggerHeight(parentWrap, height + parentHeight);
            triggerHeight(itemWrap, height);
            $(this).addClass('active');
        }else {
            triggerHeight(parentWrap, parentHeight - height);
            triggerHeight(itemWrap, 0);
            $(this).removeClass('active');
        }
    });

    // wp add link site name
    if ( $('body').attr('data-site-slug') !== undefined ) {
        $('a').not('[data-map]').not('.secondory-nav__link').on('click', function (e) {
            var slug = $('body').data('site-slug'),
                shopUrl = $('body').data('shop-url'),
                href = $(this).attr('href');

            if ( href !== undefined && href.indexOf(shopUrl) >= 0 && $(this).data('search') !== 'shopify' ) {
                e.preventDefault();
                console.log( href + '/?from=' + slug );
                window.open(href + '/?from=' + slug, '_self');
            }
        });
    }
}

