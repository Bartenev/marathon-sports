import GoogleMapsLoader from 'google-maps';
import {refresh} from './waypoints';
import {isMobile} from "../index";

var brandLink = $('.interactive-map__brand li'),
    activeBrand = $('.interactive-map__brand li.active').data('brand'),
    mapContainer = $('#map'),
    singleMapContainer = $('#single-map'),
    isRunners = $('.location-grid').data('post'),
    bounds,
    infoWindow,
    markerSettings = $('.set-markers li'),
    mapWithMarker,
    markers1 = [],
    gmarkers = [],
    markersSearch = [],
    globalColor = $('body').attr('data-color-brand'),
    userMarker,
    search = $('.interactive-map__search'),
    Masonry = require( 'masonry-layout' );

function singleMapInit() {
    var lng = singleMapContainer.data('marker-lng'),
        lat = singleMapContainer.data('marker-lat'),
        icon = singleMapContainer.data('image-url'),
        address = singleMapContainer.data('address'),
        clubTitle = $('.c-full-width__title').data('clubtitle') || 0,
        options = {
            draggable: true,
            scrollwheel: false,
            center: {lat: lat, lng: lng},
            zoom: 13,
            mapTypeControl: false,
            types: ['clothing_store']
        },
        location,
        href = 'https://www.google.com/maps/search/?api=1&query=Google&query_place_id=',
        storeName;

        GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];
        GoogleMapsLoader.KEY = 'AIzaSyDVCq7mKYJBT40JZeT2cX9kqx9ia-mn-V4';
        GoogleMapsLoader.load(function(google) {
            mapWithMarker = new google.maps.Map(singleMapContainer[0], options);
            var marker1 = new google.maps.Marker({
                position: { lat: lat, lng: lng},
                map: mapWithMarker,
                icon: new google.maps.MarkerImage(icon, null, null, null, new google.maps.Size(36,52)),
                animation: google.maps.Animation.DROP,
                optimized: false,
                url: href
        });

        location = new google.maps.LatLng(options.center.lat, options.center.lng);

        var service = new google.maps.places.PlacesService(mapWithMarker);

        var request = {
            location: location,
            radius: '500',
            types: ['store']
        };
        service.nearbySearch(request, getPlaceDetails);

        google.maps.event.addListener(mapWithMarker, 'idle', function(e) {
            if ( $( ".place-card" ).length === 0 ) {
                setTimeout(function () {
                    $(".gm-style").append('<div style="position: absolute; left: 0px; top: 0px;">' +
                        '<div style="margin: 30px 50px; padding: 1px; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; background-color: white;"> ' +
                        '<div> ' +
                        '<div class="place-card place-card-large"> ' +
                        '<div class="place-desc-large"> ' +
                        '<div class="place-name"> ' + storeName + ' </div>' +
                        '<div class="address"> ' + address + ' </div>' +
                        '</div>' +
                        '<div class="navigate"> ' +
                        '<div class="navigate"> ' +
                        '<a class="navigate-link" href="' + href + '" target="_blank"> ' +
                        '<div class="icon navigate-icon"></div><div class="navigate-text"> Directions </div>' +
                        '</a> ' +
                        '</div>' +
                        '</div>' +
                        '<div class="review-box"> ' +
                        '<div class="" style="display:none"></div>' +
                        '<div class="" style="display:none"></div>' +
                        '<div class="" style="display:none"></div>' +
                        '<div class="" style="display:none"></div>' +
                        '<div class="" style="display:none"></div>' +
                        '<div class="" style="display:none"></div>' +
                        // '<a href="https://plus.google.com/101643431012640484007/about?hl=en&amp;authuser=0&amp;gl=pt&amp;socpid=247&amp;socfid=maps_embed:placecard" target="_blank">1 review</a> ' +
                        '</div>' +
                        '<div class="saved-from-source-link" style="display:none"> </div>' +
                        '<div class="maps-links-box-exp"> ' +
                        '<div class="time-to-location-info-exp" style="display:none"> ' +
                        '<span class="drive-icon-exp experiment-icon"></span>' +
                        '<a class="time-to-location-text-exp" style="display:none" target="_blank"></a>' +
                        '<a class="time-to-location-text-exp" style="display:none" target="_blank"></a> </div>' +
                        '<div class="google-maps-link"> ' +
                        // '<a href="https://maps.google.com/maps?ll=40.837067,14.136834&amp;z=16&amp;t=m&amp;hl=en-US&amp;gl=PT&amp;mapclient=embed&amp;cid=2152474408176797502" target="_blank">View larger map</a> ' +
                        '<a href="'+ href +'" target="_blank">View larger map</a> ' +
                        '</div></div><div class="time-to-location-privacy-exp" style="display:none"> ' +
                        '<div class="only-visible-to-you-exp"> Visible only to you. </div>' +
                        '<a class="learn-more-exp" target="_blank">Learn more</a> ' +
                        '</div></div></div></div></div>');
                }, 1000);
            }
        });
    });
    function getPlaceDetails(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                var place = results[i];
                if (clubTitle) {
                    storeName = clubTitle;
                }
                else if(place.name === "Marathon Sports" || place.name === "Sound Runner" || place.name === "Runner's Alley") {
                    storeName = place.name;
                    href = href + place.place_id;
                }
            }
        }
    }

    $('.c-half-width__management').on({
        'mouseover' : function () {
            if ( window.innerWidth > 768  ) {
                let height = $(this).find('p').height();
                $(this).addClass('active');
                $(this).find('.c-name__description').height(height);
            }
        },
        'mouseleave' : function () {
            if ( window.innerWidth > 768  ) {
                $(this).removeClass('active');
                $(this).find('.c-name__description').height(0);
            }
        }
    });

    if ( window.innerWidth < 768 ) {
        $('.c-half-width__management').each(function () {
            let height = $(this).find('.c-name').height();
            $(this).css({'margin-bottom' : height});
            $($(this).find('.c-name')).css({'bottom' : -height});
        });
    }
}

var map = {
    init: function () {
        globalColor = $('body').attr('data-color-brand');

        if ( mapContainer.length ) {
            brandInit();
            mapInit();
            searchBy();

            masonryInit();
            if( isRunners !== 'run_clubs' && isRunners !== 'race_teams' ) {
                locationFilter.setActive();
            }
        }
        if (singleMapContainer.length) {
            singleMapInit();
        }
    }
};

function mapInit() {

    markerSettings.each(function() {
        var _this = $(this),
            currentIndex = _this.index();

        markers1[currentIndex] = [
            _this.data('marker-lat'),
            _this.data('marker-lng'),
            _this.data('marker-filter'),
            _this.data('image-url'),
            _this.data('href'),
            _this.data('sity'),
            _this.data('state'),
            _this.data('zip'),
            _this.data('clubtitle'),
            JSON.parse(_this.data('workhours')),
            _this.data('posturl'),
            _this.data('color')
        ];
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var userPosition = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            userMarker = new google.maps.Marker({
                position: userPosition,
                category: ['all', 'all', 'all', 'all', 'near-me'],
                map: mapWithMarker,
                animation: google.maps.Animation.DROP,
                optimized: false,
            });

            gmarkers.push(userMarker);

            $('.interactive-map__nav').addClass('js-show-nearMe');
        });
    }

    var options = {
        draggable: true,
        scrollwheel: false,
        center: {lat: 42.359701, lng: -71.048595},
        zoom: 11
    };

    GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];
    GoogleMapsLoader.KEY = 'AIzaSyDVCq7mKYJBT40JZeT2cX9kqx9ia-mn-V4';
    GoogleMapsLoader.REGION = 'US';
    GoogleMapsLoader.load(function(google) {
        mapWithMarker = new google.maps.Map(mapContainer[0], options);
        bounds = new google.maps.LatLngBounds();

        for (var i = 0; i < markers1.length; i++) {
            addMarker(markers1[i]);
        }

        // add map search

        var input = document.getElementById('search-map');
        var searchBox = new google.maps.places.SearchBox(input);

        var options = {
            types: ['(cities)'],
            componentRestrictions: {country: "us"}
        };
        var autoComplete = new google.maps.places.Autocomplete(input, options);

        // Bias the SearchBox results towards current map's viewport.
        mapWithMarker.addListener('bounds_changed', function() {
            searchBox.setBounds(mapWithMarker.getBounds());
        });

        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length === 0) {
                return;
            }

            markersSearch.forEach(function(marker) {
                marker.setMap(null);
            });
            markersSearch = [];

            // For each place, get the icon, name and location.
            var bounds = '';
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                // console.log( place.types );
                // console.log( place.types.indexOf('locality') );
                // console.log( place.types.indexOf('political') );

                markersSearch.push(new google.maps.Marker({
                    map: mapWithMarker,
                    title: place.name,
                    position: place.geometry.location
                }));

                var inputVal = input.value.split(',')[0].toLocaleLowerCase();

                for (var i = 0; i < gmarkers.length; i++) {
                    var marker = gmarkers[i],
                        markerFilter = marker.category,
                        markerDataCity = markerFilter[1],
                        markerDataZip = markerFilter[3];

                    if ( markerDataCity === inputVal.replace(/\s/g, '') || markerDataZip === inputVal ) {
                        if ( typeof bounds === 'string' ) {
                            bounds = new google.maps.LatLngBounds();
                        }
                        bounds.extend(marker.position);
                    }

                }
                if ( userMarker ) {
                    userMarker.setMap(null);
                }

                if ( typeof bounds === 'object' ) {
                    bounds.extend(place.geometry.location);
                    mapWithMarker.fitBounds(bounds);
                }else {
                    bounds = place.geometry.location;
                    mapWithMarker.setCenter(bounds);
                    mapWithMarker.setZoom(10);
                }

            });
        });
    });
}

function addMarker(marker) {
    var title = marker[8];
    var workHours = marker[9];
    var postUrl = marker[10];
    var color = marker[11];
    var target = isRunners === 'eventbrite_events' ? '_blank' : '_self';

    function hours() {
        var html = '';
        for (var i = 0; i < workHours.length; i++) {
            html += `<div class="c-info-window__workhours">${workHours[i]}</div>`;
        }
        return html;
    }

    var contentString ='';
    contentString = `<div class="c-info-wrapper c-info-wrapper_${color}">
                            <div class="c-info-window">
                            <h2 class="c-info-window__title">${title}</h2>
                            ${hours()}
                            <a class="c-info-window__link" href="${postUrl}" target="${target}">LEARN MORE</a>
                        </div>
                      </div>`;



    infoWindow = new google.maps.InfoWindow();

    var category = [marker[2], marker[5], marker[6], marker[7], 'near-me'];
    var pin = marker[3];
    var pos = new google.maps.LatLng(marker[0], marker[1]);
    var href = marker[4];
    var showMarker = true;
    var posturl = marker[10];

    var marker1 = new google.maps.Marker({
        position: pos,
        category: category,
        map: mapWithMarker,
        icon: new google.maps.MarkerImage(pin, null, null, null, new google.maps.Size(36,52)),
        animation: google.maps.Animation.DROP,
        optimized: false,
        url: href,
        posturl: posturl
    });

    gmarkers.push(marker1);

    marker1.category.forEach(function(elem) {
        if ( isRunners !== 'run_clubs' && isRunners !== 'race_teams' ) {
            if ( globalColor === elem ) showMarker = true; bounds.extend(marker1.position);
        }else {
            if ( 'all' === elem ) showMarker = true; bounds.extend(marker1.position);
        }
    });

    if ( !showMarker ) {
        marker1.setMap(null);
    }

    // Marker click listener
    google.maps.event.addListener(marker1, 'click', function (event, content, index) {
        console.log( isRunners );
        if ( isRunners === 'eventbrite_events' ) {
            window.open(this.posturl);
        }else {
            window.location.href = this.posturl;
        }
    });
    google.maps.event.addListener(marker1, 'mouseover', function (event, content, index) {
        infoWindow.setContent(contentString);
        infoWindow.open(mapWithMarker, marker1);
    });
    mapWithMarker.addListener('click', function() {
        infoWindow.close();
    });
    mapWithMarker.fitBounds(bounds);
}

function filterMap(filter) {
    var positionArray = [],
        boundsFilter = new google.maps.LatLngBounds();

    for (var i = 0; i < gmarkers.length; i++) {
        var marker = gmarkers[i],
            markerFilter = marker.category,
            showMarker = false;

        if  ( filter[0] === 'yellow' || filter[0] === 'blue' || filter[0] === 'red' ) {
            marker.setMap(null);
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }

        showLoop(markerFilter);

        if ( showMarker ) {
            marker.setMap(mapWithMarker);
            boundsFilter.extend(marker.position);
            positionArray.push(i);
        }
    }

    infoWindow.close();
    mapWithMarker.fitBounds(boundsFilter);

    function showLoop(markerFilter) {
        var show = 0;

        markerFilter.forEach(function(elem) {
            for (let i = 0; i < filter.length; i++) {
                if ( filter[i].replace(/ /g,'') === elem ) {show += 1;}
            }

            if ( show === filter.length ) {
                showMarker = true;
            }
        });
    }

    setTimeout(function() {
        // positionArray.forEach(function(elem, index) {
        //     gmarkers[elem].setAnimation(null);
        // });

        search.removeClass('error');
        if (  positionArray.length < 1 ) {
            // search.find('input').val('Search returned no results');
            // search.addClass('error');
            for (var i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
                // gmarkers[i].setAnimation(google.maps.Animation.BOUNCE);
            }
            mapWithMarker.fitBounds(bounds);
        }else {
            for (var i = 0; i < gmarkers.length; i++) {
                // gmarkers[i].setMap(mapWithMarker);
                // gmarkers[i].setAnimation(google.maps.Animation.BOUNCE);
                gmarkers[i].setAnimation(null);
            }
        }
    }, 200);
}

function brandInit() {
    
    // If only one brand is included in the map
    // set it to active by default
    if ( brandLink.length < 4 ) {
        $('.interactive-map__brand li[data-brand="' + globalColor + '"]').addClass('active');
        activeBrand = $('.interactive-map__brand li.active').data('brand');
    }

    brandLink.on({
        'click' : function(e) {
            if ( $(this).is('.link') ) {                
                e.preventDefault();

                // Ignore click if only one brand included in map
                if ( brandLink.length > 3 ) {
                    // If click active brand, reset map and grid
                    if ( $(this).hasClass('active') ) {
                        activeBrand = "all";
                        brandLink.removeClass('active');                    

                        filterMap(["near-me"]);
                        filterGrid('all');

                    // If click inactive brand, set brand to active
                    // and reset map and grid
                    } else {
                        activeBrand = $(this).data('brand');

                        brandLink.removeClass('active');
                        $(this).addClass('active');

                        filterMap([activeBrand]);
                        filterGrid(activeBrand);
                    }      
                }          
            } else {
                if ( !$(this).parent().is('.open') ) {
                    $(this).parent().addClass('open');
                }else {
                    $(this).parent().removeClass('open');
                }
            }
        }
    });
}

function searchBy() {
    var searchBox = $('.interactive-map__search'),
        serachBtn = searchBox.find('.trigger'),
        searchField = searchBox.find('input');

    serachBtn.on('click', function() {
        var value = searchField.val().toLowerCase().split(',');
        if ( brandLink.length > 3 ) {
            brandLink.removeClass('active');   
        }
        filterMap(['near-me']);  
        filterGrid('all');
    });

    search.find('input').keypress(function () {
       if ( search.hasClass('error') ) {
           search.removeClass('error');
       }
    });

    $(document).keypress(function (e) {
        if ( e.which === 13 && $(e.target).parent('.interactive-map__search').length ) {
            var value = searchField.val().toLowerCase().split(',');
            if ( brandLink.length > 3 ) {
                brandLink.removeClass('active');   
            }
            filterMap(['near-me']);  
            filterGrid('all');       
        }
    });

    $('.trigger__link').on('click', function() {
        if ( brandLink.length > 3 ) {
            brandLink.removeClass('active');
        }
        
        filterMap(['near-me']);
        nearMe();
    });
}

function nearMe() {
    if ( markersSearch.length ) {
        markersSearch[0].setMap(null);
    }
    mapWithMarker.setZoom(10);
    mapWithMarker.setCenter(userMarker.position);
    filterGrid("all");
}

//locations filter
var locationLayout = '.location-grid',
    masonryL;

var gridSize = function() {
    function gutter()  {
        if ( window.innerWidth > 1024 ) {
            return 36;
        }else if ( window.innerWidth > 320 ) {
            return 15;
        }else {
            return 0;
        }
    }
    function columnSize() {
        if ( window.innerWidth > 1024 ) {
            return (($(locationLayout).width()) - gutter()*3) / 4;
        }
        if ( window.innerWidth > 480 ) {
            return (($(locationLayout).width()) - gutter()) / 2;
        }
        if ( window.innerWidth >= 320 ) {
            // return $(locationLayout).width();
            return (($(locationLayout).width()) - gutter()) / 2;
        }
    }

    markerSettings.width(columnSize());

    return {
        gutter : gutter(),
        columnSize : columnSize()
    };
};

function masonryInit () {
    let itemHeight = 0;
    masonryL = new Masonry(locationLayout, {
        itemSelector: '.location-grid__item',
        columnWidth: gridSize().columnSize,
        gutter: gridSize().gutter,
        percentPosition: true,
    });

    $('.location-grid__item').each(function (index, el) {
        if ( itemHeight < $(el).innerHeight() ) {
            itemHeight = $(el).innerHeight();
        }
    });

    $('.location-grid__item').height(itemHeight);
}

function filterGrid (activeBrand) {
    if ( activeBrand == "all" ) {
        markerSettings.removeClass('hidden js-inview');
                
        locationFilter.init();
        refresh();
        locationFilter.firstGridLine();        
        console.log( "filter all" );
    } else {
        markerSettings.removeClass('hidden js-inview');
        markerSettings.each(function () {
            if($(this).data('marker-filter') !== activeBrand) {
                $(this).addClass('hidden');
            }
        });
        locationFilter.init();
        refresh();
        locationFilter.firstGridLine();
        console.log( "filter active" );
    }   
}

var locationFilter = {
    setActive: function () {
        if ( activeBrand === undefined ) {
            activeBrand = $('body').data('color-brand');
        }
        
        // markerSettings.addClass('hidden');
        $('li[data-marker-filter="' + activeBrand + '"]').removeClass('hidden');

        this.firstGridLine();
        this.init();
    },
    firstGridLine: function () {
        var activeItems = markerSettings.not('.hidden');
        getMaxHeight(activeItems);
        activeItems.each(function (index, elem) {
            if(index < 4) {
                $(elem).addClass('js-inview');
            }
        });
    },
    init: function () {
        if($(locationLayout).length) {
            masonryL.destroy();
            masonryInit();
        }
    }
};
function getMaxHeight (elem) {
    var maxHeight = -1;
    elem.each(function () {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    elem.each(function () {
        $(this).height(maxHeight);
    });
}

export {map, locationFilter};
