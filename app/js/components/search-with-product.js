import { scrollTop } from '../index';
let filterItemBrad = $('.sort-section.brand-sort input');
let filterItemOther = $('.sort-section.other-post [data-filter]'),
    accordion = $('.sort-section'),
    accordionTrigger = accordion.find('h2');

export default {
	pending: false,
	page: 2,
	searchQuery: '',
    // postType: '',
	container: $('[data-ajax-container]'),
    // metaQuery: [],
	init() {
		if(!this.container.length) return;

		this.event();
	},
	event() {
	    let _that = this;

        // accordionTrigger.each(function() {
        //     var parent = $(this).parent(),
        //         listWrap = parent.find('.sort-section__inner'),
        //         listHeight = parent.find('ul').innerHeight();
        //
        //     if ( listHeight > 310 ) {
        //
        //         listWrap.attr('data-height', listHeight);
        //
        //         if ( window.innerWidth > 768 ) {
        //             listWrap.height(310);
        //         }else {
        //             listWrap.height(0);
        //         }
        //
        //         parent.append('<span class="more">VIEW MORE</span>');
        //
        //     }else {
        //         listWrap.attr('data-height', listHeight);
        //         if ( window.innerWidth > 768 ) {
        //             listWrap.height(listHeight);
        //         }else {
        //             listWrap.height(0);
        //         }
        //     }
        // });
        //
        // // trigger accordion title
        // accordionTrigger.on('click', function () {
        //     var parent = $(this).parent(),
        //         listWrap = parent.find('.sort-section__inner'),
        //         mobileFilter = $('.collection-filter-mobile__sort__inner');
        //
        //     if ( parent.is('.active') ) {
        //         parent.removeClass('active');
        //
        //         listWrap.height(0);
        //
        //         if ( window.innerWidth < 768 ) {
        //             mobileFilter.height(mobileFilter.height() - listWrap.data('height'));
        //         }
        //     }else {
        //         parent.addClass('active');
        //
        //         if ( listWrap.data('height') < 310 ) {
        //             listWrap.height(listWrap.data('height'));
        //         }else {
        //             listWrap.height(310);
        //         }
        //
        //         if ( window.innerWidth < 768 ) {
        //             mobileFilter.height(mobileFilter.height() + listWrap.data('height'));
        //         }
        //     }
        // });
        //
        // accordion.find('.more').on('click', function () {
        //     var parent = $(this).parent(),
        //         height = parent.find('.sort-section__inner').data('height');
        //
        //     $(this).toggleClass('active');
        //     if ( $(this).is('.active') ) {
        //         parent.find('.sort-section__inner').height(height);
        //     }else {
        //         parent.find('.sort-section__inner').height(310);
        //     }
        // });
        //
        // //mobile event filter open
        // $('.collection-filter-mobile__title').on('click', function () {
        //     var height = 0;
        //
        //     accordion.each(function () {
        //         if ( $(this).is(':visible') ) {
        //             height += $(this).outerHeight(true);
        //         }
        //     });
        //
        //     if ( $(this).parent().is('.collection-filter-mobile__sort') ) {
        //
        //         if ( !$(this).parents('.collection-filter-mobile').is('.active') ) {
        //
        //             $(this).parents('.collection-filter-mobile').addClass('active');
        //             setTimeout(function () {
        //                 $('.collection-filter-mobile__sort__inner').height(height);
        //             }, 400);
        //         }else {
        //
        //             $(this).parents('.collection-filter-mobile').removeClass('active');
        //             $('.collection-filter-mobile__sort__inner').height(0);
        //         }
        //     }
        // });
        //
        // $('.sort-section .btn.clear').on('click', function () {
        //     filterItemBrad.each(function () {
        //         this.checked = false;
        //     });
        //     _that.postType = '';
        //     _that.page = 1;
        //     _that.metaQuery = {};
        //     _that.container.parent().addClass('js-load');
        //     _that.request('update');
        // });

        window.addEventListener('scroll', function() {
            _that.onScroll();
        });

        // filterItemBrad.on('change', function () {
        //     _that.metaQuery.push($(this).attr('id'));
        //     _that.page = 1;
        //     _that.container.parent().addClass('js-load');
        //     _that.request('update');
        // });
        //
        // filterItemOther.on('click', function () {
        //     filterItemBrad.each(function () {
        //          this.checked = false;
        //     });
        //
        //     _that.postType = $(this).data('filter');
        //     _that.page = 1;
        //     _that.metaQuery = {};
        //     _that.container.parent().addClass('js-load');
        //     _that.request('update');
        // });
	},
    onScroll() {
        if( scrollTop >= this.container.outerHeight() - window.innerHeight + 200) {
            if ( this.page <= this.container.data('max-page') ) {
                this.container.parent().addClass('js-load');
                this.request('scroll');
            }
        }
    },
	request(event) {
		if( this.pending ) { return; }
		this.pending = true;
		this.searchQuery = this.container.data('ajax-search');
		let config = {
		    'action' : 'more_post_ajax',
		    'searchQuery' : this.searchQuery,
            'pageNumber' : this.page,
            // 'postType' : this.postType,
            // 'metaQuery' : this.metaQuery
        };

        console.log('Fetching new posts...'+this.searchQuery);

        $.ajax( {
            url: ajaxUrl,
            type: "POST",
            dataType: "html",
            data: config,
            context: this,
            success(res, textStatus, req) {
                this.page++;
                if (event === 'scroll') {
                    this.container.append(res);
                }else {
                    this.container.html(res);
                }
                this.container.parent().removeClass('js-load');
                this.pending = false;
            },
            cache: false,
        });
	}
};
