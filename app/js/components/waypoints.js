require('waypoints');
import {isMobile} from '../index';

var waypoint;
var childElements = $('[data-delay]');

let item = $('[data-waypoint], [data-waypoint-fade]');


export function waypoints() {
    if(!item.length) return;

    item.each(function() {
        actionHandler(this);
    });
    if(childElements.length && !isMobile) {
        childElements.addClass('out');
    }
}

function actionHandler(el) {
    waypoint = new Waypoint({
        element: el,
        handler: function(direction) {
            var $el = $(this.element);
            if(direction == 'down' || direction == 'up' ) {
                $el.addClass('js-inview');
                if($el.has(childElements)) {
                    $el.find(childElements).each(function () {
                        var _this = $(this);
                        var bg = _this.hasClass('c-numbered__image');
                        var delay = parseInt($(this).data('delay'));
                        setTimeout(function() {
                            bg === true ? _this.addClass('in_indent') : _this.addClass('in');
                        }, delay);
                    });
                }
            }
        },
        offset: '80%'
    });
}

export function refresh() {
    Waypoint.destroyAll();
    waypoints();
}