import createHistory from 'history/createBrowserHistory';
let history = createHistory(),
    location = history.location;

let container = $('.post-list'),
    pagination = $('.post-list-nav_arrow'),
    accordionTrigger = $('.sidebar__title'),
    pageHash,
    postList = {
        init : function () {
            event();
            mobileAccordion();

            pageHash = window.location.hash.split('/');

             if ( pageHash[0] === '#page' ) {
                 let data = {
                         action: 'post_list_pagination',
                         page:  pageHash[1],
                         type: container.parents('.list-with-sidebar').data('type-post'),
                         cat: container.parents('.list-with-sidebar').data('category')
                     };

                 if ( pageHash[2] !== undefined ) {
                     data.search =  pageHash[2].toLowerCase();
                 }

                 $.post( ajaxUrl, data, function(response) {
                     if ( response !== 0 ) {
                         container.attr('data-ajax-page', data.page);
                         replaceContent(response);

                         if ( data.search !== undefined ) {
                             $('.page-header').css({'background': 'black'});
                             $('.page-header h1').text(data.name + ' RESULT FOR "' + data.search + '"');
                             container.parents('.list-with-sidebar').find('#searchform-sidebar input').val(pageHash[2].toLowerCase());
                             // window.location.hash = 'page/' + data.page + '/' + pageHash[2].toLowerCase();
                             history.push('#page/' + data.page + '/' + pageHash[2].toLowerCase(), {
                                 pathname: data.page + '/' + pageHash[2].toLowerCase(),
                                 state:{ some: data.page + '/' + pageHash[2].toLowerCase() }
                             });
                         }else {
                             // window.location.hash = 'page/' + data.page;
                             history.push('#page/' + data.page, {
                                 pathname: data.page,
                                 state:{ some: data.page }
                             });
                         }
                     }
                 });
             }

        }
    };


function event() {
    $(document).on('click', pagination, function(e) {
        if ( !$(e.target).hasClass('post-list-nav_arrow') ) return;

        let data = {
                action: 'post_list_pagination',
                page: container.data('ajax-page'),
                type: container.parents('.list-with-sidebar').data('type-post'),
                search: container.parents('.list-with-sidebar').find('#searchform-sidebar input').val(),
                cat: container.parents('.list-with-sidebar').data('category')
            },
            _this = $(e.target);

        if ( _this.data('page') === 'prev' ) {
            if ( container.attr('data-ajax-page') > 1 ) {
                data.page = +container.attr('data-ajax-page') - 1;
            }else {
                return;
            }
        }

        if ( _this.data('page') === 'next' ) {
            if ( container.attr('data-ajax-page') < $('.post-list-nav__number').length ) {
                data.page = +container.attr('data-ajax-page') + 1;
            }else {
                return;
            }
        }

        $.post( ajaxUrl, data, function(response) {
            if ( response !== 0 ) {
                container.attr('data-ajax-page', data.page);
                replaceContent(response);
                $('body, html').animate({scrollTop: container.offset().top - 80}, 300);

                if ( data.search !== '' ) {
                    // window.location.hash = 'page/' + data.page + '/' + data.search;
                    history.push('#page/' + data.page + '/' + data.search, {
                        pathname: data.page + '/' + data.search,
                        state:{ some: data.page + '/' + data.search }
                    });
                }else {
                    // window.location.hash = 'page/' + data.page;
                    history.push('#page/' + data.page, {
                        pathname: data.page,
                        state:{ some: data.page }
                    });
                }
            }
        });
    });

    $('#searchform-sidebar').on('submit', function (e) {
        e.preventDefault();
        let data = {
            action: 'post_list_pagination',
            page: 1,
            type: container.parents('.list-with-sidebar').data('type-post'),
            search: $(this).find('input').val(),
            name: $(this).data('name-post'),
            cat: container.parents('.list-with-sidebar').data('category')
        };

        console.log( data );

        $.post( ajaxUrl, data, function(response) {
            if ( response !== 0 ) {
                container.attr('data-ajax-page', data.page);
                replaceContent(response);

                $('.page-header').css({'background': 'black'});
                $('.page-header h1').text(data.name + ' RESULT FOR "' + data.search + '"');

                // window.location.hash = 'page/' + data.page + '/' + data.search.toLowerCase();
                history.push('#page/' + data.page + '/' + data.search.toLowerCase(), {
                    pathname: data.page + '/' + data.search.toLowerCase(),
                    state:{ some: data.page + '/' + data.search.toLowerCase() }
                });
            }
        });
    });

    //change breadcrumb
    if ( $('.list-with-sidebar_single').length ) {
        var breadcrumbItem = $('.breadcrumb-link__inner > span');

        breadcrumbItem.each(function () {
            if ( $(this).index() === breadcrumbItem.length - 3 ) {
              $(this).find('a').addClass('current-item');
            }

            if ( $(this).index() === breadcrumbItem.length - 2 || $(this).index() === breadcrumbItem.length - 1 ) {
                $(this).remove();
            }
        });
    }

    history.listen((location, action) => {

        if ( action === 'POP' ) {
            pageHash = window.location.hash.split('/');

            let data = {
                action: 'post_list_pagination',
                page:  pageHash[1],
                type: container.parents('.list-with-sidebar').data('type-post'),
                cat: container.parents('.list-with-sidebar').data('category')
            };

            if ( pageHash[2] !== undefined ) {
                data.search =  pageHash[2].toLowerCase();
            }

            $('body, html').animate({scrollTop: container.offset().top - 80}, 300);

            $.post( ajaxUrl, data, function(response) {
                if ( response !== 0 ) {

                    container.attr('data-ajax-page', data.page);
                    replaceContent(response);

                    if ( data.search !== undefined ) {
                        $('.page-header').css({'background': 'black'});
                        $('.page-header h1').text(data.name + ' RESULT FOR "' + data.search + '"');
                        container.parents('.list-with-sidebar').find('#searchform-sidebar input').val(pageHash[2].toLowerCase());
                    }
                }
            });
        }
    });
}
function mobileAccordion() {
    accordionTrigger.on('click', function () {
        var height = 0;
        if ( window.innerWidth <= 768 ) {
            $(this).parents('.sidebar__item').siblings().removeClass('active');
            $(this).parents('.sidebar__item').siblings().find('ul form').height(0);
            if ( !$(this).parent().is('.active') ) {
                if ( $(this).parent().find('ul').length ) {
                    $(this).next().find('li').each(function () {
                        height += $(this).outerHeight(true);
                    });
                    $(this).next().height(height);
                }

                if ( $(this).parent().find('form').length ) {
                    $(this).parent().find('form > div p').each(function () {
                        height += $(this).outerHeight(true);
                    });
                    $(this).parent().find('form').height(height);
                }
            }

            $(this).parent().toggleClass('active');
            $(this).parent().find('ul').height(height);
            $(this).parent().find('form').height(height);
        }
    });
}

function replaceContent(response) {
    container.addClass('ajax');
    setTimeout(function () {
        container.html(response);
        container.removeClass('ajax');
    }, 350);
}

export default postList;

