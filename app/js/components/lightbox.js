var Lightbox =  function (container) {
	if(!container) return;

	this.wrapper = container;
	this.inner = container.find('.lightbox__content');
	this.isOpen = false;
	if(container.attr('id') != undefined) {
		this.id = container.attr('id');
	}
	this.events();
};
Lightbox.prototype.open = function() {
	this.wrapper.addClass('open');
	this.isOpen = true;
};
Lightbox.prototype.append = function(content) {
	this.inner.append(content);
};
Lightbox.prototype.close = function() {
	this.wrapper.removeClass('open');
	if(!this.wrapper.is('.lightbox_slider')) {
		if ( !this.wrapper.is('.lightbox-product-video') ) {
			this.inner.html('');
			this.wrapper.removeClass('size-view');
		}else {
			this.inner.find('.video-player').remove();
			$('.lightbox__poster ').removeClass('hide');
		}
	}

	this.isOpen = false;
};
Lightbox.prototype.events = function() {
	var that = this;
	this.wrapper.on({
		click: function (e) {
			var target = $(e.target);
			if( that.isOpen && !target.closest(that.inner).length && !target.closest('.btn').length ) {
				that.close();
			}

		}
	});
	this.wrapper.find('.lightbox__closer').on('click', function () {
		if ( that.isOpen ) {
			that.close();
		}
	});
	$('body').on({
		keyup: function(e) {
			if (e.keyCode == 27 && that.isOpen) {
				that.close();
			}
		}
	});
};


export default Lightbox;