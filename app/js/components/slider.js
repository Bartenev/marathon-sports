import "babel-polyfill";
import TweenMax from 'gsap/src/uncompressed/TweenMax';
import VideoPlayer from './video-player';
import Lightbox from './lightbox';
import vr from './vr';
import productPage from '../components-shopify/product';

require('swiper');
export var sliders = [];
export var sliderReady = false;
var eventSliderPosition = 0;

var slider = {
	init: function () {
		var eventSlider;

		$('.swiper-container').each(function(index, el) {
			el.setAttribute('id', 'swiper-'+index);

			var defaultConfig =  {
				grabCursor: true,
				slidesPerView: 1,
				initialSlide: 0,
				loop: true,
				speed: 1000,
				autoplayDisableOnInteraction: true,
				nextButton: '.swiper-arrow_next',
				prevButton: '.swiper-arrow_prev'
			};
			var actualConfig = {},
				sliderLength = $(el).find('.swiper-slide').length;

			if( el.dataset.fullWidth !== undefined ) {

				var slide = $(el).find('.swiper-slide');

				if ($(el).find('.swiper-slide').length > 1) {
					actualConfig = Object.assign(defaultConfig, {
						effect: 'fade',
						fade: {
							crossFade: true
						},
						loop: true,
						autoplay: el.dataset.autoplay,
						autoplayDisableOnInteraction: false,
						paginationHide: true,
						bulletActiveClass: 'active',
						paginationClickable: true,
						autoHeight: true,
						pagination: '.swiper-controls',
						paginationBulletRender: function (swiper, index, className) {
							return '<span class="brand-to ' + className + '"></span>';
						},
						onSlideChangeStart: function(swiper) {

							var prev = slide.not('.swiper-slide-active').find('.hero-slide__title');
							var next = slide.eq(swiper.activeIndex).find('.hero-slide__title');

							TweenMax.to(prev, 0.3, { x: '-100%', ease: Expo.easeInOut });
							TweenMax.fromTo(next, 1, { x: '100%' }, { x: '0%', ease: Expo.easeInOut, delay: 0.5 });

						},
						onSlideChangeEnd: function (swiper) {

							if( slide.eq(swiper.activeIndex).data('video') !== undefined ) {
								VideoPlayer.load( slide.eq(swiper.activeIndex) );
							}

						},
						onInit: function (swiper) {
							slide = $(el).find('.swiper-slide');

							if( slide.eq(swiper.activeIndex).data('video') !== undefined ) {
								VideoPlayer.load( slide.eq(swiper.activeIndex) );
							}
						}
					});

					sliders.push( new Swiper( document.getElementById('swiper-' + index), actualConfig) );
				}
			} else if ( el.dataset.product !== undefined ) {

				$(el).parent().attr('id', 'variant-' + index);

				if ($(el).find('.swiper-slide').length > 3) {
					actualConfig = Object.assign(defaultConfig, {
						effect: 'slide',
						setWrapperSize: true,
						slidesPerView: 3,
						initialSlide: 0,
						spaceBetween: 10,
						paginationClickable: false,
						nextButton: '#variant-' + index + ' .swiper-arrow_next',
						prevButton: '#variant-' + index + ' .swiper-arrow_prev',
					});

					sliders.push( new Swiper( document.getElementById('swiper-' + index), actualConfig) );
				}else {
					$(el).find('.swiper-slide').addClass('swiper-slide_default');
				}
			} else if ( el.dataset.breadcrumb !== undefined ) {

				actualConfig = Object.assign(defaultConfig, {
					effect: 'slide',
					setWrapperSize: true,
					autoplay: 6000,
					slidesPerView: 1,
					initialSlide: 0,
					paginationClickable: false,
					nextButton: '.breadcrumb-link .swiper-arrow_next',
					prevButton: '.breadcrumb-link .swiper-arrow_prev',
				});

				sliders.push( new Swiper( document.getElementById('swiper-' + index), actualConfig) );
			} else if ( el.dataset.productSingle !== undefined ) {

				if ( sliderLength > 2 ) {
					actualConfig = Object.assign(defaultConfig, {
						effect: 'slide',
						setWrapperSize: true,
						slidesPerView: 1,
						initialSlide: 0,
						paginationHide: true,
						pagination: '.swiper-controls',
						paginationBulletRender: function (swiper, index, className) {
							return '<span class="' + className + '"></span>';
						},
						paginationClickable: true,
						nextButton: '.swiper-arrow_product_next',
						prevButton: '.swiper-arrow_product_prev',
					});

					sliders.push( new Swiper( document.getElementById('swiper-' + index), actualConfig) );
                    productPage.init();
				}
			} else if ( el.dataset.gallery !== undefined) {
					actualConfig = Object.assign(defaultConfig, {
						slidesPerView: 4,
						setWrapperSize: true,
						loop: false,
						slideToClickedSlide: true,
						paginationClickable: true,
						nextButton: $(el).parent().find('.swiper-arrow_next'),
						prevButton: $(el).parent().find('.swiper-arrow_prev'),
						spaceBetween: 30,
						breakpoints: {
							1024: {
								slidesPerView: 3,
								spaceBetween: 25
							},
							768: {
								slidesPerView: 2,
								spaceBetween: 25
							},
							480: {
								slidesPerView: 1,
								spaceBetween: 25
							}
						},
						onInit: function (swiper) {
							if(swiper.isBeginning) {
								swiper.prevButton.once('click', function() {
									swiper.slideTo(swiper.slides.length);
								});
								return;
							}
						},
						onSlideChangeEnd: function (swiper) {
							if(swiper.isBeginning) {
								swiper.prevButton.once('click', function() {
									swiper.slideTo(swiper.slides.length);
								});
								return;
							}
							if(swiper.isEnd) {
								swiper.nextButton.once('click', function() {
									swiper.slideTo(0);
								});
								return;
							}
						},
					});
					var slider = new Swiper( document.getElementById('swiper-' + index), actualConfig);
					slider.bind = el.dataset.sliderBind || false;
					sliders.push( slider );

					var lbox = new Lightbox($('#'+el.dataset.lightboxBind));
                    $(el).find('.swiper-slide').on('click',  function(event) {
                         lbox.open();
                    });

			} else if ( el.dataset.lightboxGallery !== undefined) {
					actualConfig = Object.assign(defaultConfig, {
						slidesPerView: 1,
						effect: 'fade',
						fade: {
							crossFade: true
						},
						paginationClickable: true,
						loop: false,
						nextButton: $(el).parent().find('.swiper-arrow_next'),
						prevButton: $(el).parent().find('.swiper-arrow_prev'),
						onInit: function (swiper) {
							if(swiper.isBeginning) {
								swiper.prevButton.once('click', function() {
									swiper.slideTo(swiper.slides.length);
								});
								return;
							}
						},
						onSlideChangeEnd: function (swiper) {
							if(swiper.isBeginning) {
								swiper.prevButton.once('click', function() {
									swiper.slideTo(swiper.slides.length);
								});
								return;
							}
							if(swiper.isEnd) {
								swiper.nextButton.once('click', function() {
									swiper.slideTo(0);
								});
								return;
							}
						},
						// onSliderMove: function (swiper, event) {
						// 	if(  $(event.target).closest('.vr-pano').length ) {
						// 		swiper.lockSwipes();
						// 		return;
						// 	}
						// },
						// onTouchEnd:function (swiper, event) {
						// 	swiper.unlockSwipes();
						// }
					});
					var slider = new Swiper( document.getElementById('swiper-' + index), actualConfig);
					slider.bind = el.dataset.sliderBind || false;
					sliders.push( slider );
					vr();

			} else {
				eventSlider = index;
				var spaceSize = 35;

				if ( window.innerWidth <= 1340 ) {
					spaceSize = 15;
				}

				if ( window.innerWidth > 1024 && sliderLength > 4 ) {
					actualConfig = Object.assign(defaultConfig, {
						effect: 'slide',
						setWrapperSize: true,
						slidesPerView: 4,
						initialSlide: 0,
						paginationClickable: false,
						slidesPerGroup: 4,
						// spaceBetween: spaceSize,
						autoResize: false,
						nextButton: '.upcoming-events .swiper-arrow_next',
						prevButton: '.upcoming-events .swiper-arrow_prev',
						onSlideChangeEnd: function (swiper) {
						   for (var i = 0; i < swiper.activeIndex; i++ ) {
							   // $(el).find('.swiper-wrapper').append($(el).find('.swiper-slide').eq(i).clone().addClass('clone'));
							   if ( sliders[eventSliderPosition] !== undefined ) {
								   sliders[eventSliderPosition].updateSlidesSize();
							   }
						   }
						},
					});

					sliders.push( new Swiper( document.getElementById('swiper-' + index), actualConfig) );
					eventSliderPosition = sliders.length - 1;
					sliders[eventSliderPosition].updateSlidesSize();
				}

				$(window).on('resize',function() {
					if ( window.innerWidth <= 1024 && eventSliderPosition !== 0 ) {
						sliders[eventSliderPosition].destroy(true, true);
						eventSliderPosition = 0;
						$('.upcoming-events').find('.swiper-slide.clone').remove();
					} else if ( window.innerWidth > 1024 && eventSliderPosition === 0 ) {
						sliders.push( new Swiper( document.getElementById('swiper-' + index), actualConfig) );
						eventSliderPosition = sliders.length - 1;
					}
				});

			}
			for (var j = 0; j < sliders.length; j++) {
				var sliderA = sliders[j];

				if(sliderA.bind) {
					for (var k = 0; k < sliders.length; k++) {
						var sliderB = sliders[k];
						if(sliderA != sliderB && sliderA.bind == sliderB.bind) {
							sliderA.params.control = sliderB;
						}
					}
				}
			}
		});

        sliderReady = true;
	}
};

export default slider;
