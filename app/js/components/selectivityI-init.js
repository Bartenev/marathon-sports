require("selectivity/apis/jquery");
require("selectivity/dropdown");
require("selectivity/event-listener");
require("selectivity/inputs/email");
require("selectivity/inputs/multiple");
require("selectivity/inputs/single");
require("selectivity/locale");
require("selectivity/plugins/ajax");
require("selectivity/plugins/async");
require("selectivity/plugins/jquery/ajax");
require("selectivity/plugins/jquery/traditional");
require("selectivity/plugins/keyboard");
require("selectivity/plugins/options-validator");
require("selectivity/plugins/submenu");
require("selectivity/plugins/tokenizer");
require("selectivity/selectivity");
require("selectivity/templates");

var selectivityInit = {
    init: function () {
       init();
    }
};

function init() {
    if ( $('select').length && !$('.product-template__container').length && !$('.address-wrap').length ) {
        $('select').selectivity({
            showSearchInputInDropdown: false
        });

        // collection heading filter
        $('.collection-hero__input').on('selectivity-selected', function (id, item) {
            $('select.collection-hero__input').val(id.id).trigger('change');
            window.location.search = `?sort_by=${id.id}`;
        });
        // mobile
        if ( window.innerWidth < 768 ) {
            $('.collection-hero__input').on('change', function (e) {
                e.preventDefault();
                console.log( $(this).val() );
                window.location.search = `?sort_by=${$(this).val()}`;
            });
        }
    }
}

export default selectivityInit;