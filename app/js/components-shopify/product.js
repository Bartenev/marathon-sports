import YouTubePlayer from 'youtube-player';
import Player from '@vimeo/player';

import Lightbox from '../components/lightbox';

var productWrap = $('.product-template__container'),
    productThumbnails = $('.product-single__small-item');
var productPage = {
    init: function () {
        init();
    }
};

function init() {
    if ( productWrap.length ) {
        reviewsInit();
        productAddCart();
        event();

        // count product options
        $('.product-form__options').each(function () {
            var optionLength = $(this).find('.selector-wrapper').length;
            if ( optionLength === 2 ) {
                $(this).addClass('two-item');
            }
            if ( optionLength === 1 ) {
                $(this).addClass('one-item');
            }
        });

        // product color variant (remove equals item)
        var selectColor = $('.product-form__color i').text(),
            item = $('.product-form__color a'),
            variantColor = [];

        item.each(function (index) {
            var _this = $(this);

            if ( variantColor.length === 0 ) {
                variantColor.push(_this.data('color'));
            }else {
                for (let i = 0; i < variantColor.length; i++) {
                    if ( variantColor[i] === _this.data('color') ) {
                        $(this).addClass('hide');
                    }
                }
            }

            if ( index !== 0 && !$(this).is('.hide') ) {
                variantColor.push(_this.data('color'));
            }

            // add class active
            if ( $(this).data('color') === selectColor ) $(this).addClass('active');
        });

        if ( $('.product-form__color a:visible').length === 1 ) {
            item.hide();
        }
    }

    if ( $('.product-single').length ) {
        setQuantity('/admin/products/' + $('.product-single').data('id') + '.json');
    }
}

function setQuantity(url) {
    let maxQuantity = '',
        quantity = $('#Quantity');

    if ( window.location.search.split('=')[1] !== undefined ) {
        url = '/admin/variants/' + window.location.search.split('=')[1] + '.json';
    }

    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(response){
            if (response.product === undefined) {
                maxQuantity = response.variant.inventory_quantity;
            }else {
                maxQuantity = response.product.variants[0].inventory_quantity;
                let variants = response.product.variants,
                    variantsLength = response.product.variants.length;

                for (let i = 0; i < variantsLength; i++) {
                    if (variants[i].inventory_quantity > 0) {
                        maxQuantity = variants[i].inventory_quantity;
                        break;
                    }
                }
            }

            quantity.find('option').each(function (index, el) {
                $(el).show();
                if ( maxQuantity < 10 ) {
                    if (index > maxQuantity - 1) {
                        $(el).hide();
                    }
                }
            });
        },
        error: function(data){
            console.log( data );
        }
    });
}

function event() {
    productThumbnails.not('.product-single__small-item_video').on('click', function () {
        var index = $(this).index(),
            controlSlider = $('body .swiper-controls');

        controlSlider.find('span').eq(index).trigger('click');
    });

    productVideo();

    $('.size-chart').on('click', function () {
        $('.lightbox__poster').css({
            'background-image' : 'url(' + $(this).data('image') +')'
        });
        $('.lightbox').addClass('size-view');
        var lbox = new Lightbox($('.lightbox'));
        lbox.open();
    });

    $('.product-form__options select').on('change', function () {
        $('.product-form__error').text('');
        if ( window.location.search.split('=')[1] !== undefined  ) {
            setQuantity('/admin/variants/' + window.location.search.split('=')[1] + '.json');
        }
    });
}

// product video in lightbox
export function productVideo() {
    var videoTrigger = $('.product-single__small-item_video, .full-width-video__control, body .swiper-slide_video'),
        player;
    if (!videoTrigger.length ) {
        return false;
    }

    videoTrigger.on('click', function (e) {
        e.preventDefault();

        var lbox = new Lightbox($('.lightbox'));
        lbox.open();

        var type = videoTrigger.data('type'),
            videoId = videoTrigger.data('video'),
            element;

        $('.lightbox__content').find('.video-player').remove();
        $('.lightbox__content').append('<div class="video-player"></div>');
        element = $('body .video-player')[0];

        if( type === 'Vimeo' ) {
            player = new Player(element, {
                id: videoId
            });
            player.ready().then(function () {
                $('.lightbox__poster').addClass('hidden');
                player.play();
            });
        }

        if( type === 'YouTube' ) {
            player = YouTubePlayer(element, {
                videoId: videoId,
            });
            player.on('ready', function(state) {
                $('.lightbox__poster').addClass('hidden');
                player.playVideo();
            });
        }
    });
}
productVideo();
function reviewsInit() {
    var interval = setInterval(function () {
        var countReviews = $('body .product-single__meta .spr-badge-caption');
        if ( countReviews.length ) {
            var count = countReviews.text().split('/ ')[1];
            countReviews.html(`<a data-anchor="review">Read all ${+count} reviews</a>`);
            clearInterval(interval);
        }
    }, 100);

    $('[data-anchor="review"]').on('click', function() {
        $('body, html').animate({scrollTop: $('.product-single-reviews').offset().top}, 400);
    });
}

function productAddCart() {
    var btn = $('.product-form__cart-submit');

    btn.on('click', function (e) {
        e.preventDefault();
        console.log( 'submit event' );
        $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            dataType: 'json',
            data: $('.product-form').serialize(),
            success: function(response){
                $.ajax({
                    type: 'GET',
                    url: '/cart.js',
                    dataType: 'json',
                    success: function(cartdata) {
                        var productPrise = $('.product-price__price span').text(),
                            productOptions = $('.single-option-selector'),
                            total = cartdata.total_price.toString();

                        // update info in lightbox product
                        $('.product-lightbox__price span').text(productPrise);
                        productOptions.each(function () {
                            var _this = $(this),
                                data = _this.data('option');
                            $('.product-lightbox__info p').each(function () {
                                if ( data === $(this).data('option') ) {
                                    $(this).find('i').text(_this.val());
                                }
                            });
                        });

                        $('a.btn-cart span').html(cartdata.item_count);
                        $('.product-lightbox__add .count i').html(cartdata.item_count);
                        $('.product-lightbox__add .total').html(`$${total.slice(0, total.length - 2)}.00`);
                    }
                });
                $('body').addClass('product-lightbox-open');
            },
            error: function(data){
                let description = data.responseJSON.description;

                $('.product-form__error').text(description);
            }
        });
    });

    $('.product-lightbox__title a').on('click', function () {
        $('body').removeClass('product-lightbox-open');
    });

    $('.product-lightbox').on('click', function (e) {
        if ( $(this).has(e.target).length === 0 ) {
            $('body').removeClass('product-lightbox-open');
        }
    });
    $('.continue_shopping').on('click', function () {
        $('body').removeClass('product-lightbox-open');
    });
}

export default productPage;

