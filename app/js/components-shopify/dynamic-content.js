import navigation from '../components/navigation';
import {filterInit} from '../components-shopify/collection-filter';
import {sliders, sliderReady} from '../components/slider';

let wpApiMenus = '';
let wpApiAcfOptions = '';
let body = $('body');
let cookeSite;

export let dynamicContent = {
    init: function () {
        // check active site in shopify
        cookeSite = window.Cookies.get('site') === undefined ? 'yellow' : window.Cookies.get('site');
        let activeSite = $('.site-logo[data-color="' + cookeSite + '"]').data('active-site');
        wpApiMenus = activeSite + '/wp-json/wp-api-menus/v2/menus';
        wpApiAcfOptions = activeSite + '/wp-json/acf/v3/options/option';

        if (body.is('.shopify')) {
            if (localStorage.getItem('wpCacheContent-' + cookeSite) === null) {
                this.getWordpressMenu();
                this.getWordPressAcfOptions();
                localStorage.clear();
            }else {
                let interval = setInterval(() => {
                    if ( sliderReady ) {
                        this.cacheContent();
                    }
                    clearInterval(interval);
                }, 100);
            }
        }
    },
    cacheContent() {
        let acfContent = localStorage.getItem('wpCacheAcf-' + cookeSite),
            headerContent = localStorage.getItem('wpCacheHeaderMenu-' + cookeSite),
            footerContent = localStorage.getItem('wpCacheFooterMenu-' + cookeSite);


        this.createAcfOptions(JSON.parse(acfContent));
        this.headerMenu(JSON.parse(headerContent));
        this.footerMenu(JSON.parse(footerContent));
    },
    getWordpressMenu: function () {
        let _that = this;

        $.ajax({
            url: wpApiMenus,
            dataType: "json",
            crossDomain: true,
            success: function (data) {

                for (let i in data) {
                    if (data[i].slug === 'company-links' || data[i].slug === 'helpful-links') {
                        _that.footerMenuAjax(data[i]);
                    }
                    if (data[i].slug === 'main') {
                        _that.headerMenuAjax(data[i]);
                    }
                }

                localStorage.setItem('wpCacheContent-' + cookeSite, JSON.stringify(data));
            },
            error: function (data) {
                console.log('error : ', data);
            }
        });
    },
    getWordPressAcfOptions: function () {
        let _that = this;

        $.ajax({
            url: wpApiAcfOptions,
            dataType: "json",
            crossDomain: true,
            success: function (data) {

                _that.createAcfOptions(data['acf']);

                localStorage.setItem('wpCacheAcf-' + cookeSite, JSON.stringify(data['acf']));
            },
            error: function (data) {
                console.log('error : ', data);
            }
        });
    },
    headerMenuAjax: function (menu) {
        let _that = this;

        $.ajax({
            url: wpApiMenus + '/' + menu.ID,
            dataType: "json",
            crossDomain: true,
            success: function (data) {
                _that.headerMenu(data);

                localStorage.setItem('wpCacheHeaderMenu-' + cookeSite, JSON.stringify(data));
            },
            error: function (data) {
                console.log('error : ', menu.slug, data);
            }
        });
    },
    headerMenu: function (data) {
        let menuItem = '', menuWrap = $('.menu-wp');

        for (let i in data.items) {
            let item = data.items[i];

            if (item.children !== undefined) {
                let menuItemDropdown = '',
                    menuItemChildren;
                for (let i in item.children) {
                    if (item.children[i].children !== undefined) {
                        menuItemChildren = '';
                        for (let j in item.children[i].children) {
                            let itemClass;
                            if (item.children[i].children[j].classes !== undefined) itemClass = item.children[i].children[j].classes;

                            menuItemChildren += `<li class="link ${itemClass}">
                                                    <a href="${item.children[i].children[j].url}">${item.children[i].children[j].title}</a>
                                                    <i class="link__hover"></i>
                                                </li>`;
                        }

                        menuItemDropdown += `<li class="link menu-item-has-children">
                                                <div>
                                                    <a href="${item.children[i].url}">${item.children[i].title}</a>
                                                    <i class="link__hover"></i>
                                                </div>
                                                <div class="menu__dropdown">
                                                    <ul class="sub-menu">
                                                        ${menuItemChildren}
                                                    </ul>
                                                </div>
                                            </li>`;
                    } else {
                        let itemClass;
                        if (item.children[i].classes !== undefined) itemClass = item.children[i].classes;

                        menuItemDropdown += `<li class="link ${itemClass}">
                                                <div>
                                                    <a href="${item.children[i].url}">${item.children[i].title}</a>
                                                    <i class="link__hover"></i>
                                                </div>
                                            </li>`;
                    }
                }

                menuItem += `<li class="link menu-item-has-children">
                                <div>
                                    <a href="${item.url}">${item.title}</a>
                                    <i class="link__hover"></i>
                                </div>
                                <div class="menu__dropdown">
                                    <ul class="sub-menu">
                                        ${menuItemDropdown}
                                    </ul>
                                </div>
                            </li>`;

            } else {
                menuItem += `<li class="link">
                                <div>
                                     <a href="${item.url}">${item.title}</a>
                                    <i class="link__hover"></i>
                                </div>
                            </li>`;
            }
        }

        menuWrap.append(menuItem);
        navigation.init();
        filterInit.initOptions();
    },
    footerMenuAjax: function (menu) {
        let _that = this;

        $.ajax({
            url: wpApiMenus + '/' + menu.ID,
            dataType: "json",
            crossDomain: true,
            success: function (data) {
                _that.footerMenu(data);

                localStorage.setItem('wpCacheFooterMenu-' + cookeSite, JSON.stringify(data));
            },
            error: function (data) {
                console.log('error : ', menu.slug, data);
            }
        });
    },
    footerMenu: function (data) {
        let menuItem = '', menuWrap,
            menuContainer = $('.footer-top__wrap-menu');

        for (let i in data.items) {
            menuItem += `<li class="footer-list__item color-to">
                            <a href="${data.items[i].url}" style="">${data.items[i].title}</a>
                        </li>`;
        }

        menuWrap = `<div class="footer-list">
                        <h3 class="footer-list__title">${data.name}</h3>
                        <ul class="dynamic-content" data-color="yellow">
                            ${menuItem}
                        </ul>
                    </div>`;

        menuContainer.append(menuWrap);
    },
    createAcfOptions: function (data) {
        // breadcrumb slideshow of notifications
        let breadcrumbWrap = $('[data-breadcrumb]');

        if (breadcrumbWrap.length) {
            let index = breadcrumbWrap.attr('id').split('-')[1],
                breadcrumbItem = data['notifications'],
                items = [];

            for (let i in breadcrumbItem) {
                let contentFull = breadcrumbItem[i].content,
                    content = contentFull.length > 31 ? contentFull.slice(0, 30) + '...' : contentFull,
                    buttonType = breadcrumbItem[i]['link_type'],
                    link = buttonType === '_self' ? breadcrumbItem[i]['page_link'] : breadcrumbItem[i]['url'];

                if (breadcrumbItem[i]['show__hide'] !== 'hide') {
                    items.push(`<li class="swiper-slide">
                                  <p>${content} <a href="${link}" target="${buttonType}"> Learn More</a></p>
                              </li>`);
                    breadcrumbWrap.find('.swiper-wrapper').append(
                        `<li class="swiper-slide">
                          <p>${content} <a href="${link}" target="${buttonType}"> Learn More</a></p>
                      </li>`);
                }
            }
            if (breadcrumbWrap.length) {
                sliders[index].appendSlide(items);
            }
        }

        // footer social media links
        let socialList = ['instagram', 'facebook', 'youtube', 'twitter'];

        for (let sm in socialList) {
            let service = socialList[sm],
                socialLink = data[service];

            if (socialLink.length) {
                $('.social_item__' + service).show(function () {
                    $(this).find('a').attr('href', socialLink);
                });
            }
        }

        // footer newsletter signup text
        $('.footer-list_newsletter .footer-list__title').text(data['newsletter_title']);
        $('.footer-list_newsletter .footer-list__item').text(data['newsletter_content']);

        // footer payments accepted icons
        let paymentItem = data['payment_type'],
            payHtml = '';
        for (let p in paymentItem) {
            let payItem = paymentItem[p];
            payHtml += '<li style="background-image: url(' + payItem.payment + ');" title="' + payItem.payment_type + '" aria-label="' + payItem.payment_type + '"></li>';
        }
        if (payHtml.length) {
            $('.footer-bottom__list').html(payHtml);
        }

        // footer copyright name
        let copyright = data['copyright'];
        if (!copyright.length) {
            copyright = 'Marathon Sports';
        }
        $('.footer-bottom__copyright').text(copyright);
    }
};
