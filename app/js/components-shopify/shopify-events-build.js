// this script working only in shopify

import slider from '../components/slider';

var brandColor = $('body').data('color-brand'),
    sortPosts = [],
    toDay = new Date(),
    wrapEvents = $('[data-event]').find('.swiper-wrapper'),
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

var upcomingEvents = {

    init: function () {
        if ( $('[data-event]').is('.shopify') ) {
            ajaxWp();
        }
    }
};

function ajaxWp() {
    $.ajax({
        url: 'http://dev-marathon-sports.pantheonsite.io/wp-json/wp/v2/events',
        dataType: "json",
        crossDomain: true,
        success: function( data ) {
            filterEvents(data);
        },
        error: function (data) {
            console.log( 'error : ', data );
        }
    });
}

function filterEvents(data) {
    for ( var key in data ) {
        if ( Date.parse(new Date(data[key].event_data)) >= Date.parse(toDay) ) {
            sortPosts.push(data[key]);
        }
    }

    sortPosts.sort(function(a, b) {
        return Date.parse(a.event_data) - Date.parse(b.event_data);
    });

    buildEvents();
}

function buildEvents() {
    for ( var key in sortPosts ) {
        var link = sortPosts[key].guid.rendered,
            title = sortPosts[key].title.rendered,
            content = sortPosts[key].content.rendered,
            date = new Date(sortPosts[key].event_data),
            dateMonth = months[date.getMonth()],
            dateDay = String(date.getDate()).length === 1 ? '0' + date.getDate() : date.getDate(),
            imageUrl = sortPosts[key]._links['wp:featuredmedia'][0].href,
            image;

        addEvent(link, dateMonth, dateDay, imageUrl, title, content);
    }
}

function addEvent(link, dateMonth, dateDay, imageUrl, title, content) {
    $.ajax({
        url: imageUrl,
        dataType: "json",
        crossDomain: true,
        success: function( data ) {
            var image = data.guid.rendered;

            wrapEvents.append(
                '<a href="' + link + '" target="_blank" class="upcoming-events__item brand-to brand-to--hover-white swiper-slide">' +
                '<div class="upcoming-events__date brand brand--color-white">' +
                '<p>' + dateMonth + '</p><span>' + dateDay + '</span>' +
                '</div>' +
                '<div class="upcoming-events__image"><div class="bg-cover" style="background-image: url(' + image + ');"></div></div>' +
                '<div class="upcoming-events__body">' +
                '<h2 class="upcoming-events__title">' + title + '<p></p></h2>' +
                '<div class="upcoming-events__description">' + content.slice(0, 88) + '...</div>' +
                '<span class="upcoming-events__link">learn more</span>' +
                '</div>' +
                '</a>'
            );

            slider.init();
        },
        error: function (data) {
            console.log( 'error : ', data );
        }
    });
}

export default upcomingEvents;