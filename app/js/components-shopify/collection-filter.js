import productRow from '../components/product';
import slider from '../components/slider';

var InfiniteScroll = require('infinite-scroll'),
	imagesLoaded = require('imagesloaded'),
	Masonry = require( 'masonry-layout' ),
	filterLayout = '.featured-products__collection',
	msnry,
	maxHeight = 320,
	accordion = $('.sort-section'),
	accordionBrand = $('.sort-section.brand-sort'),
	accordionSize = $('.sort-section.filter-group-size'),
	accordionColor = $('.sort-section.filer-group-color'),
	accordionTrigger = accordion.find('h2'),
	filterItem = $('body .featured-products__item'),
	option = {
		'brand': [],
		'size': [],
		'color': []
	},
	chooseFilter = {
		'brand': [],
		'size': [],
		'color': []
	};

export var filterInit = {
	init: function () {
		if ($('.collection-hero').length) {
			shortOptions();
			MasonryInit();
		}
	},
  initOptions: function () {
    var catList = $('.sort-section.link-cat .sort-section__inner ul');

    // add shop by category
    catList.html('');

    $('body #menu-main .menu-item-has-children li.link.menu-item-has-children').each(function () {
        if ( 'https://' + window.location.host === $(this).find('a').attr('href').substr(0, window.location.host.length + 8) ) {
            catList.append(`<li>${$(this).find('div:eq(0)').html()}</li>`);
        }
    });

    event();
  }
};

function event() {
	accordionTrigger.each(function() {
		var parent = $(this).parent(),
			listWrap = parent.find('.sort-section__inner'),
			listHeight = parent.find('ul').innerHeight(), initHeight = maxHeight;
		listWrap.data('height', listHeight).css('transition','height .4s ease 0s');

		if ( listHeight > initHeight) {
			if ( parent.find('.more').length === 0 ) {
        parent.append('<span class="more">VIEW MORE</span>');
      }
		}else {
			initHeight = listHeight;
		}

		if (parent.hasClass('has_group_selected') || parent.hasClass('all')) {
			listWrap.height(initHeight);
		} else {
			listWrap.height(0);
			parent.removeClass('active');
		}
	});

	// trigger accordion title
	accordionTrigger.on('click', function () {
		var parent = $(this).parent(),
		    listWrap = parent.find('.sort-section__inner'),
		    thisMenu = $('.collection-sidebar'),
		    otherMenu = $('.collection-filter-mobile');
		if ($(this).parents('.collection-filter-mobile').length) {
			otherMenu = $('.collection-sidebar');
			thisMenu = $('.collection-filter-mobile');
		}
		var otherOne = otherMenu.find('.sort-section').eq(thisMenu.find('.sort-section').index(parent));

		//updateListStyle();
		if ( parent.is('.active') ) {
			parent.removeClass('active');
			otherOne.removeClass('active');
			listWrap.height(0);
		}else {
			parent.addClass('active');
			otherOne.addClass('active');
			if ((listWrap.next('.more').length===1 && listWrap.next('.more').hasClass('active')) || listWrap.data('height') < 320 ) {
				listWrap.height(listWrap.data('height'));
			} else {
				listWrap.height(310);
			}
		}
	});

	$('.filter-menu').on('click', '.more', function () {
		var parent = $(this).parent(),
		    height = parent.find('.sort-section__inner').data('height'),
		    thisMenu = $('.collection-sidebar'),
		    otherMenu = $('.collection-filter-mobile');
		if ($(this).parents('.collection-filter-mobile').length) {
			otherMenu = $('.collection-sidebar');
			thisMenu = $('.collection-filter-mobile');
		}
		var otherOne = otherMenu.find('.sort-section').eq(thisMenu.find('.sort-section').index(parent)).find('.more');

		if ( !$(this).is('.active') ) {
			parent.find('.sort-section__inner').height(height);
			$(this).text('VIEW LESS').addClass('active');
			otherOne.text('VIEW LESS').addClass('active');
		}else {
			parent.find('.sort-section__inner').height(310);
			$(this).text('VIEW MORE').removeClass('active');
			otherOne.text('VIEW MORE').removeClass('active');
		}
	});

	//mobile event filter open
	$('.collection-filter-mobile__title').on('click', function () {
		if ( $(this).parent().is('.collection-filter-mobile__sort') ) {
			if ( !$(this).parents('.collection-filter-mobile').is('.active') ) {
				var height = $(window).height() - $('.collection-filter-mobile__title').height(),
						$this = $(this);
				$('.collection-header, #shopify-section-breadcrumb').slideUp(400);
				$this.parents('.collection-filter-mobile').addClass('widen');
				setTimeout(function () {
					$('.collection-filter-mobile__sort__inner').height(height);
					$this.parents('.collection-filter-mobile').addClass('active');
					$this.html('Close Filters<span></span>');
					$('html').addClass('locked');
				}, 400);
			}else {
				$('html').removeClass('locked');
				$(this).html('Filter By<span></span>');
				$('.collection-header, #shopify-section-breadcrumb').slideDown(400);
				$(this).parents('.collection-filter-mobile').removeClass('active widen');
				$('.collection-filter-mobile__sort__inner').height(0);
			}
		}
	});

	var resizeTimer;
	$(window).resize(function() {
		if ($('.collection-filter-mobile').hasClass('active')) {
			var height = $(window).height() - $('.collection-filter-mobile__title').height();
			$('.collection-filter-mobile__sort__inner').height(height);
		}
		clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
			$('.sort-section__inner').each(function() {
				$(this).height('auto').css('transition','none');
				var listHeight = $(this).find('ul').innerHeight(), parent = $(this).parent();
				$(this).data('height', listHeight);
				var initHeight = maxHeight;
				if ( listHeight > initHeight) {
					if ( parent.find('.more').length === 0 ) {
		        parent.append('<span class="more">VIEW MORE</span>');
		      }
				} else {
					initHeight = listHeight;
				}

				if (parent.hasClass('active')) {
					if (!$(this).next('.more').hasClass('active')) {
						$(this).height(initHeight);
					} else {
						// keep open (auto)
					}
				} else if (parent.hasClass('has_group_selected') || parent.hasClass('all')) {
					$(this).height(initHeight);
				} else {
					$(this).height(0);
				}
				$(this).css('transition','height .4s ease 0s');
			});
	  }, 50);
	});

	/*
	$('.apply').on('click', function () {
		masonryFilter(chooseFilter);
		$('.collection-filter-mobile').add(accordion).removeClass('active');
		$('.collection-filter-mobile__sort__inner').add($('.sort-section__inner')).height(0);
	});

	$('.clear').on('click', function () {
		accordion.find('li.active').trigger('click');
		$('.collection-filter-mobile').add(accordion).removeClass('active');
		$('.collection-filter-mobile__sort__inner').add($('.sort-section__inner')).height(0);
		masonryFilter(chooseFilter);
	});

	// choose filter (brand)
	accordionBrand.find('label').on('click', function () {
		var parent = $(this).parent(),
			dataFilter = parent.data('filter');

		if ( !parent.is('.active') ) {
			parent.addClass('active');
			chooseFilter.brand.push(dataFilter);
		}else {
			parent.removeClass('active');
			var i = chooseFilter.brand.indexOf(dataFilter);
			if( i !== -1 ) {
				chooseFilter.brand.splice(i, 1);
			}
		}

		chooseFilter.brand = unique(chooseFilter.brand);
		if ( window.innerWidth > 768 ) {
			$(filterLayout).addClass('hide');
			masonryFilter(chooseFilter);
		}
	}); */

	// choose filter (size or color)
	accordionSize.find('li').on('click', function (e) {
		document.location.href = $(this).find('a').attr('href');
		/*
		var dataFilter = $(this).data('filter'),
			obj;

		if ( $(this).parents('.sort-section').is('.filter-group-size') ) {
			obj = chooseFilter.size;
		}else {
			obj = chooseFilter.color;
		}

		$(this).toggleClass('active');

		if ( $(this).is('.active') ) {
			obj.push(dataFilter);
		}else {
			var i = obj.indexOf(dataFilter);
			if( i !== -1 ) {
				obj.splice(i, 1);
			}
		}

		if ( $(this).parents('.sort-section').is('.color') ) {
			if ( $(this).parent().find('.active').length !== 0 ) {
				$(this).parent().addClass('change');
			}else {
				$(this).parent().removeClass('change');
			}
		}

		obj = unique(obj);
		if ( window.innerWidth > 768 ) {
			$(filterLayout).addClass('hide');
			masonryFilter(chooseFilter);
		} */
	});
}

function masonryFilter(object) {
	filterItem = $('body .featured-products__item');

	filterItem.each(function () {
		var _this = $(this),
			brand = $(this).data('vendor'),
			visible = 0,
			isVisible = 0,
            checkObject = {
		        brand: false,
                color: false,
                size: false
            };


		if ( object.brand.length ) {
			visible++;

			for ( let i = 0; i < object.brand.length; i++ ) {
				if ( brand === object.brand[i] ) { isVisible++; }
			}
		}

		if ( object.color.length ) {
			visible++;

			for ( let i = 0; i < object.color.length; i++ ) {
				if ( _this.hasClass(object.color[i] ) ) {
                    if ( checkObject.color === false ) {
                        isVisible++;
                        checkObject.color = true;
                    }
                }
			}
		}

		if ( object.size.length ) {
			visible++;

			for ( let i = 0; i < object.size.length; i++ ) {
				if ( _this.hasClass(object.size[i] ) ) {
					if ( checkObject.size === false ) {
						isVisible++;
                        checkObject.size = true;
					}
				}
			}
		}

		if ( isVisible !== visible ) {
			_this.addClass('hidden');
		}else {
			_this.removeClass('hidden');
		}

		setTimeout(function () {
			msnry.layout();
		}, 250);

		setTimeout(function () {
			$(filterLayout).removeClass('hide');
		}, 1000);
	});
}

var gridSize = function() {
	var searchHeader = $('.collection-header_search');

	function gutter()  {
		if ( window.innerWidth > 1024 ) {
			return 35;
		}else if ( window.innerWidth > 768 ) {
			return 20;
		}else {
			return 0;
		}
	}
	function columnSize() {
		var containerW = $(filterLayout).width();
		if ( window.innerWidth > 1024 ) {
			return containerW = (containerW - 90) / 3;
		}
		if ( window.innerWidth > 768 ) {
			return containerW = (containerW - 90) / 2;
		}
		return containerW;
	}

	if ( searchHeader.length ) {
        $('.featured-products__item').width($('.featured-products ').width());
    }else {
        $('.featured-products__item').width(columnSize());
    }

	return {
		gutter : searchHeader.length ? 0 : gutter(),
		columnSize : searchHeader.length ? $('.featured-products ').width() : columnSize()
	};
};

function MasonryInit() {
	if ( $(filterLayout).length ) {

		var grid = document.querySelector(filterLayout),
			viewMoreButton = '.view-more-button-collection';
		msnry = new Masonry( grid, {
			itemSelector: 'none',
			columnWidth: gridSize().columnSize,
			gutter: gridSize().gutter,
			percentPosition: true,
			// stagger: 30,
			resize: false
		});

		InfiniteScroll.imagesLoaded = imagesLoaded;

		// initial items reveal
		imagesLoaded( grid, function() {
			grid.classList.remove('are-images-unloaded');
			msnry.options.itemSelector = '.featured-products__item';
			var items = grid.querySelectorAll('.featured-products__item');
			msnry.appended( items );
		});

		// init Infinte Scroll

		var infScroll = new InfiniteScroll( grid, {
			path: '.btn--next',
			append: '.featured-products__item',
			outlayer: msnry,
			button: viewMoreButton,
			status: '.page-load-status',
		});

		infScroll.on( 'append', function( ) {
			if ( infScroll.pageIndex % 3 === 0 &&
                chooseFilter.color.length === 0 &&
                chooseFilter.size.length === 0 &&
                chooseFilter.color.length === 0
			) {
				infScroll.options.loadOnScroll = false;
				infScroll.options.scrollThreshold = false;
				$(viewMoreButton).addClass('visible');
			}

			shortOptions();
			/*
			if ( chooseFilter.brand.length || chooseFilter.color.length || chooseFilter.size.length ) {
				masonryFilter(chooseFilter);
			}*/
			slider.init();
			productRow.init();
			masonryResize();
		});

		$(viewMoreButton).on('click', function () {
			$(this).removeClass('visible');
			infScroll.options.loadOnScroll = true;
			infScroll.options.scrollThreshold = true;
		});

	}
}

// resize Masonry
export function masonryResize() {
	if ( $(filterLayout).length ) {
		msnry.destroy();
		msnry = new Masonry( filterLayout, {
			itemSelector: '.featured-products__item',
			columnWidth: gridSize().columnSize,
			gutter: gridSize().gutter,
			percentPosition: true,
			// stagger: 30,
			resize: false
		});
	}
}

// set filter options
function shortOptions() {
	/*
	var catList = $('.sort-section.link-cat .sort-section__inner ul'),
		brandList = $('.sort-section.brand-sort .sort-section__inner:visible ul'),
		sizeList = $('.sort-section.filter-group-size .sort-section__inner ul'),
		colorList = $('.sort-section.filter-group-color .sort-section__inner ul'),
		filterItem = $('body .featured-products__item');

	filterItem.each(function () {
		var brand = $(this).data('vendor'),
			tag = $(this).data('tag').split(','),
			_this = $(this);

		for (var k = 0; k < tag.length; k++) {
			var item = tag[k].split('-');

			if ( item[0] === 'color' ) {
				option.color.push(item[1]);
				_this.addClass(item[1]);
			}

			if ( item[0] === 'size' ) {
				if ( item[1] === 'onesize' ) {
					option.size.push('one-size');
					_this.addClass('one-size');
				}else {
					option.size.push(item[1]);
					_this.addClass(item[1]);
				}
			}
		}

		option.brand.push(brand);
		_this.removeAttr('data-tag');
	});

	var naturalSort = function(ary, fullNumbers) {
	  var re = fullNumbers ? /[\d\.\-]+|\D+/g : /\d+|\D+/g;

	  // Perform a Schwartzian transform, breaking each entry into pieces first
	  for (var i=ary.length;i--;)
	    ary[i] = [ary[i]].concat((ary[i]+"").match(re).map(function(s){
	      return isNaN(s) ? [s,false,s] : [s*1,true,s];
	    }));

	  // Perform a cascading sort down the pieces
	  ary.sort(function(a,b){
	    var al = a.length, bl=b.length, e=al>bl?al:bl;
	    for (var i=1;i<e;++i) {
	      // Sort "a" before "a1"
	      if (i>=al) return -1; else if (i>=bl) return 1;
	      else if (a[i][0]!==b[i][0])
	        return (a[i][1]&&b[i][1]) ?        // Are we comparing numbers?
	               (a[i][0]-b[i][0]) :         // Then diff them.
	               (a[i][2]<b[i][2]) ? -1 : 1; // Otherwise, lexicographic sort
	    }
	    return 0;
	  });

	  // Restore the original values into the array
	  for (var i=ary.length;i--;) ary[i] = ary[i][0];
	  return ary;
	};

	var size =  {},
		color = [],
		brandColor = $('body').data('color-brand');
	option.size = unique(option.size);
	option.size = naturalSort(option.size);
	option.color = unique(option.color);

	for (let i = 0; i < option.size.length; i++) {
		var sizeStr = option.size[i].split(',');

		for (let l = 0; l < sizeStr.length; l++) {
			if ( sizeStr[l] !== '' ) {
                if ( sizeStr[l] === 'xs' ) {
                    size[0] = sizeStr[l];
                }else if ( sizeStr[l] === 's' ) {
                    size[1] = sizeStr[l];
                }else if ( sizeStr[l] === 'm' ) {
                    size[2] = sizeStr[l];
                }else if ( sizeStr[l] === 'l' ) {
                    size[3] = sizeStr[l];
                }else if ( sizeStr[l] === 'xl' ) {
                    size[4] = sizeStr[l];
                }else if ( sizeStr[l] === 'xxl' ) {
                    size[5] = sizeStr[l];
                }else if ( sizeStr[l] === 'xxl' ) {
                    size[6] = sizeStr[l];
                }else if ( sizeStr[l] === 'xxxl' ) {
                    size[7] = sizeStr[l];
                }else if ( sizeStr[l] === '4xl' ) {
                    size[8] = sizeStr[l];
                }else if ( sizeStr[l] === '5xl' ) {
                    size[9] = sizeStr[l];
                } else {
                    size[option.size.length + i] = sizeStr[l];
                }
			}
		}
	}

    var sortableSize = [];
    for (var vehicle in size) {
        sortableSize.push(size[vehicle]);
    }

	for (let i = 0; i < option.color.length; i++) {
		var colorStr = option.color[i].split(',');

		for (let l = 0; l < colorStr.length; l++) {
			if ( colorStr[l] !== '' ) color.push(colorStr[l]);
		}
	}

	option.brand = unique(option.brand).sort();
	option.size = unique(sortableSize);
	option.color = unique(color);

	// add shop by category
	catList.html('');

	$('body #menu-main .menu-item-has-children li.link.menu-item-has-children').each(function () {
		if ( 'https://' + window.location.host === $(this).find('a').attr('href').substr(0, window.location.host.length + 8) ) {
            catList.append(`<li>${$(this).find('div:eq(0)').html()}</li>`);
		}
	});

	// add shop by brand
	brandList.html('');
	for (let i = 0; i < option.brand.length; i++) {
		let active;

		for (let k = 0; k < chooseFilter.brand.length; k++) {
			if ( chooseFilter.brand[k] === option.brand[i] ) { active = 'active'; }
		}

		brandList.append(
			`<li class="${active === 'active' ? active : ''}" data-filter="${option.brand[i]}">
				<input type="checkbox" id="${option.brand[i]}" ${active === 'active' ? 'checked' : ''}>
				<label for="${option.brand[i]}">${option.brand[i]}</label>
			</li>`
		);
	}

	// add size
	sizeList.html('');
	for (let i = 0; i < sortableSize.length; i++) {
		let active;

		for (let k = 0; k < chooseFilter.size.length; k++) {
			if ( chooseFilter.size[k] === sortableSize[i] ) { active = 'active'; }
		}

		sizeList.append(
			`<li class="${sortableSize[i] === 'one-size' ? 'small' : ''} ${active === 'active' ? active : ''}" data-filter="${sortableSize[i]}">${sortableSize[i].toUpperCase()}</li>`
		);
	}

	// add color
	colorList.html('');
	for (let i = 0; i < option.color.length; i++) {
		let active;

		for (let k = 0; k < chooseFilter.color.length; k++) {
			if ( chooseFilter.color[k] === option.color[i] ) { active = 'active'; }
		}

		colorList.append(
			`<li class="${active === 'active' ? active : ''}" style="background-color: ${option.color[i]};" data-filter="${option.color[i]}"></li>`
		);
	}
	updateListStyle();*/

}
function updateListStyle() {
	var items;

	accordion.each(function (i, el) {
		items = $(el).find('ul a');

		if( items.length && $(el).is('.active')) {
			items.each(function (index, elem) {

				if(window.location.pathname === $(elem).attr('href')) {
					items.parent().removeClass('active');
					$(elem).parent().addClass('active');
					return;
				}
			});
		}
	});
}
// remove repeating item
function unique(arr) {
	var obj = {};

	for (var i = 0; i < arr.length; i++) {
		var str = arr[i];
		obj[str] = true;
	}

	return Object.keys(obj);
}
