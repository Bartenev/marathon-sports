var stamped = {
    init: function () {

        var interval = setInterval(()=> {
           if ( $('.stamped-container').length ) {
               intiChange();
               clearInterval(interval);
           }
        }, 200);
    }
};

function intiChange() {
    var fit,
        performance,
        comfort,
        durability;

    $('.stamped-summary-actions a').each(function () {
        var test = $(this).text();

        $(this).html(`<span>${test}</span>`).addClass('btn brand');
    });

    var starRating = $('.summary-overview');

    $('.stamped-summary .stamped-review-options ul li').each(function () {
        if ( $(this).data('title') ==='fit' ) {
            fit = $(this).html();
        }
        if ( $(this).data('title') ==='performance' ) {
            performance = $(this).html();
        }
        if ( $(this).data('title') ==='comfort' ) {
            comfort = $(this).html();
        }
        if ( $(this).data('title') ==='durability' ) {
            durability = $(this).html();
        }
    });

    $('.stamped-summary .stamped-review-options ul').html(`<li>${fit}</li><li>${performance}</li><li>${comfort}</li><li>${durability}</li>`);

    $('.stamped-summary .stamped-review-options ul').prepend('<li><strong class="stamped-review-option-title">Overall</strong>' + starRating.find('span').html() + '</li>');
    starRating.remove('');

    $('.summary-rating-title').each(function () {
        var text = $(this).siblings('.summary-rating-bar').data('rating');
        $(this).html(text);
    });

    $('.stamped-summary .stamped-review-options [data-title]').each(function () {
        var value = $(this).find('.stamped-review-option-scale').data('value');
    });

    $('.stamped-summary .stamped-summary-ratings').prepend('<h2 class="review-title">RATINGS OVERVIEW </h2>');
    $('.stamped-summary .stamped-review-options').prepend('<h2 class="review-title">AVERAGE CUSTOMER RATINGS</h2>');

    $('[data-value="would-you-recommend-this-products"]').each(function () {
        $(this).parents('.stamped-review').find('.stamped-review-header').eq(0).append(`<div class="question">${$(this).html()}</div>`);
    });

    if ( $('.product-single-reviews').data('review-count') === 0 || $('.product-single-reviews').data('review-count') === '' ) {
        $('.stamped-reviews').html('<p class="product-single-reviews__not active">be the first to leave a review</p>');
    }
}

export default stamped;