import map from '../components/map';
window.Cookies = require('js-cookie');

var brandColor = {
        'marathon_sports' : 'yellow',
        'runners_alley' : 'red',
        'sound_runner' : 'blue'
    };
var body = $('body');

var cookie = {
    init: function () {
        if ( body.is('.shopify') ) {
            setCookie();
        }
    }
};
function setCookie() {
    var siteColor = (location.search).split('=');

    for (var i = 0; i < siteColor.length; i++) {
        if ( siteColor[i] === 'marathon_sports' || siteColor[i] === 'runners_alley' || siteColor[i] === 'sound_runner' ) {
            if ( window.Cookies.get('site') !== siteColor ) {
                window.Cookies.set('site', brandColor[siteColor[i]], { expires: 1 });

            }
        }
    }

    if (  window.Cookies.get('site') === 'red' || window.Cookies.get('site') === 'blue' ) {
        body.removeClass('color-theme-yellow');
        body.addClass('color-theme-' + window.Cookies.get('site'));
        body.attr('data-color-brand', window.Cookies.get('site'));
    }

    function changeFavicon(img) {
        var favicon = document.querySelector('link[rel="shortcut icon"]');

        if (!favicon) {
            favicon = document.createElement('link');
            favicon.setAttribute('rel', 'shortcut icon');
            var head = document.querySelector('head');
            head.appendChild(favicon);
        }


        favicon.setAttribute('type', 'image/png');
        favicon.setAttribute('href', img);
    }

    if ( window.Cookies.get('site') === undefined ) {
        changeFavicon('https://cdn.shopify.com/s/files/1/2078/6663/t/1/assets/marathin_sports.png?10694592740171013127');
        $('.cart__footer .cart-note input').val('Marathon Sports');
    }
    if ( window.Cookies.get('site') === 'red' ) {
        changeFavicon('https://cdn.shopify.com/s/files/1/2078/6663/t/1/assets/Runners%20Alley.png?10694592740171013127');
        $('.cart__footer .cart-note input').val('Runners Alley');
    }
    if ( window.Cookies.get('site') === 'blue' ) {
        changeFavicon('https://cdn.shopify.com/s/files/1/2078/6663/t/1/assets/Soundrunner.png?10694592740171013127');
        $('.cart__footer .cart-note input').val('Sound Runner');
    }


    // let shopifyShippingLink = $('.product-form__shipping');
    // if ( shopifyShippingLink.length ) {
    //     if ( window.Cookies.get('site') === undefined ) {
    //         shopifyShippingLink.find('a').attr('href', shopifyShippingLink.data('yellow') );
    //     }else {
    //         shopifyShippingLink.find('a').attr('href', window.Cookies.get('site') );
    //     }
    // }

    showContent();
}

function showContent() {
    var content = $('.dynamic-content');

    content.each(function() {
        if ( $(this).data('color') !== body.attr('data-color-brand') ) {
            $(this).hide();
        }
    });
}

export default cookie;
