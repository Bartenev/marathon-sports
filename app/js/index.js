import navigation from './components/navigation';
import slider from './components/slider';
import VideoPlayer from './components/video-player';
import productRow from './components/product';
import {waypoints} from './components/waypoints';
import {map, locationFilter} from "./components/map";
import selectivityInit from './components/selectivityI-init';
import upcomingEvents from './components-shopify/shopify-events-build';
import cookie from './components-shopify/cookie';
import {filterInit, masonryResize} from './components-shopify/collection-filter';
import productInit from './components-shopify/product';
import stamped from './components-shopify/stamped';
import {dynamicContent} from './components-shopify/dynamic-content';
import accordion from './components/accordion';
import postList from './components/post-list';
import search from './components/search-with-product';



$(document).ready(function () {
	jQuery.noConflict();
    // A P P L I C A T I O N     C O D E);
    cookie.init();
    upcomingEvents.init();
    dynamicContent.init();
    filterInit.init();
    productInit.init();

    selectivityInit.init();
    if ( !$('body').is('.shopify') ) {
        navigation.init();
    }
    globalEvents();

    productRow.init();
    map.init();
    postList.init();

    slider.init();
    VideoPlayer.init();
    waypoints();
    btnWrap();
    accordion.init();
    loadComplete();
    stamped.init();
    search.init();
});

function btnWrap() {
    $('.shopify-challenge__button.btn')
        .wrap('<div class="btn brand brand--color-white"></div>')
        .removeClass('btn')
        .parents('form')
        .css('text-align', 'center');
}

function globalEvents() {
	$(window).on("mousewheel DOMMouseScroll touchmove scroll", function(event){
		scrollTop = getScrollPosition();
	});

    $(window).on('resize', function () {
        masonryResize();
        locationFilter.init();
    });
}
function getScrollPosition() {
	return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
}
function loadComplete() {
    if ( isIe ) {
        $('body').addClass('is-ie');
    }
    setTimeout(function() {
        $('body').addClass('loaded');
    }, 1000);
}
export var scrollTop = getScrollPosition();
export var isIe = navigator.userAgent.indexOf('MSIE ') > -1 || navigator.userAgent.indexOf('Trident/') > -1;
export var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
